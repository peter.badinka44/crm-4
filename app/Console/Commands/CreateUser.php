<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class CreateUser extends Command
{
    protected $signature = 'user:create {email}';

    protected $description = 'Create a new user';

    public function handle()
    {
        $email = $this->argument('email');

        if (User::where('email', $email)->exists()) {
            $this->error("User with email {$email} already exists!");
            return;
        }
 
        $user = new User();
        $user->email = $email;
        $user->email_array = "'$email'";
        $user->access = "'Kalkulačky'";
        $user->role = 'admin';

        $user->reg_date = now();
        $user->exp_date = '2100-01-01';
        $user->cp_mesta = '';
        $user->suhlas_callpage = 'false';
        $user->create_user = 'false';
        $user->kataster_okres = '';
        $user->key_api = '';
        $user->app_access = 'false';
        $user->app_max_rows = 100;
        $user->cp_contact_count = 0;
        $user->cp_contact_id = '';
        $user->cp_contact_send = 'true';
        $user->key_service = '';
        $user->login_key = '';

        $user->save();

        $this->info("User created successfully: {$email}");
    }
}