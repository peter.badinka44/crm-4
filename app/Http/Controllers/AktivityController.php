<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AktivityController extends Controller
{
    public function view() {
        $user = UserService::all();
        $access = false;
        foreach($user['access'] as $item) {
            if($item == 'Aktivity') $access = true;  
        }
        if($access) {
            $data = [];
            $access = false;
            foreach($user['access'] as $item) if($item == 'Aktivity') $data['aktivity'] = true;
            foreach($user['access'] as $item) if($item == 'Call-Page') $data['callPage'] = true;
            foreach($user['access'] as $item) if($item == 'Kalkulačky') $data['calculators'] = true;
            return view('aktivity.index', ['data' => $data]);
        }
        else return 'Acces danied!';
    }

    public function index() {
        $dateStart = request()['dateStart'];
        $dateEnd = request()['dateEnd'];

        $q = "select 
        id_user
        , sum(beb) as beb
        , sum(oslovene_kontakty) as oslovene_kontakty
        , sum(termin_studeny_trh) as termin_studeny_trh
        , sum(termin_databaza) as termin_databaza
        , sum(termin_odporucania) as termin_odporucania
        , sum(termin_leady) as termin_leady
        , sum(predstavenie) as predstavenie
        , sum(analyza) as analyza
        , sum(analyza_studeny_trh) as analyza_studeny_trh
        , sum(analyza_databaza) as analyza_databaza
        , sum(analyza_odporucania) as analyza_odporucania
        , sum(analyza_leady) as analyza_leady
        , sum(predaj) as predaj
        , sum(podpis) as podpis
        , sum(servis) as servis
        , sum(odporucania) as odporucania        
        , sum(poistenie_nezivot_bezny_ucet) as poistenie_nezivot_bezny_ucet
        , sum(poistenie_zivot) as poistenie_zivot
        , sum(uvery_hypo) as uvery_hypo
        , sum(dochodok_sds_dds) as dochodok_sds_dds
        , sum(investicie) as investicie
        , sum(oslovene_kontakty_spolupraca) as oslovene_kontakty_spolupraca
        , sum(zrealizovanie_pohovory) as zrealizovanie_pohovory
        , sum(den_bez_aktivit) as den_bez_aktivit
        , sum(administrativa) as administrativa
        , sum(vzdelavanie) as vzdelavanie
        , sum(dovolenka) as dovolenka
        from aktivity";
        $q .= " where date_reg >= '$dateStart'";
        $q .= " and date_reg <= '$dateEnd'";
        $q .= " group by id_user"; 
        $aktivity = DB::select($q);
        $aktivity = json_decode(json_encode($aktivity), true);

        $q = "SELECT 
        id_user
        , sum(beb_skut) as beb_skut
        FROM zmluvy";
        $q .= " where check_delete != 'ok'";
        $q .= " and date_podpis >= '$dateStart'";
        $q .= " and date_podpis <= '$dateEnd'";
        $q .= " group by id_user";
        $zmluvy = DB::select($q);
        $zmluvy = json_decode(json_encode($zmluvy), true);

        for($i = 0; $i < count($aktivity); $i++) {
            for($j = 0; $j < count($zmluvy); $j++) {
                if($aktivity[$i]['id_user'] == $zmluvy[$j]['id_user']) {
                    $sumBeb = $aktivity[$i]['beb'] + $zmluvy[$j]['beb_skut'];
                    $aktivity[$i]['beb'] = $sumBeb;
                }
            }
        }

        usort($aktivity, function($a, $b) {
            return $b['beb'] - $a['beb'];
        });

        $q = "SELECT id_user, date_start, beb_plan, beb_skut, kategoria, date_podpis FROM zmluvy";
        $q .= " WHERE check_delete != 'ok'";
        $q .= " and date_podpis >= '$dateStart'";
        $q .= " and date_podpis <= '$dateEnd'";
        $q .= " and check_delete != 'ok'";
        $q .= " order by beb_skut desc";
        $zmluvy = DB::select($q);

        return [
            'aktivity' => $aktivity,
            'zmluvy' => $zmluvy
        ];
    }

    public function store() {
        $q = QuerySqlService::insertAktivity(request()->data, 'aktivity');
        return DB::statement($q);
    }
}
