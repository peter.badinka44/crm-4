<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AngelicSoulController extends Controller
{
    public function view($id_person)
    {
        $data = ['id_person' => $id_person];
        return view('angelic-soul.index', ['data' => $data]);
    }

    public function index($id_person)
    {
        return $data = DB::table('main_db')
            ->where('id_person', $id_person)
            ->get();
    }

    public function update(Request $request) 
    {
        $query = QuerySqlService::update($request->data, 'main_db', 'id_person');
        DB::statement($query);
        return true;
    }
}
