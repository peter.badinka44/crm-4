<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiSupportController extends Controller
{
    public function index()
    {
        return [
            'randomString' => [
                'url' => '/api/support/randomString',
            ],
        ];
    }

    public function randomString($length = 32)
    {		
        $chars = [
            'numbers' => '0123456789',
            'lowercase' => 'abcdefghijklmnopqrstuvwxyz',
            'uppercase' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',            
            'special' => '_',
        ];
        $mix = $chars['numbers'].$chars['lowercase'].$chars['uppercase'].$chars['special'];

        if(isset(request()->chars)) $characters = request()->chars;
        else $characters = $mix;
        $timestamp = '';
        if(isset(request()->timestamp)) {
            if(request()->timestamp == true) {
                $timestamp = '_'.date('Ymd_His');
            }
        }

		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
        
        $link = "/api/support/randomString/32/$mix/true";
        $html = "<a href='$link'>$link</a>".'<br><br>';
        $html .= $randomString . $timestamp;

		return $html;
    }

    public function magic()
    {
        $x = request()->x; 
        $admin = 'peter.badinka44@gmail.com';
        if(UserService::value('email') == $admin) {
            if($x == 'false') {
                DB::table('users')
                    ->where('email', $admin)
                    ->update(['email_array' => "'".$admin."'"]);
                return "<h1>Magic deactive!</h1>";
            } 
            else {
                $collection = DB::table('users')->select('email')->get();
                $data = json_decode(json_encode($collection), true); 
                $email_array = '';           
                foreach($data as $item) {
                    $email_array .= "'".$item['email']."',";
                }
                $email_array = substr($email_array, 0, strlen($email_array) - 1);
                DB::table('users')
                    ->where('email', $admin)
                    ->update(['email_array' => $email_array]);
                return "<h1>Magic active!</h1>";
            }            
        } 
        else {
            return 'Access danied!';
        }


    }

}
