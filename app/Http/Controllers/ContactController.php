<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\MainDb;

class ContactController extends Controller
{
    // view
    public function view()
    {
        $user = UserService::all();
        $data = [];
        $access = false;
        foreach($user['access'] as $item) if($item == 'Aktivity') $data['aktivity'] = true;
        foreach($user['access'] as $item) if($item == 'Call-Page') $data['callPage'] = true;
        foreach($user['access'] as $item) if($item == 'Kalkulačky') $data['calculators'] = true;
        foreach($user['access'] as $item) if($item == 'SL - Správca klienta') $data['sl-spravca-klienta'] = true;
        return view('database.index', ['data' => $data]);
    }

    // get
    public function get($id)
    {
        $q = "
            select * from main_db
            where id_person = '$id'
            limit 1
        ";
        return DB::select($q);
    }

    // index
    public function index()
    {
        $sql = "select * from main_db";
        if(request()->filterUser != '-' && request()->filterDate != 'owner' ) {
            $sql .= " where id_user = '".request()->filterUser."'";
            $sql .= " and id_user in (".UserService::value('email_array').")";
        }  
        if(request()->filterUser == '-' && request()->filterDate != 'owner') {
            $sql .= " where id_user in (".UserService::value('email_array').")";
        }    
        if(request()->filterDate == 'owner'){
			$id_user = UserService::value('email');
			$sql .= " WHERE id_user_reg = '$id_user'";
			$sql .= " AND id_user != '$id_user'";
		}
        if(request()->filterDate == '0'){
			$sql .= " AND datum_akcie <= CURDATE()";
			$sql .= " AND datum_akcie != '0000-00-00'";
		}
        if(request()->filterDate == '1'){
			$date = strtotime("+1 day");
			$date_format = date('Y-m-d',$date);
			$sql .= " AND datum_akcie <= '$date_format'";
			$sql .= " AND datum_akcie != '0000-00-00'";
		}
        if(request()->filterDate == '7'){
			$date = strtotime("+7 day");
			$date_format = date('Y-m-d',$date);
			$sql .= " AND datum_akcie <= '$date_format'";
			$sql .= " AND datum_akcie != '0000-00-00'";
		}
        if(request()->filterDate == '30'){
			$date = strtotime("+30 day");
			$date_format = date('Y-m-d',$date);
			$sql .= " AND datum_akcie <= '$date_format'";
			$sql .= " AND datum_akcie != '0000-00-00'";
		}
        if(request()->filterDate == 'narodeniny'){
			$sql .= " AND DAY(datum_narodenia) >= DAY(CURDATE())";
			$sql .= " AND MONTH(datum_narodenia) >= MONTH(CURDATE())";
		}
        if(request()->filterDate == 'op_expire'){
			$sql .= " AND platnost_op_do > CURDATE()";
		}
		if(request()->checkDelete == false) $sql .= " AND check_delete != 'ok'";
		if(request()->checkDelete == true) $sql .= " AND check_delete = 'ok'";
        if(request()->checkKlient == true) $sql .= " AND check_klient = 'true'";

        if(request()->filterDate == '-'){
			$sql .= " ORDER BY date_reg  DESC";	
		}
        else if(request()->filterDate == '0' || request()->filterDate == '7' || request()->filterDate == '30'){         
            $sql .= " ORDER BY datum_akcie ASC";
        }
        else if(request()->filterDate == 'narodeniny'){ 
			$sql .= " ORDER BY MONTH(datum_narodenia), DAY(datum_narodenia) ASC";
		}
        else if(request()->filterDate == 'op_expire'){	
			$sql .= " ORDER BY platnost_op_do ASC";
		}
        else { 
		    $sql .= " ORDER BY datum_akcie ASC";
        }
        $sql .= " LIMIT 1000";

        $results = DB::select($sql);

        foreach ($results as $row) {
            $row->custom_fields = json_decode($row->custom_fields, true);
        }

        return $results;
    }

    // store
    public function store(Request $request)
    {
        $query = QuerySqlService::insert($request->data, 'main_db');
        DB::statement($query);
    }

    // update
    public function update()
    {
        $db = MainDb::where('id_person', request()->data['id_person'])->first();
        $db->update(request()->data);

        $q = "select sum(beb_skut) as beb from zmluvy";
        $q .= " where id_osoba = '" . request()->data['id_person'] ."'";
        $q .= " and check_delete != 'ok'";
        $q .= " group by id_osoba";
        $result = DB::select($q);
        if($result != [])
        {
            $result = json_decode(json_encode($result), true);
            $beb = $result[0]['beb'];
            DB::table('main_db')
            ->where('id_person', request()->data['id_person'])
            ->update(['beb_zmluvy' => $beb]);
            return true;
        } 
        else {
            return true;
        }
    }

    // destroy
    public function destroy($id)
    {
        $query = "
            update main_db set
            check_delete = 'ok'
            where id_person = '$id'
        ";
        return DB::statement($query);
    }

    // restore
    public function restore($id)
    {
        $query = "
            update main_db set
            check_delete = 'false'
            where id_person = '$id'
        ";
        return DB::statement($query);
    }

    // userSwap
    public function userSwap()
    {
        $users = DB::table('users')
        ->where('email', request()['email'])
        ->get();

        $email = request()['email'];
        $id_person = request()['contact']['id_person'];

        if(count($users) == 1) 
        {
            $q = "
                update main_db
                set id_user = '$email'
                where id_person = '$id_person'
                limit 1
            ";
            DB::statement($q);

            $q = "update zmluvy";
            $q .= " set id_user = '$email'";
            $q .= " where id_osoba = '$id_person'";
            DB::statement($q);           

            $q = "update ziadosti";
            $q .= " set id_user = '$email'";
            $q .= " where id_person = '$id_person'";
            DB::statement($q);

            $q = "update main_db_io";
            $q .= " set id_user = '$email'";
            $q .= " where id_person = '$id_person'";
            DB::statement($q);

            return ['action' => true];
        }
        else {
            return ['action' => false];
        }

    }

    // markKlient
    public function markKlient()
    {
        $check_klient = request()['contact']['check_klient'];        
        $id_user = UserService::value('email');
        $phone = request()['phone'];

        if($check_klient == 'false') $check_klient = 'true';
        else $check_klient = 'false';

        $array = request()['contact'];
        $array['check_klient'] = $check_klient;

        $q = QuerySqlService::update($array, 'main_db', 'id_person');
        DB::statement($q);

        // $stav = '-';
        // if($check_klient == 'true') $stav = 'klient';
        
        // $q = "
        //     update call_page
        //     set stav = '$stav',
        //     id_user = '$id_user'
        //     where phone = '$phone'
        //     limit 1
        // ";
        // DB::statement($q);

        // $q = "
        //     update kataster
        //     set check_klient = '$check_klient'
        //     where phone = '$phone'
        //     limit 1
        // ";
        // DB::connection('kataster')->statement($q);

        return ['check_klient' => $array['check_klient']];
    }

    // createZapis
    public function createZapis()
    {
        $id_person = request()['contact']['id_person'];
        $q = "
            update main_db
            set	zapis_link = 'true'
            where id_person = '$id_person'
            limit 1
        ";
        return DB::statement($q);
    }

    // createFinPlan
    public function createFinPlan()
    {
        $id_person = request()['contact']['id_person'];
        $q = "
            update main_db
            set	financny_plan_link = 'true'
            where id_person = '$id_person'
            limit 1
        ";
        return DB::statement($q);
    }

    // massChange
    public function massChange()
    {
        if(request()['data']['checkEmail']) {
            $user = $this->validateUser(request()['data']['email']);
            if(!$user) {
                return ['err' => 'Zadaný účet neexistu.'];
            }
        }        

        $id = implode(',', request()['id']);
        $q = "update main_db set id_upg = '".UserService::value('email')."'"; 
        $q .= ", date_upg = current_timestamp()";

        if(request()['data']['checkdDatumAkcie']) {
            $q .= ", datum_akcie = '".request()['data']['datumAkcie']."'";
        }
        if(request()['data']['checkStav']) {
            $q .= ", stav = concat('".request()['data']['stav'].", ', stav)";
        }
        if(request()['data']['checkEmail']) {
            $q .= ", id_user = '".request()['data']['email']."'";
        }
        if(request()['data']['checkIntervalServis']) {
           $q .= ", func_servis_email = '".request()['data']['intervalServis']."'";
        }
        if(request()['data']['checkDelete']) {            
            $q .= ", check_delete = 'ok'";
        } else {
            $q .= ", check_delete = '".request()['data']['checkDelete']."'";
        }
        
        $q .= " where id in (".$id.")";
        // return $q;
        DB::statement($q);

        $emails = [];

        if(request()['data']['checkCopyEmails']) {
            $emails = DB::table('main_db')
                ->whereIn('id', request()['id'])
                ->get('email');
        }

        return [
            'err' => false,
            'contacts' => $this->index(),
            'emails' => $emails,
        ];
    }

    private function validateUser($id_user)
    {
        $q = "
            select * from users
            where email = '$id_user'
        ";
        $result = DB::select($q);
        if(count($result) > 0) return true;
        else return false;
    }
    
}