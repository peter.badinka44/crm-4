<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Services\UserService;



class ContractController extends Controller
{
    public function index($id)
    {
        $email_array = UserService::array('email_array');
        return DB::table('zmluvy')
        ->whereIn('id_user', $email_array)
        ->where('id_user', '=', $id)
        ->where('check_delete', '!=', 'ok')
        ->orderBy('date_start', 'desc')
        ->orderBy('date_reg', 'desc')            
        ->get();
    }

    public function store(Request $request)
    {
        $query = QuerySqlService::insertContract($request->data, 'zmluvy');
        DB::statement($query);

        $id = $request['data']['id_osoba'];
        $beb = DB::table('zmluvy')
        ->where('id_osoba', '=', $id)
        ->where('check_delete', '!=', 'ok')
        ->sum('beb_skut');

        $query = "
            update main_db set
            beb_zmluvy = '$beb'
            where id_person = '$id'
        ";

        DB::statement($query);

        return DB::table('zmluvy')
        ->where('id_osoba', '=', $request['data']['id_osoba'])
        ->where('check_delete', '!=', 'ok')
        ->orderBy('date_start', 'desc')
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function update(Request $request)
    { 
        $query = QuerySqlService::update($request->data, 'zmluvy', 'id_zmluva');
        DB::statement($query);

        $id = $request['data']['id_osoba'];

        $beb = DB::table('zmluvy')
        ->where('id_osoba', '=', $id)
        ->where('check_delete', '!=', 'ok')
        ->sum('beb_skut');

        $query = "
            update main_db set
            beb_zmluvy = '$beb'
            where id_person = '$id'
        ";
        
        DB::statement($query);
        return ['beb' => $beb];
    }

    public function destroy()
    {
        $id_zmluv = request()->data['id_zmluva'];
        $id_person = request()->data['id_osoba'];
        $query = "
            update zmluvy set
            check_delete = 'ok'
            where id_zmluva = '$id_zmluv'
        ";
        DB::statement($query);

        $q = "select sum(beb_skut) as beb from zmluvy";
        $q .= " where id_osoba = '$id_person'";
        $q .= " and check_delete != 'ok'";
        $q .= " group by id_osoba";
        $result = DB::select($q);
        if($result != [])
        {
            $result = json_decode(json_encode($result), true);
            $beb = $result[0]['beb'];
            DB::table('main_db')
            ->where('id_person', $id_person)
            ->update(['beb_zmluvy' => $beb]);
            return ['beb' => $beb];
        } 
        else {
            DB::table('main_db')
            ->where('id_person', $id_person)
            ->update(['beb_zmluvy' => '0']);
            return ['beb' => '0'];
        }
    }
}
