<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomFields extends Controller
{
    public function index()
    {
        return DB::table('global_attributes')
            ->where('type', 'custom_fields_main_db')
            ->get('attr')
            ->toArray()[0]
            ->attr;
    }

    public function updateAttribute(
        Request $request
    ): bool
    {
        DB::table('global_attributes')
            ->where('type', 'custom_fields_main_db')
            ->update([
                'attr' => $request->all(),
            ]);

        return true;
    }
}
