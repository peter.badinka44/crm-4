<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Services\UserService;

class DogController extends Controller
{
    public function getAll() {
        return DB::table('dogs')
        ->where('id_user', UserService::value('email'))
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    public function store(Request $request)
    {
        $query = QuerySqlService::insertDog($request, 'dogs');
        DB::statement($query);        
        return DB::table('dogs')
        ->where('id_user', UserService::value('email'))
        ->where('id_person', $request['contact']['id_person'])
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    public function update(Request $request)
    {
        $query = QuerySqlService::update($request->data, 'dogs', 'id');
        DB::statement($query);
        return true;
    }

    public function delete()
    {
        $id_string = request()->data['id_string']; 
        DB::statement("
            delete from dogs
            where id_string = '$id_string'
            limit 1
        ");
        return true;
    }
}
