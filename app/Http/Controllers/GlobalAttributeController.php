<?php

namespace App\Http\Controllers;

use App\Models\GlobalAttribute;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;

class GlobalAttributeController extends Controller
{    
    public function getHypoIndex()
    {      
        $user = UserService::all();  
        return [
            'default' => GlobalAttribute::where('type', 'hypo_default_attributes')
                ->get('attr')[0],
            'data' => GlobalAttribute::where('type', 'hypo_custom_calculation')
                ->where('value', $user['email'])
                ->orderBy('created_at', 'desc')
                ->get()
        ];
    }

    public function crateNewHypo()
    {
        $user = UserService::all();
        $model = new GlobalAttribute();
        $model->type = 'hypo_custom_calculation';
        $model->key = 'user_id';
        $model->value = $user['email'];
        $model->attr = request('data');
        $model->save();
        return true;
    }

    public function updateHypo()
    {
        DB::table('global_attributes')
            ->where('id', request('data.id'))
            ->update(['attr' => request('data')]);

        return true;
    }    

    public function deleteHypo()
    {
        DB::table('global_attributes')
            ->where('id', request('id'))
            ->delete();
        
        return true;
    }

    public function updateHypoSettings()
    {
        DB::table('global_attributes')
            ->where('type', 'hypo_default_attributes')
            ->update(['attr' => request('data')]);

        return true;
    }
}
