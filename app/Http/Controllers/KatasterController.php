<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KatasterController extends Controller
{
    public function view()
    {
        return view('kataster.index');
    }
}
