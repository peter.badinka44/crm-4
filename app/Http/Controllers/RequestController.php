<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class RequestController extends Controller
{
    // index
    public function index() {
        return DB::table('requests')        
        ->where('id_user', UserService::value('email'))
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    // index vyrocie
    public function indexVyrocie() {
        $id_user = UserService::value('email');
        
        $query = "
            SELECT *, MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) - 42 as ltt 
            FROM zmluvy as t
            WHERE id_user = '$id_user' 
            AND date_start > '0000-00-00' 
            AND length(date_start) > 9 
            AND date_start != 'NaN.NaN.NaN' 
            AND check_delete != 'ok' 
            AND stav != 'vypovedaná' 
            AND MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) >= 42
            AND MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) <= 120 
            ORDER BY ltt ASC
        ";

        return DB::select($query);
    }

    // store
    public function store() {
        $query = QuerySqlService::insertRequest(request(), 'requests');
        DB::statement($query);
        return DB::table('requests')
        ->where('id_person', request()['contact']['id_person'])
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    // update
    public function update() {
        $query = QuerySqlService::update(request()->data, 'requests', 'id_string');
        DB::statement($query);

        return DB::table('requests')
        ->where('id_person', request()->data['id_person'])
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    // update
    public function destroy() {
        DB::table('requests')
        ->where('id_string', request()->data['id_string'])
        ->delete();
        
        return DB::table('requests')
        ->where('id_person', request()->data['id_person'])
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    // companies address
    public function companies() {
        return DB::table('companies')->orderBy('address_1', 'ASC')->get();
    }

    // templates
    public function templates() {
        return DB::table('requests_template')
        ->orderBy('typ')
        ->get();
    }

    // stream pdf
    public function stream($id) {
        $collection = DB::table('requests')
        ->where('id_string', $id)
        ->get();

    	$data = json_decode(json_encode($collection), true);

        $collection = DB::table('main_db')
        ->where('id_person', $data[0]['id_person'])
        ->get();

        $contact = json_decode(json_encode($collection), true);

        $data[0]['body'] = $this->replaceVariables(
            $contact[0],
            $data[0]['body'],
            $data[0]['cislo_uctu']
        );

        if(count($data) == 0) {
            return '<h1>Access danied!</h1>';
        } else {
            $pdf = PDF::loadView('database.requests.pdf', ['data' => $data[0]]);
            return $pdf->stream(
                $data[0]['name_full']
                    .', '.$data[0]['institucia']
                    .', '.$data[0]['program']
                    .'.pdf'
                );
        }
    }

    // download pdf
    public function download($id) {
        $temp = DB::table('requests')        
        ->where('id_string', $id)
        ->get();

        $collection = $temp;
    	$data = json_decode(json_encode($collection), true);

        if(count($data)  == 0) {
            return '<h1>Access danied!</h1>';
        } else {
            $pdf = PDF::loadView('database.requests.pdf', ['data' => $data[0]]);
            return $pdf->download(
                $data[0]['name_full']
                    .', '.$data[0]['institucia']
                    .', '.$data[0]['program']
                    .'.pdf'
                );
        }
    }
    
    private function replaceVariables(
        $contact,
        $body,
        $bank_number = null
    )
    {
        foreach ($contact as $key => $value) {
            $searchValue = '{' . $key . '}';
            if (strpos($body, $searchValue) !== false) {
                if (strlen($value) == 0 || $value == null) {
                    $value = '................................................';
                }
                if ($key == 'cislo_uctu') {
                    $value = $bank_number;
                }
                $body = str_replace(
                    $searchValue, 
                    $value, 
                    $body
                );
            }
        }
        return $body;
    }
}
