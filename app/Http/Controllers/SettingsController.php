<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    public function view() 
    {
        $user = UserService::all();
        $data = [];
        $access = false;
        foreach($user['access'] as $item) if($item == 'Aktivity') $data['aktivity'] = true;
        foreach($user['access'] as $item) if($item == 'Call-Page') $data['callPage'] = true;
        foreach($user['access'] as $item) if($item == 'Kalkulačky') $data['calculators'] = true;
        return view('settings.index', ['data' => $data]);
    }

    public function getShareUsers()
    {
        $email = UserService::value('email');
        $q = "
        select email from users
        where email_array like '%$email%'
        ";
        return DB::select($q);
    }    

    public function addShareUser($email)
    {
        if($this->validateUser($email)) {

            $_email = UserService::value('email');
            $collect = DB::table('users')->where('email', $email)->get();
            $user = json_decode(json_encode($collect), true);            
            $email_array = $user[0]['email_array'].",'$_email'";

            return DB::table('users')
                ->where('email', $email)
                ->update(['email_array' => $email_array]);  

            return [
                'err' => false,
            ];
        } else {
            return ['err' => "Účet [$email] neexistuje."];
        }        
    }

    public function removeShareUser($email)
    {
        if($this->validateUser($email)) {

            $_email = UserService::value('email');
            $collect = DB::table('users')->where('email', $email)->get();
            $user = json_decode(json_encode($collect), true);
            $email_array = $user[0]['email_array'];
            $email_array = str_replace(",'$_email'", '', $email_array);

            return DB::table('users')
                ->where('email', $email)
                ->update(['email_array' => $email_array]);

            return [
                'err' => false,
            ];
        } else {
            return ['err' => "Účet [$email] neexistuje."];
        }
    }

    public function removeAccess($email) 
    {
        if($this->validateUser($email)) 
        {
            $_email = UserService::value('email');
            $collect = DB::table('users')->where('email', $_email)->get();
            $user = json_decode(json_encode($collect), true);
            $email_array = $user[0]['email_array'];
            $email_array = str_replace(",'$email'", '', $email_array);

            DB::table('users')
                ->where('email', $_email)
                ->update(['email_array' => $email_array]);    

            session(['user' => DB::table('users')
                ->where('email', $_email)
                ->get()
            ]);

            $email_array = str_replace("'", '', $email_array);
            $array = explode(',', $email_array);

            return [
                'err' => false,
                'email_array' => $array,
            ];
        } else {
            return ['err' => "Účet [$email] neexistuje."];
        }
    }

    private function validateUser($id_user)
    {
        $q = "
            select * from users
            where email = '$id_user'
        ";
        $result = DB::select($q);
        if(count($result) > 0) return true;
        else return false;
    }

    public function getCompanies() 
    {
        return DB::select("
            SELECT * FROM companies
            ORDER BY address_1 ASC
        ");
    }

    public function insertCompany()
    {
        $newCompany = request()->toArray();
        $newCompany['name'] = $newCompany['address_1'];
        DB::table('companies')->insert($newCompany);
        return true;
    }

    public function deleteCompany()
    {
        DB::table('companies')->delete(request()['id']);
        return true;
    }

    public function editCompany()
    {
        $company = request();
        $q = "UPDATE companies";
        $q .= " SET name = " . "'" . $company['address_1'] . "'";
        $q .= ", address_1 = " . "'" . $company['address_1'] . "'";
        $q .= ", address_2 = " . "'" . $company['address_2'] . "'";
        $q .= ", address_3 = " . "'" . $company['address_3'] . "'";
        $q .= ", address_4 = " . "'" . $company['address_4'] . "'";
        $q .= ", address_5 = " . "'" . $company['address_5'] . "'";
        $q .= " WHERE id = " . $company['id'];
        return DB::statement($q);
    }

    public function getRequests() 
    {
        return DB::select("
            SELECT * FROM requests_template 
            ORDER BY typ ASC
        ");
    }

    public function insertRequest()
    {        
        DB::table('requests_template')->insert(
            request()->toArray()
        );
        return true;
    }

    public function editRequest()
    {
        $data = request()->toArray();
        unset($data['id']);
        DB::table('requests_template')
            ->where('id', '=', request('id'))
            ->update($data);
        return true;            
    }

    public function deleteRequest()
    {
        DB::table('requests_template')->delete(request()['id']);
        return true;
    }

    public function getLoginHistory()
    {
        $email = UserService::value('email');
        return DB::table('login')
            ->where('email', $email)
            ->orderBy('date', 'DESC')
            ->limit(100)
            ->get();
    }
}
