<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DOMDocument;
use DOMXPath;

class SlSpravcaKlientaController extends Controller
{
    public function view()
    {
        return view('calculators.sl-spravca-klienta');
    }

    public function processHtml(Request $request)
    {
        $request->validate([
            'htmlFile' => 'required|file|mimes:html,htm'
        ]);
 
        $file = $request->file('htmlFile');
        $html = file_get_contents($file->getPathname());
  
        $dom = new DOMDocument;
        @$dom->loadHTML($html);

        $xpath = new DOMXPath($dom);

        $rows = $xpath->query('//table[@class="grid"]/tbody/tr');

        $data = [];

        foreach ($rows as $row) {
            $cols = $xpath->query('td', $row);
            $rowData = [];

            foreach ($cols as $col) {
                $rowData[] = trim($col->nodeValue);
            }

            $onclickElements = $xpath->query('.//@onclick', $row);
            foreach ($onclickElements as $onclick) {
                if (preg_match('/window\.location\.href=\'(.*?)\'/', $onclick->nodeValue, $matches)) {
                    $rowData[] = $matches[1];
                }
            }

            $data[] = $rowData;
        }

        $cleanData = [];
        foreach ($data as $value) {
            $cleanData[] = [
                "https://findata.swisslifeselect.sk$value[19]",
                $value[4],
                $value[5],
                $value[6],                
            ];
        }

        return view('calculators.sl-spravca-klienta', ['links' => $cleanData]);
    }
}
