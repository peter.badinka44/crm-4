<?php

namespace App\Http\Controllers;

use App\Services\QuerySqlService;
use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;

class SubContactController extends Controller
{

    public function index()
    {
        return DB::table('main_db_io')
        ->whereIn('id_user', UserService::array('email_array'))
        ->where('check_delete', 'false')
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    public function update() 
    {
        $query = QuerySqlService::update(request()->data, 'main_db_io', 'id_person_io');
        DB::statement($query);

        return DB::table('main_db_io')
        ->where('id_person', request()->data['id_person'])
        ->where('check_delete', 'false')
        ->get();
    }

    public function store()
    {
        $query = QuerySqlService::insertSubContact(request(), 'main_db_io');
        DB::statement($query);
        return DB::table('main_db_io')
        ->where('id_person', request()['contact']['id_person'])
        ->where('check_delete', 'false')
        ->orderBy('date_reg', 'desc')
        ->get();
    }

    public function destroy()
    {
        $id = request()->data['id_person_io'];
        $query = "
            update main_db_io set
            check_delete = 'ok'
            where id_person_io = '$id'
            limit 1
        ";
        DB::statement($query);

        return DB::table('main_db_io')
        ->where('id_person', request()->data['id_person'])
        ->where('check_delete', 'false')
        ->get();
    }
}
