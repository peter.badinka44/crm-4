<?php

namespace App\Http\Controllers;

use App\Services\SendMailService;
use Illuminate\Support\Facades\DB;
use App\Services\SupportService;
use App\Services\UserService;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $user;

    //==================================================================
    // loginView
    //==================================================================
    public function view()
    {
        if(!session()->has('user'))
        {            
            return view('auth.login');
        }
        else return redirect('/');
    }

    public function sendLoginKey() {
        $email = request()->email;
        if($this->checkUserExist($email)) {

            $loginKey = SendMailService::sendMailLogin($email);

            DB::statement("
                update users 
                set login_key = '$loginKey'
                where email = '$email'
            ");

            return ['err' => false];
        } else {
            return ['err' => "Email <$email> sa nepodarilo spárovať zo žiadným účtom."];
        }
        
    }

    public function changePassword() {
        $user = UserService::all();
        $users = DB::table('users')->where('email', $user['email'])->get();

        if (!Hash::check(request('old'), $users[0]->pass)) {
            return [
                'err' => true,
                'msg' => 'Old password is not valid.'
            ];
        }

        DB::table('users')
        ->where('email', $user['email'])
        ->update(
            ['pass' => Hash::make(request('new'))]
        );

        return [
            'err' => false
        ];
    }

    private function checkUserExist($email) {
        $email = DB::select("
            select email from users
            where email = '$email'
        ");
        if(count($email) > 0) return true;
        else return false;
    }

    //==================================================================
    // convertCollectionToArray
    //==================================================================
    public function convertCollectionToArray() 
    {
        $collect = session()->get('user');
        $user = json_decode(json_encode($collect), true);
        return $user[0]['email'];
    }

    //==================================================================
    // show
    //==================================================================
    public function show() {
        return UserService::all();
    }

    //==================================================================
    // login
    //==================================================================
    public function login()
    {
        $auth = $this->auth();

        if($auth['access'] == 'true')
        {
            session()->put('user', $this->user);
            return [
                'access' => true,
                'user' => $this->user,
            ];
        }
        
        return $auth;  
    }

    //==================================================================
    // logout
    //==================================================================
    public function logout() 
    {
        session()->forget('user');
        return redirect('/login');
    }

    //==================================================================
    // auth
    //==================================================================
    private function auth()
    {
        if (str_contains(env('APP_URL'), 'localhost')) {
            return $this->offlineAuth();
        } else {
            return $this->serverAuth();
        }
    }

    private function serverAuth()
    {
        $email = request()->email;        
        $this->user = DB::table('users')
            ->where('email', '=', $email)
            ->get();
        if(count($this->user) > 0)
        {
            $pass = $this->user[0]->login_key;
            if($pass == request()->pass) {
                $new_api_key = SupportService::genRandomString(32);
                DB::table('users')
                    ->where('email', '=', $email)
                    ->update(['key_api' => $new_api_key]);  
                $this->user[0]->key_api = $new_api_key;
                $this->saveLogin($email, 'true');
                DB::statement("
                    update users set login_key = '$new_api_key'
                    where email = '$email'
                ");
                return [
                    'access' => 'true',
                    'user' => $this->user[0],
                ];
            } else {
                $this->saveLogin($email, 'false');
                return ['access' => 'false'];
            }
        }
        else { 
            $this->saveLogin($email, 'false');
            return [
                'access' => 'false',
            ];
        }
    }

    //==================================================================
    // auth
    //==================================================================
    private function offlineAuth()
    {
        $email = request()->email;
        $this->user = DB::table('users')
            ->where('email', '=', $email)
            ->get();
        if(count($this->user) > 0)
        {
            $pass = $this->user[0]->pass;
            if(Hash::check(request()->pass, $pass)) {
                $this->saveLogin($email, 'true');   
                return [
                    'access' => 'true',
                    'user' => $this->user[0],
                ];
            } else {
                $this->saveLogin($email, 'false');
                return [
                    'access' => 'false',
                    'pass' => Hash::make(request()->pass)
                ];
            }
        }
        else { 
            $this->saveLogin($email, 'false');
            return [
                'access' => 'false',
                'pass' => Hash::make(request()->pass)
            ];
        }
    }

    //==================================================================
    // save login
    //==================================================================
    private function saveLogin($email, $access)
    {
        DB::table('login')->insert([        
            'email' => $email,
            'access' => $access,
        ]);
    }    
    
    //==================================================================
    // getEmailHistory
    //==================================================================
    public function getEmailHistory() {
        return DB::table('servis_h')
            ->where('id_user', UserService::value('email'))
            ->orderBy('date_reg', 'desc')
            ->limit(500)
            ->get();
    }    
    
}