<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainDb extends Model
{
    use HasFactory;

    protected $table = 'main_db';

    protected $guarded = [];

    protected $casts = [
        'custom_fields' => 'array',
    ];
}
