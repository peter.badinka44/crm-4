<?php

namespace App\Services;

use App\Services\UserService;
use App\Services\SupportService;

class QuerySqlService extends Service {

	//=======================================================================================
	// update
	//=======================================================================================
	static public function update($array, $tableName, $idName) {
		$sql = "UPDATE $tableName SET ";
		foreach($array as $key => $value) {
			$sql .= "$key = '$value', ";
		}
		$sql = substr($sql, 0, strlen($sql) - 2);
		$sql .= " WHERE $idName = '".$array[$idName]."'";
		$sql .= " LIMIT 1";
		return $sql;
	}

	//=======================================================================================
	// insert contact
	//=======================================================================================
	static public function insert($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_person, id_user, id_user_reg, id_upg) values(";
		foreach($array as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_person = SupportService::genRandomString(32);
		$id_user = UserService::value('email');

		$sql .= "'$id_person', ";
		$sql .= "'$id_user', ";
		$sql .= "'$id_user', ";
		$sql .= "'$id_user')";
		return $sql;
	}

	//=======================================================================================
	// insert contract
	//=======================================================================================
	static public function insertContract($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_user, id_user_reg, id_zmluva) values(";
		foreach($array as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_contract = SupportService::genRandomString(32);
		$id_user = UserService::value('email');

		$sql .= "'$id_user', ";
		$sql .= "'$id_user', ";
		$sql .= "'$id_contract')";
		return $sql;
	}

	//=======================================================================================
	// insert request
	//=======================================================================================
	static public function insertRequest($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array['request'] as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_user, id_person, id_string) values(";
		foreach($array['request'] as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_string = SupportService::genRandomString(32);
		$id_person = $array['contact']['id_person'];
		$id_user = UserService::value('email');

		$sql .= "'$id_user', ";
		$sql .= "'$id_person', ";
		$sql .= "'$id_string')";
		return $sql;
	}

	//=======================================================================================
	// insert subContact
	//=======================================================================================
	static public function insertSubContact($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array['subContact'] as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_user, id_person, id_person_io) values(";
		foreach($array['subContact'] as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_person_io = SupportService::genRandomString(32);
		$id_person = $array['contact']['id_person'];
		$id_user = UserService::value('email');

		$sql .= "'$id_user', ";
		$sql .= "'$id_person', ";
		$sql .= "'$id_person_io')";
		return $sql;
	}

	//=======================================================================================
	// insert Dog
	//=======================================================================================
	static public function insertDog($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array['dog'] as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_user, id_person, id_string) values(";
		foreach($array['dog'] as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_string = SupportService::genRandomString(32);
		$id_person = $array['contact']['id_person'];
		$id_user = UserService::value('email');

		$sql .= "'$id_user', ";
		$sql .= "'$id_person', ";
		$sql .= "'$id_string')";
		return $sql;
	}

	//=======================================================================================
	// insert subContact
	//=======================================================================================
	static public function insertAktivity($array, $tableName) {
		$sql = "insert into $tableName (";
		foreach($array as $key => $value) {
			$sql .= "$key, ";
		}

		$sql .= "id_user) values(";
		foreach($array as $key => $value) {
			$sql .= "'$value', ";
		}

		$id_user = UserService::value('email');

		$sql .= "'$id_user')";
		return $sql;
	}
}