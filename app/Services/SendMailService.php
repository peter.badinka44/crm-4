<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class SendMailService extends Service
{
	//===================================================================================
	// Send custom mail
	//===================================================================================
    public static function sendMail($to, $subject, $html)
    {        
        Mail::send([], [], function ($message) use ($to, $html, $subject)
        {
            $message->to($to)
                ->subject($subject)
                ->setBody($html, 'text/html');
            $message->from(env('MAIL_USERNAME'));
        });
    }

	//===================================================================================
	// Send login mail
	//===================================================================================
	public static function sendMailLogin($to)
    {
		$loginKey = self::genLoginKye();
        $subject = 'Prihlasovací kód: '.$loginKey;
        $html = 'Prihlasovací kód: <b>'.$loginKey.'</b>';
        self::sendMail($to, $subject, $html);
		return $loginKey;
    }

	//===================================================================================
	// genLoginKye
	//===================================================================================
	public static function genLoginKye()
	{
		// $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 4; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];			
		}
		return $randomString;
	}
}