<?php

namespace App\Services;

/*
|============================================================================================
| Tools
|============================================================================================
|
| Support functions...
|
*/
class SupportService extends Service {

	//=======================================================================================
	// Ger user value from session()->get('user')
	//=======================================================================================
	static public function user($key)
	{
		$collection = session()->get('user');
        $user = json_decode(json_encode($collection), true);
		return $user[0][$key];
	}

	//=======================================================================================
	// Return random string with timestamp
	//=======================================================================================
	static public function genRandomString($length = 10) 
	{
		$date = date('Ymd_His');
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString . '_' . $date;
	}

	//=======================================================================================
	// encrypt / decrypt
	//=======================================================================================
	static public function encrypt($message)
	{
		$encryption_key = '860e1350c27b2d761d477d44c405608ba6927668ec548adf0b2fc94aaab34425';
		$key = hex2bin($encryption_key);
		$nonceSize = openssl_cipher_iv_length('aes-256-ctr');
		$nonce = openssl_random_pseudo_bytes($nonceSize);
		$ciphertext = openssl_encrypt(
			$message,
			'aes-256-ctr', 
			$key,
			OPENSSL_RAW_DATA,
			$nonce
		);
		return base64_encode($nonce.$ciphertext);
	}

	//=======================================================================================
	// decrypt
	//=======================================================================================
	static public function decrypt($message)
	{
		$encryption_key = '860e1350c27b2d761d477d44c405608ba6927668ec548adf0b2fc94aaab34425';
		$key = hex2bin($encryption_key);
		$message = base64_decode($message);
		$nonceSize = openssl_cipher_iv_length('aes-256-ctr');
		$nonce = mb_substr($message, 0, $nonceSize, '8bit');
		$ciphertext = mb_substr($message, $nonceSize, null, '8bit');
		$plaintext= openssl_decrypt(
				$ciphertext, 
				'aes-256-ctr', 
				$key,
				OPENSSL_RAW_DATA,
				$nonce
		);
		return $plaintext;
	}

	//=======================================================================================
	// Return next bussiness day / SK holidays
	//=======================================================================================
	static public function nextBusinessDay($date)	
	{
		$holidays = ['01-01', '01-06', '04-10', '04-13', '05-01', '05-08', '07-05', '08-29', '09-01', '09-15', '11-17', '12-24', '12-25', '12-26'];
		$Y = date('Y', strtotime($date));
		$i = 1;
		$nextBusinessDay = date('m-d', strtotime($date . ' +' . $i . ' Weekday'));
		while (in_array($nextBusinessDay, $holidays)) {
			$i++;
			$nextBusinessDay = date('m-d', strtotime($date . ' +' . $i . ' Weekday'));
		}
		return date('Y-m-d', strtotime($Y.'-'.$nextBusinessDay));
	}

}