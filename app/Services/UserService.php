<?php

namespace App\Services;

class UserService extends Service
{
	//=======================================================================================
	// Ger user value from session()->get('user')
	//=======================================================================================
	static public function value($key)
	{
		$collection = session()->get('user');
    	$user = json_decode(json_encode($collection), true);
		return $user[0][$key];
	}

	//=======================================================================================
	// Ger array from user value => session()->get('user')
	//=======================================================================================
	static public function array($key)
	{
		$collection = session()->get('user');
    	$user = json_decode(json_encode($collection), true);
		$array = explode(',', str_replace("'", "", $user[0][$key]));
		return $array;
	}

	//=======================================================================================
	// Ger array from user value => session()->get('user')
	//=======================================================================================
	static public function all()
	{
		$collection = session()->get('user');
    	$user = json_decode(json_encode($collection), true);
		$user = $user[0];
		$user['access'] = self::array('access');
		$user['email_array'] = self::array('email_array');
		$user['cp_mesta'] = self::array('cp_mesta');
		$user['kataster_okres'] = self::array('kataster_okres');
		return $user;
	}
}