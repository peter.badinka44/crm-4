<?php

use App\Models\GlobalAttribute;
use Illuminate\Database\Migrations\Migration;

class SetHypoDefaultAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new GlobalAttribute();
        $model->type = 'hypo_default_attributes';
        $model->attr = [
            'koeficienty' => [
                'zivotneMin' => [
                    'dospelaOsoba1' => 234.41,
                    'dospelaOsoba2' => 163.53,
                    'dieta' => 107.03,
                ],
                'dti' => 8
            ],
            'default_values' => [
                'vek' => 18,
                'mesacnyPrijem' => 1000,
                'mesacneSplatky' => 0,
                'mesacneSplatkyHypo' => 0,
                'aktualnyZostatokUverov' => 0,
                'pocetOsob' => 1,
                'pocetDeti' => 0,
                'vztah' => 'manžel a manželka / druh a druzka'
            ],
        ];
        $model->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
