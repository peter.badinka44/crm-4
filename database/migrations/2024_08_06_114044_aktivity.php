<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Aktivity extends Migration
{
    public function up()
    {
        Schema::create('aktivity', function (Blueprint $table) {
            $table->dateTime('date_reg')->useCurrent();
            $table->text('id_user');
            $table->text('oslovene_kontakty');
            $table->text('termin_studeny_trh');
            $table->text('termin_databaza');
            $table->text('termin_odporucania');
            $table->integer('termin_leady')->default(0);
            $table->text('predstavenie');
            $table->text('analyza');
            $table->integer('analyza_studeny_trh')->default(0);
            $table->integer('analyza_databaza')->default(0);
            $table->integer('analyza_odporucania')->default(0);
            $table->integer('analyza_leady')->default(0);
            $table->text('predaj');
            $table->text('podpis');
            $table->text('servis');
            $table->text('odporucania');
            $table->text('beb');
            $table->text('poistenie_nezivot_bezny_ucet');
            $table->text('poistenie_zivot');
            $table->text('uvery_hypo');
            $table->text('dochodok_sds_dds');
            $table->text('investicie');
            $table->text('oslovene_kontakty_spolupraca');
            $table->text('zrealizovanie_pohovory');
            $table->text('den_bez_aktivit');
            $table->integer('administrativa');
            $table->integer('vzdelavanie');
            $table->integer('dovolenka');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('aktivity');
    }
}