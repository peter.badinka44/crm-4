<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CallPageH extends Migration
{
    public function up()
    {
        Schema::create('call_page_h', function (Blueprint $table) {
            $table->date('date_reg')->useCurrent();
            $table->string('id_string', 100)->primary();
            $table->text('name_full');
            $table->text('phone');
            $table->dateTime('date_upg')->useCurrent();
            $table->text('id_user');
            $table->text('okres');
            $table->text('stav')->default('-');
            $table->date('date_akcia');
            $table->text('time');
            $table->text('poznamka');
            $table->text('produkt');
            $table->text('zamestnanie');
            $table->text('oblast');
            $table->text('email');
            $table->text('historia');
            $table->text('blacklist')->default('');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('call_page_h');
    }
}
