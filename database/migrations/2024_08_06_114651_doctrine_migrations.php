<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DoctrineMigrations extends Migration
{
    public function up()
    {
        Schema::create('doctrine_migrations', function (Blueprint $table) {
            $table->string('version', 14)->primary();
            $table->dateTime('executed_at')->comment('(DC2Type:datetime_immutable)');
        });
    }

    public function down()
    {
        Schema::dropIfExists('doctrine_migrations');
    }
}
