<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dogs extends Migration
{
    public function up()
    {
        Schema::create('dogs', function (Blueprint $table) {
            $table->id();
            $table->string('id_string')->nullable();
            $table->string('id_user')->nullable();
            $table->string('id_person')->nullable();
            $table->date('date_reg')->useCurrent();
            $table->string('meno');
            $table->string('meno_otec');
            $table->string('meno_matka');
            $table->string('cislo_cipu', 100);
            $table->string('vrh', 100);
            $table->date('datum_narodenia')->default('0000-00-00');
            $table->string('cena', 100);
            $table->string('rezervacny_poplatok', 100);
            $table->text('poznamky');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dogs');
    }
}
