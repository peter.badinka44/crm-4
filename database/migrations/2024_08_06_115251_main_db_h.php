<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MainDbH extends Migration
{
    public function up()
    {
        Schema::create('main_db_h', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_reg')->useCurrent();
            $table->text('zdroj_kontaktu')->default('-');
            $table->text('odporucil');
            $table->text('id_user');
            $table->text('okres');
            $table->text('android_sync')->default('');
            $table->text('name_full');
            $table->text('phone');
            $table->text('email');
            $table->dateTime('date_upg')->useCurrent();
            $table->date('datum_akcie')->default('0000-00-00');
            $table->text('stav');
            $table->mediumText('poznamka');
            $table->text('rodinny_stav');
            $table->text('vztah');
            $table->text('oblast');
            $table->text('specifikacia');
            $table->text('zamestnanie');
            $table->text('uzemna_platnost');
            $table->text('prijmy');
            $table->text('vydavky');
            $table->text('deti');
            $table->text('vyska');
            $table->text('hmotnost');
            $table->text('lekar_meno');
            $table->text('lekar_adresa');
            $table->text('lekar_kontakt');
            $table->text('cislo_uctu');
            $table->text('ulica');
            $table->text('psc_obec');
            $table->date('datum_narodenia')->default('0000-00-00');
            $table->text('miesto_narodenia');
            $table->text('rodne_cislo');
            $table->text('cislo_op');
            $table->date('platnost_op_od')->default('0000-00-00');
            $table->date('platnost_op_do')->default('0000-00-00');
            $table->text('doklad_vydal');
            $table->text('name_last');
            $table->text('name_first');
            $table->text('oslovenie')->default('');
            $table->text('func_narodeniny_meniny')->default('');
            $table->integer('func_servis_email')->default(0);
            $table->integer('index_servis')->default(0);
            $table->text('id_person');
            $table->text('check_delete')->default('false');
            $table->text('vzdelanie');
            $table->text('typ_prac_zmluvy');
            $table->date('zaciatok_zamestnania')->default('0000-00-00');
            $table->text('pocet_deti');
            $table->text('byvanie');
            $table->text('bezny_ucet_banka');
            $table->text('zdravotna_poistovna');
            $table->text('call_page');
            $table->string('kampan_email', 100);
            $table->text('id_user_reg');
            $table->text('kampan');
            $table->string('kampan_nazov', 200);
            $table->text('okres_kampan');
            $table->text('podiel_beb');
            $table->integer('index_narodeniny')->default(0);
            $table->integer('index_meniny')->default(0);
            $table->text('financny_plan_link')->default('false');
            $table->text('zapis_link')->default('false');
            $table->text('prod_odporucania');
            $table->text('prod_zakonne_poistenie');
            $table->text('prod_poistenie_majetku');
            $table->text('prod_zivotne_poistenie');
            $table->text('prod_hypoteka');
            $table->text('prod_spotrebny_uver');
            $table->text('prod_investicia');
            $table->text('prod_druhy_pilier');
            $table->text('prod_treti_pilier');
            $table->text('prod_beb');
            $table->text('check_klient')->default('false');
            $table->text('id_upg');
            $table->text('typ_akcie');
            $table->integer('beb_zmluvy')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('main_db_h');
    }
}
