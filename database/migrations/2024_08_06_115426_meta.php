<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Meta extends Migration
{
    public function up()
    {
        Schema::create('meta', function (Blueprint $table) {
            $table->id();
            $table->string('id_user', 100);
            $table->string('id_db', 100)->nullable();
            $table->string('db_name', 100)->nullable();
            $table->string('key', 100);
            $table->string('value', 500);
            $table->string('type', 100)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('meta');
    }
}
