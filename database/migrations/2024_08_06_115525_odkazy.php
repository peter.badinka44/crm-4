<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Odkazy extends Migration
{
    public function up()
    {
        Schema::create('odkazy', function (Blueprint $table) {
            $table->id();
            $table->text('id_user');
            $table->text('name');
            $table->text('info');
            $table->text('link');
            $table->text('check_delete')->default('false');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('odkazy');
    }
}
