<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Requests extends Migration
{
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_reg')->useCurrent();
            $table->string('id_string', 100);
            $table->string('id_user', 100);
            $table->string('id_person', 300);
            $table->string('typ_ziadosti', 200);
            $table->string('institucia', 100);
            $table->string('program', 100);
            $table->string('cislo_zmluvy', 100);
            $table->string('miesto_podpisu', 100);
            $table->dateTime('datum_podpisu')->useCurrent();
            $table->string('name_full', 100);
            $table->string('datum_narodenia', 100);
            $table->string('ulica', 100);
            $table->string('psc_obec', 100);
            $table->string('cislo_uctu', 100);
            $table->text('body');
            $table->string('address_1', 100);
            $table->string('address_2', 100);
            $table->string('address_3', 100);
            $table->string('address_4', 100);
            $table->string('address_5', 100);
            $table->string('t_odosielatel', 100);
            $table->string('t_miesto_datum_podpisu', 100);
            $table->string('t_vec', 100);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
