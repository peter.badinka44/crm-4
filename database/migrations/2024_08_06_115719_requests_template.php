<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RequestsTemplate extends Migration
{
    public function up()
    {
        Schema::create('requests_template', function (Blueprint $table) {
            $table->id();
            $table->string('typ', 200)->nullable();
            $table->string('vec', 200)->nullable();
            $table->text('body')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('requests_template');
    }
}
