<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServisH extends Migration
{
    public function up()
    {
        Schema::create('servis_h', function (Blueprint $table) {
            $table->id();
            $table->date('date_reg')->useCurrent();
            $table->text('id_user');
            $table->text('name_full');
            $table->text('email');
            $table->text('akcia');
            $table->text('id_person');
            $table->text('id_zmluva');
            $table->text('cislo_zmluvy');
            $table->text('produkt');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servis_h');
    }
}
