<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UlimiroFo extends Migration
{
    public function up()
    {
        Schema::create('ulimiro_fo', function (Blueprint $table) {
            $table->string('id', 100)->primary();
            $table->dateTime('date_reg')->useCurrent();
            $table->string('name_full', 100);
            $table->string('phone', 50);
            $table->text('address');
            $table->string('okres', 50);
            $table->string('stav', 100)->default('-');
            $table->date('datum_narodenia')->default('0000-00-00');
            $table->date('date_zapis')->default('0000-00-00');
            $table->string('banka', 100);
            $table->mediumText('poznamka');
            $table->dateTime('date_upg')->default('0000-00-00 00:00:00');
            $table->string('id_user', 100);
            $table->string('check_klient', 50)->default('false');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ulimiro_fo');
    }
}
