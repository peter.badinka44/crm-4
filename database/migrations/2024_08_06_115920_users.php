<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->dateTime('reg_date')->useCurrent();
            $table->date('exp_date')->default('2100-01-01');
            $table->text('access');
            $table->text('cp_mesta');
            $table->text('email');
            $table->text('pass');
            $table->text('email_array');
            $table->text('suhlas_callpage')->default('false');
            $table->dateTime('suhlas_callpage_date');
            $table->text('create_user')->default('false');
            $table->text('kataster_okres')->default("''");
            $table->string('key_api', 100);
            $table->integer('req_count')->default(0);
            $table->string('app_access', 100)->default('false');
            $table->integer('app_max_rows')->default(100);
            $table->integer('cp_contact_count')->default(0);
            $table->string('cp_contact_id', 100);
            $table->string('cp_contact_send', 100)->default('true');
            $table->string('key_service', 100);
            $table->string('login_key', 100);
            $table->string('role', 100);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
