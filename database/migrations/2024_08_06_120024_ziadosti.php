<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ziadosti extends Migration
{
    public function up()
    {
        Schema::create('ziadosti', function (Blueprint $table) {
            $table->dateTime('date_reg')->useCurrent();
            $table->text('id_user');
            $table->text('name_full');
            $table->text('typ_ziadosti');
            $table->text('institucia');
            $table->text('program');
            $table->text('cislo_zmluvy');
            $table->text('miesto_podpisu');
            $table->text('datum_narodenia');
            $table->text('ulica');
            $table->text('psc_obec');
            $table->text('cislo_uctu');
            $table->text('id_person');
            $table->string('id_ziadost', 100)->primary();
            $table->text('link')->default('false');
            $table->text('check_delete')->default('false');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ziadosti');
    }
}
