<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Zmluvy extends Migration
{
    public function up()
    {
        Schema::create('zmluvy', function (Blueprint $table) {
            $table->id();
            $table->date('date_reg')->useCurrent();
            $table->text('id_user');
            $table->text('name_full');
            $table->text('phone');
            $table->text('email');
            $table->text('poistnik');
            $table->text('poisteny');
            $table->text('produkt');
            $table->text('spolocnost');
            $table->text('cislo_zmluvy');
            $table->text('stav');
            $table->string('stav_servis', 100);
            $table->text('poznamka');
            $table->date('date_start');
            $table->date('date_end');
            $table->text('platba');
            $table->text('platba_interval');
            $table->text('rizikova_cast');
            $table->text('investicna_cast');
            $table->text('s');
            $table->text('su');
            $table->text('sud');
            $table->text('tn');
            $table->text('tn_max_plnenie');
            $table->text('pn');
            $table->text('do');
            $table->text('kch');
            $table->text('chz');
            $table->text('hosp');
            $table->text('zlomeniny_popleniny');
            $table->text('inv_40');
            $table->text('inv_70');
            $table->text('osl');
            $table->text('hodnota_nehnutelnost');
            $table->text('poistenie_nehntelnost');
            $table->text('poistenie_domacnost');
            $table->text('zodpovednost');
            $table->text('datum_poslednej_splatky');
            $table->text('vyska_uveru');
            $table->text('splatka');
            $table->text('pocet_splatok');
            $table->text('ostava_splatok');
            $table->text('urokova_sadzba');
            $table->text('rpmn');
            $table->text('fixacia');
            $table->text('pravidelna_mesacna_investicia');
            $table->text('hodnota_uctu');
            $table->text('cielova_suma');
            $table->text('upozornenie_makler');
            $table->text('upozornenie_klient');
            $table->integer('pocet_tyzdnov')->default(0);
            $table->text('id_zmluva');
            $table->text('id_osoba');
            $table->text('check_delete')->default('false');
            $table->text('id_user_reg');
            $table->text('kampan');
            $table->integer('beb_plan')->default(0);
            $table->integer('beb_skut')->default(0);
            $table->string('kategoria', 50)->default('-');
            $table->date('date_podpis')->default('0000-00-00');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('zmluvy');
    }
}
