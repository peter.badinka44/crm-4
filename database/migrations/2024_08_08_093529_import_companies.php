<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ImportCompanies extends Migration
{

    public function up()
    {
        DB::table('companies')->insert($this->getData());
    }

    public function down()
    {

    }

    public function getData(): array
    {
        return [
            [
                'name' => 'Allianz - Slovenská poisťovňa, a.s.',
                'address_1' => 'Allianz - Slovenská poisťovňa, a.s.',
                'address_2' => 'Pribinova 19',
                'address_3' => '815 74 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'ASTRA poisťovňa, a.s.',
                'address_1' => 'ASTRA poisťovňa, a.s.',
                'address_2' => 'Bratislava Business Center 1',
                'address_3' => 'Plynárenská 1',
                'address_4' => '821 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Colonnade Insurance S.A.',
                'address_1' => 'Colonnade Insurance S.A.',
                'address_2' => 'pobočka poisťovne z iného členského štátu',
                'address_3' => 'Moldavská cesta 8 B',
                'address_4' => '042 80 Košice',
                'address_5' => 'Slovenská republika',
            ],
            [
                'name' => 'ČSOB Poistovňa, a.s.',
                'address_1' => 'ČSOB Poistovňa, a.s.',
                'address_2' => 'Žižkova 11',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'DÔVERA zdravotná poisťovňa, a.s.',
                'address_1' => 'DÔVERA zdravotná poisťovňa, a.s.',
                'address_2' => 'DIGITAL PARK II',
                'address_3' => 'Einsteinova 25',
                'address_4' => '851 01 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Generali Poisťovňa, pobočka poisťovne z iného členského štátu',
                'address_1' => 'Generali Poisťovňa, pobočka poisťovne z iného členského štátu',
                'address_2' => 'Lamačská cesta 3/A',
                'address_3' => '841 04 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Generali Poisťovňa, a.s., odštepný závod Genertel',
                'address_1' => 'Generali Poisťovňa, a.s., odštepný závod Genertel',
                'address_2' => 'Lamačská cesta 3/A',
                'address_3' => '841 04 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Groupama poisťovňa a. s.',
                'address_1' => 'Groupama poisťovňa a. s.',
                'address_2' => 'Miletičova 21',
                'address_3' => '821 08 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'KOMUNÁLNA poisťovňa, a.s.',
                'address_1' => 'KOMUNÁLNA poisťovňa, a.s.',
                'address_2' => 'Vienna Insurance Group',
                'address_3' => 'Štefánikova 17',
                'address_4' => '811 05 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'KOOPERATIVA poisťovňa, a.s.',
                'address_1' => 'KOOPERATIVA poisťovňa, a.s.',
                'address_2' => 'Vienna Insurance Group',
                'address_3' => 'Štefanovičova 4',
                'address_4' => '816 23 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'MetLife Europe d.a.c., pobočka poisťovne z iného členského štátu',
                'address_1' => 'MetLife Europe d.a.c., pobočka poisťovne z iného členského štátu',
                'address_2' => 'Pribinova 10',
                'address_3' => '811 09 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'NN Životná poisťovňa, a.s.',
                'address_1' => 'NN Životná poisťovňa, a.s.',
                'address_2' => 'Jesenského 4/C',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'NOVIS Poisťovňa, a.s.',
                'address_1' => 'NOVIS Poisťovňa, a.s.',
                'address_2' => 'Nám. Ľ. Štúra 2',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Poisťovňa Cardif Slovakia, a.s.',
                'address_1' => 'Poisťovňa Cardif Slovakia, a.s.',
                'address_2' => 'Plynárenská 7/B',
                'address_3' => '821 09 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Poisťovňa Slovenskej sporiteľne, a.s.',
                'address_1' => 'Poisťovňa Slovenskej sporiteľne, a.s.',
                'address_2' => 'Vienna Insurance Group',
                'address_3' => 'Tomášikova 48',
                'address_4' => '832 68 Bratislava 3',
                'address_5' => '',
            ],
            [
                'name' => 'Poštová poisťovňa, a. s.',
                'address_1' => 'Poštová poisťovňa, a. s.',
                'address_2' => 'Dvořákovo nábrežie 4',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Rapid life životná poisťovňa, a.s.',
                'address_1' => 'Rapid life životná poisťovňa, a.s.',
                'address_2' => 'Garbiarska 2',
                'address_3' => '040 71 Košice',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Sociálna poisťovňa',
                'address_1' => 'Sociálna poisťovňa',
                'address_2' => 'Ul. 29. augusta č. 8 a 10',
                'address_3' => '813 63 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UNION poisťovňa, a.s.',
                'address_1' => 'UNION poisťovňa, a.s.',
                'address_2' => 'Karadžičova 10',
                'address_3' => '813 60 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Union zdravotná poisťovňa, a.s.',
                'address_1' => 'Union zdravotná poisťovňa, a.s.',
                'address_2' => 'Karadžičova 10',
                'address_3' => '814 53 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UNIQA pojišťovna, a.s., pobočka poisťovne z iného členského štátu',
                'address_1' => 'UNIQA pojišťovna, a.s., pobočka poisťovne z iného členského štátu',
                'address_2' => 'Krasovského 3986/15',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Všeobecná zdravotná poisťovňa, a.s.',
                'address_1' => 'Všeobecná zdravotná poisťovňa, a.s.',
                'address_2' => 'Mamateyova 17',
                'address_3' => 'P.O.BOX 41',
                'address_4' => '850 05 Bratislava 55',
                'address_5' => '',
            ],
            [
                'name' => 'Wüstenrot poisťovňa, a.s.',
                'address_1' => 'Wüstenrot poisťovňa, a.s.',
                'address_2' => 'Digital Park l, Einsteinova 21',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'AEGON, d.s.s., a.s.',
                'address_1' => 'AEGON, d.s.s., a.s.',
                'address_2' => 'Slávičie údolie 106',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Allianz, d.s.s, a. s.',
                'address_1' => 'Allianz - Slovenská dôchodková správcovská spoločnosť, a. s.',
                'address_2' => 'Račianska 62',
                'address_3' => '831 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'AXA d.s.s., a.s.',
                'address_1' => 'AXA d.s.s., a.s.',
                'address_2' => 'Kolárska 6',
                'address_3' => '811 06 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'NN, d.s.s., a.s.',
                'address_1' => 'NN dôchodková správcovská spoločnosť, a. s.',
                'address_2' => 'Trnavská 50/B',
                'address_3' => '821 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => '365.life, d.s.s., a.s.',
                'address_1' => '365.life, d.s.s., a.s.',
                'address_2' => 'Dvořákovo nábrežie 4',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'VÚB Generali, d.s.s., a.s.',
                'address_1' => 'VÚB Generali dôchodková správcovská spoločnosť, a.s.',
                'address_2' => 'Mlynské nivy 1',
                'address_3' => '820 04 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'AXA d.d.s., a.s.',
                'address_1' => 'AXA d.d.s., a.s.',
                'address_2' => 'Kolárska 6',
                'address_3' => '811 06 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Doplnková dôchodková spoločnosť Tatra banky, a.s.',
                'address_1' => 'Doplnková dôchodková spoločnosť Tatra banky, a.s.',
                'address_2' => 'P.O.BOX 59',
                'address_3' => '850 05 Bratislava 55',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'NN Tatry – Sympatia, d.d.s., a.s.',
                'address_1' => 'NN Tatry – Sympatia, d.d.s., a.s.',
                'address_2' => 'Jesenského 4/C',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Amundi Asset Management, a.s.',
                'address_1' => 'Amundi Asset Management, a.s.',
                'address_2' => 'Mýtna 48, blog G',
                'address_3' => '811 07 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Conseq Investment Management, a. s.',
                'address_1' => 'Conseq Investment Management, a. s.',
                'address_2' => 'MALROSE apartmány',
                'address_3' => 'Betliarska 12',
                'address_4' => '851 07 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'European Investment Centre, o.c.p., a.s.',
                'address_1' => 'European Investment Centre, o.c.p., a.s.',
                'address_2' => 'Tomášikova 64',
                'address_3' => '831 04 Bratislava',
                'address_4' => 'Slovak Republic',
                'address_5' => '',
            ],
            [
                'name' => 'Fincentrum, a.s.',
                'address_1' => 'Fincentrum, a.s.',
                'address_2' => 'Apollo BC II. blok H',
                'address_3' => 'Mlynské Nivy 49/II.16920',
                'address_4' => '821 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'IAD Investments, správ. spol., a.s.',
                'address_1' => 'IAD Investments, správ. spol., a.s.',
                'address_2' => 'Malý trh 2/A',
                'address_3' => '811 08 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'ROYAL Golden Group, a. s.',
                'address_1' => 'ROYAL Golden Group, a. s.',
                'address_2' => 'Ružinovská 1',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Banco Banif Mais, S. A.',
                'address_1' => 'Banco Banif Mais, S. A.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Einsteinova 21',
                'address_4' => '851 01 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'BKS Bank AG',
                'address_1' => 'BKS Bank AG',
                'address_2' => 'pobočka zahraničnej banky v SR',
                'address_3' => 'Pribinova 4',
                'address_4' => '811 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Citibank Europe plc',
                'address_1' => 'Citibank Europe plc',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Mlynské nivy 43',
                'address_4' => '825 01 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'COMMERZBANK Aktiengesellschaft',
                'address_1' => 'COMMERZBANK Aktiengesellschaft',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Rajská 15/A',
                'address_4' => '811 06 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Československá obchodná banka, a.s.',
                'address_1' => 'Československá obchodná banka, a.s.',
                'address_2' => 'Michalská 18',
                'address_3' => '815 63 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'ČSOB stavebná sporiteľňa, a. s.',
                'address_1' => 'ČSOB stavebná sporiteľňa, a. s.',
                'address_2' => 'Radlinského 10',
                'address_3' => '813 23 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Fio banka, a.s.',
                'address_1' => 'Fio banka, a.s.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Nám. SNP 21',
                'address_4' => '811 01 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'ING Bank N. V.',
                'address_1' => 'ING Bank N. V.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Jesenského 4/C',
                'address_4' => '811 02 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'J&T BANKA, a. s.',
                'address_1' => 'J&T BANKA, a. s.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Dvořákovo nábrežie 8',
                'address_4' => '811 02 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'KDB Bank Europe Ltd.',
                'address_1' => 'KDB Bank Europe Ltd.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Obchodná 2',
                'address_4' => '811 06 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Komerční banka, a.s.',
                'address_1' => 'Komerční banka, a.s.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Hodžovo námestie 1A',
                'address_4' => '811 06 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'mBank S.A.',
                'address_1' => 'mBank S.A.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Pribinova 10',
                'address_4' => '811 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Oberbank AG',
                'address_1' => 'Oberbank AG',
                'address_2' => 'pobočka zahraničnej banky v SR',
                'address_3' => 'Prievozská 4/A',
                'address_4' => '821 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'OTP Banka Slovensko, a. s.',
                'address_1' => 'OTP Banka Slovensko, a. s.',
                'address_2' => 'Štúrova 5',
                'address_3' => '813 54 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => '365.bank, a.s.',
                'address_1' => '365.bank, a.s.',
                'address_2' => 'Dvořákovo nábrežie 4',
                'address_3' => '811 02 Bratislava',
                'address_4' => 'budova River Park, Blok 4',
                'address_5' => '',
            ],
            [
                'name' => 'Prima banka Slovensko, a. s.',
                'address_1' => 'Prima banka Slovensko, a. s.',
                'address_2' => 'Hodžova 11',
                'address_3' => '010 11 Žilina',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Privatbanka, a. s.',
                'address_1' => 'Privatbanka, a. s.',
                'address_2' => 'Eisteinova 25',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Prvá stavebná sporiteľňa, a. s.',
                'address_1' => 'Prvá stavebná sporiteľňa, a. s.',
                'address_2' => 'Bajkalská 30',
                'address_3' => 'P. O. Box 48',
                'address_4' => '829 48 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Raiffeisen BANK (Tatra banka a.s., odštepný závod)',
                'address_1' => 'Raiffeisen BANK (Tatra banka a.s., odštepný závod)',
                'address_2' => 'Hodžovo námestie 3',
                'address_3' => '811 06 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Sberbank Slovensko, a. s.',
                'address_1' => 'Sberbank Slovensko, a. s.',
                'address_2' => 'Vysoká 9',
                'address_3' => '810 00 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Slovenská sporiteľňa, a. s.',
                'address_1' => 'Slovenská sporiteľňa, a. s.',
                'address_2' => 'Tomášikova 48',
                'address_3' => '832 37 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Slovenská záručná a rozvojová banka, a. s.',
                'address_1' => 'Slovenská záručná a rozvojová banka, a. s.',
                'address_2' => 'Štefánikova 27',
                'address_3' => '814 99 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Tatra banka, a. s.',
                'address_1' => 'Tatra banka, a. s.',
                'address_2' => 'Hodžovo námestie 3',
                'address_3' => '811 06 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'The Royal Bank of Scotland N. V.',
                'address_1' => 'The Royal Bank of Scotland N. V.',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Hviezdoslavovo nám. 25',
                'address_4' => '811 02 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'UniCredit Bank Czech Republic and Slovakia, a. s.',
                'address_1' => 'UniCredit Bank Czech Republic and Slovakia, a. s.',
                'address_2' => 'pobočka zahr. banky',
                'address_3' => 'Šancová 1/A',
                'address_4' => '813 33 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Všeobecná úverová banka, a. s.',
                'address_1' => 'Všeobecná úverová banka, a. s.',
                'address_2' => 'Mlynské nivy 1',
                'address_3' => '829 90 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Wüstenrot stavebná sporiteľňa, a. s.',
                'address_1' => 'Wüstenrot stavebná sporiteľňa, a. s.',
                'address_2' => 'Grösslingova 77',
                'address_3' => '824 68 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'ZUNO BANK AG',
                'address_1' => 'ZUNO BANK AG',
                'address_2' => 'pobočka zahraničnej banky',
                'address_3' => 'Digital Park II',
                'address_4' => 'Einsteinova 23',
                'address_5' => '851 01 Bratislava',
            ],
            [
                'name' => 'COFIDIS, a. s.',
                'address_1' => 'COFIDIS, a. s.',
                'address_2' => 'Einsteinova ulica 11/3677',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Consumer Finance Holding, a.s.',
                'address_1' => 'Consumer Finance Holding, a.s.',
                'address_2' => 'Hlavné námestie 12',
                'address_3' => '060 01 Kežmarok 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Diners Club CS, s.r.o.',
                'address_1' => 'Diners Club CS, s.r.o.',
                'address_2' => 'Námestie Slobody 11',
                'address_3' => '811 06 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Fair Credit Slovakia, SE',
                'address_1' => 'Fair Credit Slovakia, SE',
                'address_2' => 'Mlynské nivy 48',
                'address_3' => '821 09 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'FINANC PARTNERS s. r. o.',
                'address_1' => 'FINANC PARTNERS s. r. o.',
                'address_2' => 'Jesenského 230/7',
                'address_3' => '958 01 Partizánskea',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Friendly Finance Slovakia, s.r.o.',
                'address_1' => 'Friendly Finance Slovakia, s.r.o.',
                'address_2' => 'Tallerova 47',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'HB REAVIS Finance SK s. r. o.',
                'address_1' => 'HB REAVIS Finance SK s. r. o.',
                'address_2' => 'Karadžičova 12',
                'address_3' => '821 08 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Home Credit Slovakia, a.s.',
                'address_1' => 'Home Credit Slovakia, a.s.',
                'address_2' => 'Teplická 7434/147',
                'address_3' => '921 22 Piešťany 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Intrum Justitia Slovakia, s. r. o.',
                'address_1' => 'Intrum Justitia Slovakia, s. r. o.',
                'address_2' => 'Karadžičova 8/A',
                'address_3' => '824 68 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'J&T Global Finance III, s. r. o.',
                'address_1' => 'J&T Global Finance III, s. r. o.',
                'address_2' => 'Dvořákovo nábrežie 8',
                'address_3' => '811 02 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Penta Funding, a. s.',
                'address_1' => 'Penta Funding, a. s.',
                'address_2' => 'Einsteinova 25',
                'address_3' => '851 01 Bratislava 5',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'POHOTOVOSŤ, s.r.o.',
                'address_1' => 'POHOTOVOSŤ, s.r.o.',
                'address_2' => 'Pribinova 25',
                'address_3' => '811 09 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'PROFI CREDIT Slovakia, s.r.o.',
                'address_1' => 'PROFI CREDIT Slovakia, s.r.o.',
                'address_2' => 'Pribinova 25',
                'address_3' => '824 96 Bratislava 26',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Provident Financial, s.r.o.',
                'address_1' => 'Provident Financial, s.r.o.',
                'address_2' => 'Mlynské nivy 49',
                'address_3' => '821 09 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'RMS Mezzanine, a.s., organizačná zložka Slovensko',
                'address_1' => 'RMS Mezzanine, a.s., organizačná zložka Slovensko',
                'address_2' => 'Dvořákovo nábrežie 10',
                'address_3' => '811 02 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'TELERVIS PLUS a.s.',
                'address_1' => 'TELERVIS PLUS a.s.',
                'address_2' => 'Staré Grunty 7',
                'address_3' => '841 04 Bratislava 4',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'BKS-Leasing s. r. o.',
                'address_1' => 'BKS-Leasing s. r. o.',
                'address_2' => 'Pribinova 4',
                'address_3' => '811 09 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'BPT LEASING, a.s.',
                'address_1' => 'BPT LEASING, a.s.',
                'address_2' => 'Drieňová 34',
                'address_3' => '821 02 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'ČSOB Leasing, a.s.',
                'address_1' => 'ČSOB Leasing, a.s.',
                'address_2' => 'Panónska cesta 11',
                'address_3' => '852 01 Bratislava 5',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Deutsche Leasing Slovakia, spol. s r.o.',
                'address_1' => 'Deutsche Leasing Slovakia, spol. s r.o.',
                'address_2' => 'Prievozská 4B',
                'address_3' => '821 09 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'GRENKELEASING s. r. o.',
                'address_1' => 'GRENKELEASING s. r. o.',
                'address_2' => 'Karadžičova 8/A',
                'address_3' => '821 08 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'IKB Leasing SR, s.r.o.',
                'address_1' => 'IKB Leasing SR, s.r.o.',
                'address_2' => 'Plynárenská 1',
                'address_3' => '821 09 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'IMPULS-LEASING Slovakia s.r.o.',
                'address_1' => 'IMPULS-LEASING Slovakia s.r.o.',
                'address_2' => 'Štetinová 4',
                'address_3' => '811 06 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Mercedes-Benz Financial Services Slovakia, s.r.o.',
                'address_1' => 'Mercedes-Benz Financial Services Slovakia, s.r.o.',
                'address_2' => 'Tuhovská 11',
                'address_3' => '831 07 Bratislava 36',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Oberbank Leasing s.r.o.',
                'address_1' => 'Oberbank Leasing s.r.o.',
                'address_2' => 'Prievozská 4',
                'address_3' => '821 09 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'PSA FINANCE SLOVAKIA,s.r.o.',
                'address_1' => 'PSA FINANCE SLOVAKIA,s.r.o.',
                'address_2' => 'Prievozská 4/C',
                'address_3' => '822 09 Bratislava 3',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 's Autoleasing SK, s. r. o.',
                'address_1' => 's Autoleasing SK, s. r. o.',
                'address_2' => 'Vajnorská 100/A',
                'address_3' => '831 04 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'S Slovensko, spol. s r.o.',
                'address_1' => 'S Slovensko, spol. s r.o.',
                'address_2' => 'Tomášikova 17',
                'address_3' => '821 02 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Scania Finance Slovak Republic s. r. o.',
                'address_1' => 'Scania Finance Slovak Republic s. r. o.',
                'address_2' => 'Diaľničná cesta 4570/2A',
                'address_3' => '903 01 Senec',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'SG Equipment Finance Czech Republic s.r.o.',
                'address_1' => 'SG Equipment Finance Czech Republic s.r.o.',
                'address_2' => 'organizačná zložka',
                'address_3' => 'Hodžovo námestie 1a',
                'address_4' => '811 06 Bratislava 1',
                'address_5' => '',
            ],
            [
                'name' => 'Tatra-Leasing, s.r.o.',
                'address_1' => 'Tatra-Leasing, s.r.o.',
                'address_2' => 'Černyševského 50',
                'address_3' => '851 01 Bratislava 5',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Toyota Financial Services Slovakia s.r.o.',
                'address_1' => 'Toyota Financial Services Slovakia s.r.o.',
                'address_2' => 'Gagarinova 7/C',
                'address_3' => '821 03 Bratislava 2',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UniCredit Leasing Slovakia, a.s.',
                'address_1' => 'UniCredit Leasing Slovakia, a.s.',
                'address_2' => 'Plynárenská 7/A',
                'address_3' => '814 16 Bratislava 1',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'VFS Financial Services Slovakia s.r.o.',
                'address_1' => 'VFS Financial Services Slovakia s.r.o.',
                'address_2' => 'Diaľničná cesta 9',
                'address_3' => '903 01 Senec',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'VOLKSWAGEN Finančné služby Slovensko s.r.o.',
                'address_1' => 'VOLKSWAGEN Finančné služby Slovensko s.r.o.',
                'address_2' => 'Vajnorská 98',
                'address_3' => '831 04 Bratislava 3',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'VÚB Leasing, a. s.',
                'address_1' => 'VÚB Leasing, a. s.',
                'address_2' => 'Mlynské nivy 1',
                'address_3' => '820 05 Bratislava 25',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UNIQA investiční společnost, a.s., organizačná zložka Slovensko',
                'address_1' => 'UNIQA investiční společnost, a.s., organizačná zložka Slovensko',
                'address_2' => 'Krasovského 3986/15',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UNIQA d.d.s., a.s.',
                'address_1' => 'UNIQA d.d.s., a.s.',
                'address_2' => 'Krasovského 3986/15',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'UNIQA d.s.s., a.s',
                'address_1' => 'UNIQA d.s.s., a.s',
                'address_2' => 'Krasovského 3986/15',
                'address_3' => '851 01 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => '365.invest, správ. spol., a.s.',
                'address_1' => '365.invest, správ. spol., a.s.',
                'address_2' => 'Dvořákovo nábrežie 4',
                'address_3' => '811 02 Bratislava',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'YOUPLUS Životná poisťovňa, pobočka poisťovne z iného členského štátu Nivy Tower',
                'address_1' => 'YOUPLUS Životná poisťovňa, pobočka poisťovne z iného členského štátu Nivy Tower',
                'address_2' => 'Mlynské Nivy 5',
                'address_3' => '821 09 Bratislava – mestská časť Ružinov',
                'address_4' => '',
                'address_5' => '',
            ],
            [
                'name' => 'Swiss Life Select Slovensko, a.s.',
                'address_1' => 'Swiss Life Select Slovensko, a.s.',
                'address_2' => 'Green Point Offices blok H',
                'address_3' => 'Mlynské Nivy 49/II.16920',
                'address_4' => '821 09 Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'European Investment Centre, o.c.p., a.s.',
                'address_1' => 'European Investment Centre, o.c.p., a.s.',
                'address_2' => 'Vajnorská 100/B',
                'address_3' => '831 04',
                'address_4' => 'Bratislava',
                'address_5' => '',
            ],
            [
                'name' => 'Investika',
                'address_1' => 'Investika',
                'address_2' => 'investiční společnost, a.s.',
                'address_3' => 'U Zvonařky 291/3',
                'address_4' => '120 00',
                'address_5' => 'Praha 2',
            ],
            [
                'name' => 'Wealth Effect Management o.c.p., a.s.',
                'address_1' => 'Wealth Effect Management o.c.p., a.s.',
                'address_2' => 'Bottova 2A',
                'address_3' => '811 09',
                'address_4' => 'Bratislava',
                'address_5' => '',
            ],
        ];
    }
    
}
