<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ImportRequestTemplate extends Migration
{

    public function up()
    {
        DB::table('requests_template')->insert($this->getData());
    }

    public function down()
    {

    }

    public function getData(): array
    {
        return [
            [
                "typ" => "Čestné prehlásenie klienta - otázka dôchodku",
                "vec" => "VEC: Čestné prehlásenie",
                "body" => "<p>\r\nČestne a dobrovoľne prehlasujem, že mi boli odprezentované zo strany makléra(príjemcu) podstatné náležitosti ohľadom tvorby rezervy na dôchodok, ako aj potreby systematického odkladania symbolickej sumy pre potreby svojho života v dôchodkovom veku.\r\n</p>\r\n\r\n<p>\r\nSom si ako klient vedomý, že aj napriek potrebe vnímania dôchodku ako skutočnosti, ktorú neovplyvním, rovnako ako aj zabezpečenie výplaty dôchodku zo strany tretej strany nepovažujem za dôležité v súčasnosti vnímať otázku svojho dôchodku.\r\n</p>\r\n\r\n<p>\r\nSvojím podpisom potvrdzujem, že až do odvolania nechcem byť kontaktovaný ohľadom otázky svojho dôchodku a financiami s ním spojenými.\r\n</p>"
            ],
            [
                "typ" => "Čestné prehlásenie o zdravotnom stave",
                "vec" => "VEC: Čestné prehlásenie",
                "body" => "<p>\r\nČestne prehlasujem, že v minulosti som absolvovala vyšetrenie ................................ uvedené v lek.správe, ktorú som Vám dala k dispozícii na predbežné ocenenie zdravotného stavu. Novšiu správu nemám, pretože lekár v tom čase po zhodnotení výsledkov vyšetrenia skonštatoval, že žiadne následné vyšetrenia nie sú potrebné.\r\n</p>\r\n\r\n<p>\r\nOd posledného vyšetrenia - výsledky ktorého ste videli v lekárkej správe, som ďalšie vyšetrenie neabsolvovala a ani nie som objednaná na žiadne konkrétne vyšetrenie. Takže lekársku správu, ktorú odomňa požadujete doložiť nemám možnosť. Prosím teda o predbežné ocenenie zdravotného stavu v súlade s týmto čestným prehlásením.\r\n</p>"
            ],
            [
                "typ" => "Čestné prehlásenie - zabezpečenie klienta a jeho rodiny",
                "vec" => "VEC: Čestné prehlásenie",
                "body" => "<p>\r\nČestne a dobrovoľne prehlasujem, že mi boli odprezentované zo strany makléra(príjemcu) podstatné náležitosti ohľadom zabezpečenie seba a svojej rodiny, jednotlivých rizík, ako aj smrti a zabezpečenie úveru.\r\n</p>\r\n\r\n<p>\r\nSom si ako klient vedomý, že aj napriek potrebe vnímania rizík a skutočnosti, ktorú neovplyvním, rovnako ako aj zabezpečenie svojej rodiny nepovažujem za dôležité v súčasnosti vnímať otázku krytia.\r\n</p>\r\n\r\n<p>\r\nSvojím podpisom potvrdzujem, že až do odvolania nechcem byť kontaktovaný ohľadom otázky zabezpečenia seba a svojej rodiny a otázok s tým spojenými.\r\n</p>"
            ],
            [
                "typ" => "Informatívne vyčíslenie aktuálnej hodnoty účtu a odkupnej hodnoty",
                "vec" => "VEC: Žiadosť o informatívne vyčíslenie aktuálnej hodnoty účtu a odkupnej hodnoty",
                "body" => "<p>\r\nNa hore uvedenej PZ žiadam o informatívne vyčíslenie aktuálnej hodnoty účtu a odkupnej hodnoty (vyplatenej hodnoty pri predčasnom ukončení PZ v najbližšom možnom období).\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu.\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy - vrátenie bielej karty",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy - vrátenie bielej karty",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu. K žiadosti o výpoveď prikladám aj bielu kartu, ktorú ste mi zaslali na ďalšie poistné obdobie, v ktorom už nebudem využívať Vaše služby.\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy - zmena majitela vozidla (alikvotna čiastka - účet)",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy - zmena majitela vozidla",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu z dôvodu predaja motorového vozidla.\r\n</p>\r\n\r\n<p>\r\nK žiadosti o výpoveď prikladám aj kópiu pôvodného veľkého technického preukazu ako doklad o zmene majiteľa motorového vozidla. Zároveň žiadam o zaslanie alikvotnej čiastky na číslo účtu: ...............................................\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy - zmena majitela vozidla (alikvotna čiastka - adresa",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy - zmena majitela vozidla",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu z dôvodu predaja motorového vozidla.\r\n</p>\r\n\r\n<p>\r\nK žiadosti o výpoveď prikladám aj kópiu pôvodného veľkého technického preukazu ako doklad o zmene majiteľa motorového vozidla. Zároveň žiadam o zaslanie alikvotnej čiastky na hore uvedenú adresu.\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy s vyplatením odkupnej hodnoty na adresu",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu a vyplatenie odkupnej hodnoty na adresu poistníka.\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy s vyplatením odkupnej hodnoty na adresu(*)",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy",
                "body" => "<p>\r\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu a vyplatenie odkupnej hodnoty na adresu poistníka.\r\n</p>\r\n\r\n<p>\r\nV prípade vzniku dlžnej sumy prosím dlžnú sumu odrátať z odkupnej hodnoty poistníka.\r\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy s vyplatením odkupnej hodnoty na bankový účet",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy",
                "body" => "<p>\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu a vyplatenie odkupnej hodnoty na bankový účet: {cislo_uctu}\n</p>"
            ],
            [
                "typ" => "Výpoveď poistnej zmluvy s vyplatením odkupnej hodnoty na bankový účet(*)",
                "vec" => "VEC: Žiadosť o výpoveď poistnej zmluvy",
                "body" => "<p>\nŽiadam o výpoveď hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu a vyplatenie odkupnej hodnoty na bankový účet: {cislo_uctu}\n</p>\n\n<p>\nV prípade vzniku dlžnej sumy prosím dlžnú sumu odrátať z odkupnej hodnoty poistníka.\n</p>"
            ],
            [
                "typ" => "Vyhotovenie duplikátu - mám číslo zmluvy",
                "vec" => "VEC: Žiadosť o vyhotovenie duplikátu",
                "body" => "<p>\r\nŽiadam o vyhotovenie duplikátu hore uvedenej zmluvy a následne jeho zaslanie na adresu klienta.\r\n</p>"
            ],
            [
                "typ" => "Vyhotovenie duplikátu - nemám číslo zmluvy",
                "vec" => "VEC: Žiadosť o vyhotovenie duplikátu",
                "body" => "<p>\r\nŽiadam o vyhotovenie duplikátov všetkých zmlúv, ktoré mám uzatvorené s Vašou spoločnosťou a následne ich zaslanie na hore uvedenú adresu.\r\n</p>"
            ],
            [
                "typ" => "Zmena dokladu totožnosti",
                "vec" => "VEC: Oznamenie zmeny dokladu totožnosti",
                "body" => "<p>\r\nOznamujem že nastala zmena dokladu totožnosti. Do obálky prikladám kópiu aktuálneho dokladu totožnosti.\r\n</p>"
            ],
            [
                "typ" => "Zmena poistenia na poistenie v splatenom stave(redukcia poistnej sumy)",
                "vec" => "VEC: Žiadosť o zmenu poistenia na poistenie v splatenom stave",
                "body" => "<p>\r\nŽiadam o odpoistenie všetkých pripoistení a zmenu poistenia na poistenie so zníženou poistnou sumou (redukcia poistnej sumy), bez povinnosti platiť ďalšie poistné.\r\n</p>"
            ],
            [
                "typ" => "Zmena trvalého pobytu a dokladu totožnosti",
                "vec" => "VEC: Oznamenie zmeny trvalého pobytu a dokladu totožnosti",
                "body" => "<p>\r\nOznamujem že nastala zmena trvalého pobytu a dokladu totožnosti. Do obálky prikladám kópiu aktuálneho dokladu totožnosti.\r\n</p>"
            ],
            [
                "typ" => "Žiadosť o vrátenie preplatku",
                "vec" => "VEC: Žiadosť o vrátenie preplatku",
                "body" => "<p>\r\nŽiadam o vrátenie preplatku na horeuvedenej poistnej zmluve na účet, z ktorého platím pravidelné bežné poistné. Prosím o odpoveď, či ste mi preplatok zaslali podľa mojej požiadavky.\r\n</p>"
            ],
            [
                "typ" => "Žiadosť o zálohové plnenie PN",
                "vec" => "VEC: Žiadosť o zálohové poistné plnenie v rámci pripoistenia PN",
                "body" => "<p>\r\nŽiadam o zálohové poistné plnenie v rámci pripoistenia PN, nakoľko som vypísaná na PN a aj podľa priloženej dokumentácie na PN budem zrejme dlhšie obdobie.\r\n</p>\r\n\r\n<p>\r\nPoistné plnenie prosím poukázať na moje číslo účtu: ............................................., ktoré je uvedené aj v návrhu zmluvy.\r\n</p>"
            ],
            [
                "typ" => "Žiadosť o zmenu sprostredkovateľa",
                "vec" => "VEC: Žiadosť o zmenu sprostredkovateľa",
                "body" => "<p>\r\nŽiadam o zmenu sprostredovateľa hore uvedenej poistnej zmluvy k najbližšiemu možnému dátumu. Žiadam, aby moja zmluva bola v správe sprostredkovateľa, konkrétne ............................................., osobné číslo ............................................., registračné číslo v NBS:............................................. - spoločnosť Fincentrum a.s.\r\n</p>"
            ],
            [
                "typ" => "Žiadosť o zrušenie indexácie",
                "vec" => "VEC: Žiadosť o zrušenie indexácie na poistnej zmluve",
                "body" => "<p>\r\nŽiadam o zrušenie indexácie na poistnej zmluve k najbližšiemu možnému dátumu.\r\n</p>"
            ]
        ];
    }
}
