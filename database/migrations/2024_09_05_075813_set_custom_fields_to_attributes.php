<?php

use App\Models\GlobalAttribute;
use Illuminate\Database\Migrations\Migration;

class SetCustomFieldsToAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new GlobalAttribute();
        $model->type = 'custom_fields_main_db'; 
        $model->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
