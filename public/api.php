<?php
header('Access-Control-Allow-Origin: *');

require './config.php';

$conn = sql_connect();
if($conn){

    if(isset($_GET['username']))     $username = $_GET['username'];     else $username = 'undefined';
    if(isset($_GET['password']))     $password = $_GET['password'];     else $password = 'undefined';
    if(isset($_GET['action']))     $action = $_GET['action'];     else $action = 'undefined';
    if(isset($_GET['names']))     $names = $_GET['names'];     else $names = 'undefined';
    if(isset($_GET['key_service']))     $key_service = $_GET['key_service'];     else $key_service = 'undefined';

    if(isset($_GET['id_ziadost']))     $id_ziadost = $_GET['id_ziadost'];     else $id_ziadost = 'undefined';
    if(isset($_GET['ziadost_link']))     $ziadost_link = $_GET['ziadost_link'];     else $ziadost_link = 'undefined';

    if(isset($_GET['id_person']))     $id_person = $_GET['id_person'];     else $id_person = 'undefined';
    if(isset($_GET['id_fp']))     $id_fp = $_GET['id_fp'];     else $id_fp = 'undefined';
	if(isset($_GET['id_zapis']))     $id_zapis = $_GET['id_zapis'];     else $id_zapis = 'undefined';

    $action_status = 'unknown';

    $array_nar = array(); $array_nar_io = array();
    $array_men = array(); $array_men_io = array();
    $array_ser = array(); $array_ser_io = array();
    $array_zml_klient = array();
    $array_zml_makler = array();
    $array_sync = array();
    $array_sync_io = array();
    $array_ziadost = array();

    //====================================================================
    // function save history
    //====================================================================
    function save_history($data, $conn, $servis_action){ 
        $length = count($data);
        for($i = 0; $i < $length; $i++){
            $fetch = mysqli_query($conn, "
            insert into servis_h (
                id_user,
                name_full, 
                email,
                akcia,
                id_person
                ) 
            values(
                "."'".$data[$i]['id_user']."'".", 
                "."'".$data[$i]['name_full']."'".", 
                "."'".$data[$i]['email']."'".",
                "."'".$servis_action."'".", 
                "."'".$data[$i]['id_person']."'"."
                )    
            ");
        }
    }

    //====================================================================
    // function save history - zmluvy
    //====================================================================
    function save_history_zmluvy($data, $conn, $servis_action){ 
        $length = count($data);
        for($i = 0; $i < $length; $i++){
            $fetch = mysqli_query($conn, "
            insert into servis_h (
                id_user,
                name_full, 
                email,
                akcia,
                id_person
                ) 
            values(
                "."'".$data[$i]['id_user']."'".", 
                "."'".$data[$i]['name_full']."'".", 
                "."'".$data[$i]['email']."'".",
                "."'".$servis_action."'".", 
                "."'".$data[$i]['id_osoba']."'"."
                )    
            ");
        }
    }

    //====================================================================
    // check access
    //====================================================================
    $pass = $password;
    $fetch = mysqli_query($conn, "
        select * from users
        where key_service = '$key_service'
        limit 1
    ");
    $user = $fetch->fetch_assoc();

    if(isset($user['key_service'])){  
        $access = 'true';
        $username = $user['email'];
    } else $access = 'false';


    //====================================================================
    // app_login
    //====================================================================
		if($action == 'app_login'){
			$email = ''; if(isset($_GET['username'])) $email = $_GET['username'];
			$pass = ''; if(isset($_GET['password'])) $pass = $_GET['password'];

			$fetch = mysqli_query($conn, "
					SELECT * from users
					WHERE email = '$email'
			");
			if($fetch->num_rows > 0){
					$user = $fetch->fetch_assoc();

					$pass_dat = decrypt($user['pass']);
					if($pass != $pass_dat){
							die(json_encode(array('access' => 'false')));
					}

					$date_exp = $user['exp_date'];
					$date_now = date('Y-m-d');
					
					$access_app = $user['app_access'];

					if($date_exp >= $date_now){
							die(json_encode(array('access' => 'true', 'access_app' => $access_app, "username" => "$email")));
					} else{
							die(json_encode(array('access' => 'expire')));
					}
			}
			else die(json_encode(array('access' => 'false')));
		}

    //====================================================================
    // get_notification_data
    //====================================================================
    if($access == 'true'){        
        if($action == 'get_notification_data'){
            $action_status = 'true';

            //====================================================================
            // narodeniny
            //====================================================================
                $result = mysqli_query($conn, "
                    SELECT * FROM main_db    
                    WHERE DAY(datum_narodenia) = DAY(CURDATE())
                    AND MONTH(datum_narodenia) = MONTH(CURDATE())
                    AND id_user = '$username'
                    AND func_narodeniny_meniny = 'áno'
                    AND check_delete != 'ok'
                "); 
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_nar[] = $row;
                    }
                }            
                if(count($array_nar) > 0){ // index++, reset
                    $length = count($array_nar);     
                    for($i = 0; $i < $length; $i++){                        
                        $result = mysqli_query($conn, " 
                            UPDATE main_db
                            SET index_narodeniny = index_narodeniny + 1
                            WHERE id = ". $array_nar[$i]['id'] ."
                        ");
                        $result = mysqli_query($conn, "
                            UPDATE main_db
                            SET index_narodeniny = 0
                            WHERE id = ". $array_nar[$i]['id'] ."
                            AND index_narodeniny > 4
                        ");
                    }
                    save_history($array_nar, $conn, 'Narodeninový email');
                }

            //====================================================================
            // narodeniny io
            //====================================================================
                 $result = mysqli_query($conn, "
                    SELECT * FROM main_db_io    
                    WHERE DAY(datum_narodenia) = DAY(CURDATE())
                    AND MONTH(datum_narodenia) = MONTH(CURDATE())
                    AND id_user = '$username'
                    AND func_narodeniny_meniny = 'áno'
                    AND check_delete != 'ok'
                "); 
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_nar_io[] = $row;
                    }
                }            
                if(count($array_nar_io) > 0){ // index++, reset
                    $length = count($array_nar_io);     
                    for($i = 0; $i < $length; $i++){                        
                        $result = mysqli_query($conn, " 
                            UPDATE main_db_io
                            SET index_narodeniny = index_narodeniny + 1
                            WHERE id = ". $array_nar_io[$i]['id'] ."
                        ");
                        $result = mysqli_query($conn, "
                            UPDATE main_db_io
                            SET index_narodeniny = 0
                            WHERE id = ". $array_nar_io[$i]['id'] ."
                            AND index_narodeniny > 4
                        ");
                    }
                    save_history($array_nar_io, $conn, 'Narodeninový email');
                }
            
            //====================================================================
            // meniny
            //====================================================================
                $names_array = explode('/', $names);
                $names_string = implode("','", $names_array);
                $result = mysqli_query($conn, "
                    SELECT * FROM main_db
                    WHERE id_user = '$username'
                    AND func_narodeniny_meniny = 'áno'
                    AND check_delete != 'ok'
                    AND name_first IN ('$names_string')
                ");
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_men[] = $row;
                    }
                }
                if(count($array_men) > 0){ // index++, reset
                    $length = count($array_men);     
                    for($i = 0; $i < $length; $i++){                        
                        $result = mysqli_query($conn, " 
                            UPDATE main_db
                            SET index_meniny = index_meniny + 1
                            WHERE id = ". $array_men[$i]['id'] ."
                        ");                  
                        $result = mysqli_query($conn, "
                            UPDATE main_db
                            SET index_meniny = 0
                            WHERE id = ". $array_men[$i]['id'] ."
                            AND index_meniny > 4
                        ");
                    }
                    save_history($array_men, $conn, 'Meninový email');  
                }

            //====================================================================
            // meniny io
            //====================================================================
                $names_array = explode('/', $names);
                $names_string = implode("','", $names_array);
                $result = mysqli_query($conn, "
                    SELECT * FROM main_db_io
                    WHERE id_user = '$username'
                    AND func_narodeniny_meniny = 'áno'
                    AND check_delete != 'ok'
                    AND name_first IN ('$names_string')
                ");
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_men_io[] = $row;
                    }
                }
                if(count($array_men_io) > 0){ // index++, reset
                    $length = count($array_men_io);     
                    for($i = 0; $i < $length; $i++){                        
                        $result = mysqli_query($conn, " 
                            UPDATE main_db_io
                            SET index_meniny = index_meniny + 1
                            WHERE id = ". $array_men_io[$i]['id'] ."
                        ");                  
                        $result = mysqli_query($conn, "
                            UPDATE main_db_io
                            SET index_meniny = 0
                            WHERE id = ". $array_men_io[$i]['id'] ."
                            AND index_meniny > 4
                        ");
                    } 
                    save_history($array_men_io, $conn, 'Meninový email'); 
                }

            //====================================================================
            // servis
            //====================================================================
                $result = mysqli_query($conn, "
                    UPDATE main_db
                    SET index_servis = index_servis + 1
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                ");
                $result = mysqli_query($conn, "
                    UPDATE main_db
                    SET index_servis = 0
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                    AND index_servis > func_servis_email
                ");
                $result = mysqli_query($conn, "
                    SELECT * FROM main_db
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                    AND index_servis = func_servis_email
                ");
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_ser[] = $row;
                    }
                    save_history($array_ser, $conn, 'Servisný email');
                }        
            
            //====================================================================
            // servis io
            //====================================================================
                $result = mysqli_query($conn, "
                    UPDATE main_db_io
                    SET index_servis = index_servis + 1
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                ");
                $result = mysqli_query($conn, "
                    UPDATE main_db_io
                    SET index_servis = 0
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                    AND index_servis > func_servis_email
                ");
                $result = mysqli_query($conn, "
                    SELECT * FROM main_db_io
                    WHERE id_user = '$username'
                    AND func_servis_email > 0
                    AND index_servis = func_servis_email
                ");
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_ser_io[] = $row;
                    }
                    save_history($array_ser_io, $conn, 'Servisný email');
                } 

            //====================================================================
            // zmluvy - upozornenie klient
            //====================================================================
                $result = mysqli_query($conn, "
                    SELECT *, MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) / 7 as ltt FROM zmluvy as t
                    WHERE id_user = '$username'
                    and date_start > '0000-00-00'
                    and length(date_start) > 9  
                    and date_start != 'NaN.NaN.NaN'
                    and check_delete != 'ok'                    
                    and MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) = t.pocet_tyzdnov * 7
                    and upozornenie_klient = 'áno'
                    ORDER BY ltt asc
                ");
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        $array_zml_klient[] = $row;
                    }
                    save_history_zmluvy($array_zml_klient, $conn, 'Upozornenie - Výročie zmluvy (klient)');
                }

            //====================================================================
            // zmluvy - upozornenie makler
            //====================================================================
                $result = mysqli_query($conn, "
                    SELECT *, MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) / 7 as ltt FROM zmluvy as t
                    WHERE id_user = '$username'
                    and date_start > '0000-00-00'
                    and length(date_start) > 9  
                    and date_start != 'NaN.NaN.NaN'
                    and check_delete != 'ok'                    
                    and MOD(DAYOFYEAR(t.date_start) - DAYOFYEAR(NOW()) + 365, 365) = t.pocet_tyzdnov * 7
                    and upozornenie_makler =  'áno'
                    ORDER BY ltt asc
            ");
            if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_assoc($result)){
                    $array_zml_makler[] = $row;
                }
                save_history_zmluvy($array_zml_makler, $conn, 'Upozornenie - Výročie zmluvy (maklér)');
            }

            //====================================================================
            // google sync
            //====================================================================
            $result = mysqli_query($conn, "
                SELECT * from `main_db` 
                WHERE id_user = '$username'
                and android_sync = 'áno'
            ");
            if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_assoc($result)){
                    $array_sync[] = $row;
                }
            }
            if(count($array_sync) > 0){
                $length = count($array_sync);     
                for($i = 0; $i < $length; $i++){                        
                    $result = mysqli_query($conn, " 
                        UPDATE main_db
                        SET android_sync = 'KL - Uložený v telefóne'
                        WHERE id = ". $array_sync[$i]['id'] ."
                    ");
                }
                save_history($array_sync, $conn, 'Synchronizácia s kontaktami google');
            }

            //====================================================================
            // google sync io
            //====================================================================
            $result = mysqli_query($conn, "
                SELECT * from `main_db_io` 
                WHERE id_user = '$username'
                and android_sync = 'áno'
            ");
            if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_assoc($result)){
                    $array_sync_io[] = $row;
                }
            }
            if(count($array_sync_io) > 0){
                $length = count($array_sync_io);     
                for($i = 0; $i < $length; $i++){                        
                    $result = mysqli_query($conn, " 
                        UPDATE main_db_io
                        SET android_sync = 'KL - Uložený v telefóne'
                        WHERE id = ". $array_sync_io[$i]['id'] ."
                    ");
                }
                save_history($array_sync_io, $conn, 'Synchronizácia s kontaktami google');
            }

            die(json_encode(array(
                'conn' => 'true',
                'access' => $access,
                'action_name' => $action,
                'action_status' => $action_status,
        
                'data_nar' => $array_nar,
                'data_men' => $array_men,
                'data_ser' => $array_ser,
        
                'data_nar_io' => $array_nar_io,
                'data_men_io' => $array_men_io,  
                'data_ser_io' => $array_ser_io,

                'data_zml_klient' => $array_zml_klient,
                'data_zml_makler' => $array_zml_makler,
        
                'data_sync' => $array_sync,
                'data_sync_io' => $array_sync_io
            )));
        }
		}	

    //====================================================================
    // ziadost get data
    //====================================================================
    if($action == 'ziadost_get_data'){
        $action_status = 'true';

        $result = mysqli_query($conn, "
            SELECT * FROM ziadosti
            WHERE id_ziadost = '$id_ziadost'  
            AND check_delete != 'ok'
        "); 
        $ziadost = $result->fetch_assoc();
        if(isset($ziadost['id_ziadost'])) $array_ziadost = $ziadost;

        die(json_encode(array(
            'conn' => 'true',
            'access' => $access,
            'action_name' => $action,
            'action_status' => $action_status,    
            'data_ziadost' => $array_ziadost
        )));
    }

    //====================================================================
    // ziadost set url
    //====================================================================
    if($action == 'ziadost_set_url'){
        $action_status = 'true';

        $link_main = 'https://docs.google.com/spreadsheets/d/';
        $link_full = $link_main . $ziadost_link;
        $result = mysqli_query($conn, "
            UPDATE ziadosti
            SET link = '$link_full'
            WHERE id_ziadost = '$id_ziadost'
            AND check_delete != 'ok'
        ");
    }

    //====================================================================
    // fp set url
    //====================================================================
    if($action == 'fp_set_url'){
        $action_status = 'true';

        $link_main = 'https://docs.google.com/spreadsheets/d/';
        $link_full = $link_main . $id_fp;
        $result = mysqli_query($conn, "
            UPDATE main_db
            SET financny_plan_link = '$link_full'
            WHERE id = '$id_person'
            AND check_delete != 'ok'
        ");
    }

    //====================================================================
    // zapis set url
    //====================================================================
    if($action == 'zapis_set_url'){
        $action_status = 'true';

        $link_main = 'https://docs.google.com/document/d/';
        $link_full = $link_main . $id_zapis;
        $result = mysqli_query($conn, "
            UPDATE main_db
            SET zapis_link = '$link_full'
            WHERE id = '$id_person'
            AND check_delete != 'ok'
        ");
    }

    $array_obce = array();
    //====================================================================
    // zobrazi obce / mesta v danom rozsahu
    //====================================================================
    if($action == 'search_obce'){
        $mesto = $_GET['mesto'];
        $radius = $_GET['radius'];
        $mesto = str_replace(' ', '%', $mesto);
        $country = 'SK';
        $address = $mesto.','.$country;
        $key = 'AIzaSyBa8s4X_scgSfTCXlInAK0wgQWoWYpMnwM';
        
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$key;
        $data = file_get_contents($url);
        $jsonData = stripslashes(html_entity_decode($data));
        $array = json_decode($jsonData,true);
        
        $latitude = $array['results'][0]['geometry']['location']['lat'];
        $longitude = $array['results'][0]['geometry']['location']['lng'];
        
        $responseStyle = 'short'; // the length of the response
        $citySize = ''; // the minimal number of citizens a city must have
        $radius = $radius; // the radius in KM
        $maxRows = 5000; // the maximum number of rows to retrieve
        $username = 'peter.badinka'; // the username of your GeoNames account
        
        $url = 'http://api.geonames.org/findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.$responseStyle.'&radius='.$radius.'&maxRows='.$maxRows.'&username='.$username;
        
        $data = file_get_contents($url);
        $jsonData = stripslashes(html_entity_decode($data));
        $array = json_decode($jsonData,true);        
 
        foreach($array['geonames'] as $obec){ 
            $array_obce[] = $obec['name'];
        }
        die(json_encode(array('array' => $array_obce)));

    }

    $conn->close();
    die(json_encode(array(
        'conn' => 'true',
        'access' => $access,
        'action_name' => $action,
        'action_status' => $action_status,

        'data_nar' => $array_nar,
        'data_men' => $array_men,
        'data_ser' => $array_ser,

        'data_nar_io' => $array_nar_io,
        'data_men_io' => $array_men_io,  
        'data_ser_io' => $array_ser_io,

        'data_zml_klient' => $array_zml_klient,
        'data_zml_makler' => $array_zml_makler,

        'data_sync' => $array_sync,
        'data_sync_io' => $array_sync_io
    )));

} else {
    die(json_encode(array(
        'conn' => 'false'
    )));
}