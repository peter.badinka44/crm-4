//=======================================================================
// format date dd.mm.yyyy
//=======================================================================
function dateToDMY(date) {
	if(date == "" || date == " "){
		return "";
	}
	var datum =  new Date(date);
	var d = datum.getDate();
	var m = datum.getMonth() + 1; //Month from 0 to 11
	var y = datum.getFullYear();
	let result = '' + (d <= 9 ? '0' + d : d) + '.' + (m<=9 ? '0' + m : m) + '.' + y
	if(result == 'NaN.NaN.NaN') result = ''
	return result
}

//=======================================================================
// Date formar - YYYY-MM-DD hh:mm
//=======================================================================
function dateToDMY_hhmm(date) {
	let datum =  new Date(date)
	let min = datum.getMinutes()
	let hod = datum.getHours()
	let d = datum.getDate()
	let m = datum.getMonth() + 1; //Month from 0 to 11
	let y = datum.getFullYear()
	let result = '' + (d <= 9 ? '0' + d : d) + '.' + (m<=9 ? '0' + m : m) + '.' + y
	result += ' ' + (hod <= 9 ? '0' + hod : hod) + ':' + (min <= 9 ? '0' + min : min)
	return result			
}

//=======================================================================
// Date formar - YYYY-MM-DD hh:mm:ss
//=======================================================================
function dateToYMD_hhmmss(date) {
	let datum =  new Date(date)
	let sec = datum.getSeconds()
	let min = datum.getMinutes()
	let hod = datum.getHours()
	let d = datum.getDate()
	let m = datum.getMonth() + 1; //Month from 0 to 11
	let y = datum.getFullYear()
	let result = '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d)
	result += ' ' + (hod <= 9 ? '0' + hod : hod) + ':' + (min <= 9 ? '0' + min : min) + ':' + (sec <= 9 ? '0' + sec : sec) 
	return result			
}

//=======================================================================
// Date formar - YYYY-MM-DD
//=======================================================================
function dateToYMD(date) {
	let datum =  new Date(date)
	let d = datum.getDate()
	let m = datum.getMonth() + 1; //Month from 0 to 11
	let y = datum.getFullYear()
	let result = '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d)
	if(result == "NaN-NaN-NaN") result = "0000-00-00"
	return result			
}

//=======================================================================
// format phone
//=======================================================================
function formatPhone(str) {
	let p = str[0]
	p += str[1]
	p += str[2]
	p += str[3]
	p += "-"
	p += str[4]
	p += str[5]
	p += str[6]
	p += "-"
	p += str[7]
	p += str[8]
	p += str[9]
	return p
}