@extends('master')
@section('title', 'Aktivity')

@section('content')

<div class="p-1">	

	<div class="d-flex mb-1 border-bottom pb-1">
		<input type="date" class="form-control mr-1" v-model="dateStart" style="width: 200px;">
		<input type="date" class="form-control mr-1" v-model="dateEnd" style="width: 200px;">
		<button class="btn btn-info" @click="getData()" style="width: 150px;">Aktualizovať</button>
		<div class="ml-auto">
			@include('menu')
		</div>
	</div>

	{{-- new aktivity --}}
	<div v-if="aktivitySend == false" class="">

		<table>					
			<tr>
				<td>
					<label class="popis">Oslovené kontakty</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.oslovene_kontakty">
				</td>
				<td>
					<label class="popis">Termín studený trh</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.termin_studeny_trh">
				</td>
				<td>
					<label class="popis">Termín databáza</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.termin_databaza">
				</td>
				<td>
					<label class="popis">Termín odporúčanie</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.termin_odporucania">
				</td>
				<td>
					<label class="popis">Termín leady</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.termin_leady">
				</td>					
			</tr>
			<tr>
				<td>
					<label class="popis">Analýza studený trh</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.analyza_studeny_trh">
				</td>
				<td>
					<label class="popis">Analýza databáza</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.analyza_databaza">
				</td>
				<td>
					<label class="popis">Analýza odporúčanie</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.analyza_odporucania">
				</td>
				<td>
					<label class="popis">Analýza leady</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.analyza_leady">
				</td>						
			</tr>
			<tr>				
				<td>
					<label class="popis">Predaj</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.predaj">
				</td>
				<td>
					<label class="popis">Podpis</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.podpis">
				</td>
				<td>
					<label class="popis">Servis</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.servis">
				</td>
				<td>
					<label class="popis">Odporúčania</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.odporucania">
				</td>				
			</tr>
			<tr>
				<td>
					<label class="popis">Administratíva (počet hodín)</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.administrativa">
				</td>
				<td>
					<label class="popis">Vzdelávanie (počet hodín)</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.vzdelavanie">
				</td>
				<td>
					<label class="popis">Dovolenka</label><br>
					<input class="input" type="number" value="0" v-model="aktivityNew.dovolenka">
				</td>
			</tr>
			<tr>
				<td colspan="3" class="pt-2">
					<button class="btn btn-info" @click="sendAktivity()" style="width: 150px;">Odoslať</button>
					<a href="https://docs.google.com/spreadsheets/d/1h4RU8UaWWHHp6sgRGPWRZYQl1lieUZo1HMUtbWB_CPw" target="_blank">
						<b><u>Plán a skutočnosť BEB</u></b>
					</a>
				</td>
			</tr>
			
		</table>
		
		<hr>

	</div>	

	<div v-if="aktivity.length > 0">		
	
	{{-- table 1 --}}
	<table class="table-custom">
		{{-- thead --}}
		<tr>
			<th class="text-right"></th>
			<th class="text-right" 
				@click="sortAktivityAsc('beb')"
				style="cursor: pointer;">
				<small><b>BEB</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('oslovene_kontakty')"
				style="cursor: pointer;">
				<small><b>Oslovené kontakty</b></small>
			</th>
			<th class="text-right" 
				{{-- @click="sortAktivityAsc('termin_studeny_trh')" --}}
				>
				<small><b>Termín (súčet)</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('termin_studeny_trh')"
				style="cursor: pointer;">
				<small><b>Termín studený trh</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('termin_databaza')"
				style="cursor: pointer;">
				<small><b>Termín databaza</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('termin_odporucania')"
				style="cursor: pointer;">
				<small><b>Termín odporúčania</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('termin_leady')"
				style="cursor: pointer;">
				<small><b>Termín leady</b></small>
			</th>
			{{-- <th class="text-right"><small><b>Osl. / Ter.</b></small></th>
			<th class="text-right"><small><b>Ter. / FA</b></small></th> --}}
			<th class="text-right"><small><b>Osl. / FA</b></small></th>
		</tr>
		{{-- tbody --}}
		<tr v-for="row in aktivity" :key="row.id_user">
			<td class="text-right">@{{row.id_user}}</td>
			<td class="text-right">@{{row.beb > 0 ? row.beb : 0}}</td>
			<td class="text-right">@{{row.oslovene_kontakty}}</td>
			<td class="text-right">@{{sumTerminy(row)}}</td>
			<td class="text-right">@{{row.termin_studeny_trh}}</td>
			<td class="text-right">@{{row.termin_databaza}}</td>
			<td class="text-right">@{{row.termin_odporucania}}</td>
			<td class="text-right">@{{row.termin_leady}}</td>
			{{-- <td class="text-right">@{{konverziaOslTer(row)}}</td>
			<td class="text-right">@{{konverziaTerFa(row)}}</td> --}}
			<td class="text-right">@{{konverziaOslFa(row)}}</td>
		</tr>
		{{-- tfooter --}}
		<tr>
			<th class="text-right"></th>
			<th class="text-right" 
				@click="sortAktivityDesc('beb')"
				style="cursor: pointer;">
				<small><b>BEB</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityDesc('oslovene_kontakty')"
				style="cursor: pointer;">
				<small><b>Oslovené kontakty</b></small>
			</th>
			<th class="text-right" 
				{{-- @click="sortAktivityAsc('termin_studeny_trh')" --}}
				>
				<small><b>Termín (súčet)</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityDesc('termin_studeny_trh')"
				style="cursor: pointer;">
				<small><b>Termín studený trh</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityDesc('termin_databaza')"
				style="cursor: pointer;">
				<small><b>Termín databaza</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityDesc('termin_odporucania')"
				style="cursor: pointer;">
				<small><b>Termín odporúčania</b></small>
			</th>
			<th class="text-right" 
				@click="sortAktivityAsc('termin_leady')"
				style="cursor: pointer;">
				<small><b>Termín leady</b></small>
			</th>
			{{-- <th class="text-right"><small><b>Osl. / Ter.</b></small></th>
			<th class="text-right"><small><b>Ter. / FA</b></small></th> --}}
			<th class="text-right"><small><b>Osl. / FA</b></small></th>
		</tr>
		{{-- sum --}}
		<tr>
			<td></td>
			<td class="text-right">@{{sumColByKey('beb', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('oslovene_kontakty', aktivity)}}</td>
			<td class="text-right">@{{sumTerminyAll()}}</td>
			<td class="text-right">@{{sumColByKey('termin_studeny_trh', aktivity)}}</td>			
			<td class="text-right">@{{sumColByKey('termin_databaza', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('termin_odporucania', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('termin_leady', aktivity)}}</td>
			{{-- <td class="text-right">@{{avgKonverziaOslTer()}}</td>
			<td class="text-right">@{{avgKonverziaTerFa()}}</td> --}}
			<td class="text-right">@{{avgKonverziaOslFa()}}</td>			
		</tr>
	</table>

	<hr>

	{{-- table 2 --}}
	<table class="table-custom mt-3">
		{{-- thead --}}
		<tr>
			<th class="text-right"></th>
			<th class="text-right"
				@click="sortAktivityAsc('analyza')"
				style="cursor: pointer;">
				<small><b>Analyza (súčet)</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('analyza_studeny_trh')"
				style="cursor: pointer;">
				<small><b>A. studený trh</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('analyza_databaza')"
				style="cursor: pointer;">
				<small><b>A. databáza</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('analyza_odporucania')"
				style="cursor: pointer;">
				<small><b>A. Odporúčania</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('analyza_leady')"
				style="cursor: pointer;">
				<small><b>A. leady</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('predaj')"
				style="cursor: pointer;">
				<small><b>Predaj</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('podpis')"
				style="cursor: pointer;">
				<small><b>Podpis</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('servis')"
				style="cursor: pointer;">
				<small><b>Servis</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('odporucania')"
				style="cursor: pointer;">
				<small><b>Odporúčania</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('administrativa')"
				style="cursor: pointer;">
				<small><b>Administratíva</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('vzdelavanie')"
				style="cursor: pointer;">
				<small><b>Vzdelávanie</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityAsc('dovolenka')"
				style="cursor: pointer;">
				<small><b>Dovolenka</b></small>
			</th>
		</tr>
		{{-- tbody --}}
		<tr v-for="row in aktivity" :key="row.id_user">
			<td class="text-right">@{{row.id_user}}</td>
			<td class="text-right">@{{sumAnalyzy(row)}}</td>
			<td class="text-right">@{{row.analyza_studeny_trh}}</td>
			<td class="text-right">@{{row.analyza_databaza}}</td>
			<td class="text-right">@{{row.analyza_odporucania}}</td>
			<td class="text-right">@{{row.analyza_leady}}</td>
			<td class="text-right">@{{row.predaj}}</td>
			<td class="text-right">@{{row.podpis}}</td>
			<td class="text-right">@{{row.servis}}</td>
			<td class="text-right">@{{row.odporucania}}</td>
			<td class="text-right">@{{row.administrativa}}</td>
			<td class="text-right">@{{row.vzdelavanie}}</td>
			<td class="text-right">@{{row.dovolenka}}</td>
		</tr>
		{{-- tfooter --}}
		<tr>
			<th class="text-right"></th>
			<th class="text-right"
				@click="sortAktivityDesc('analyza')"
				style="cursor: pointer;">
				<small><b>Analyza (súčet)</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('analyza_studeny_trh')"
				style="cursor: pointer;">
				<small><b>A. studený trh</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('analyza_databaza')"
				style="cursor: pointer;">
				<small><b>A. databáza</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('analyza_odporucania')"
				style="cursor: pointer;">
				<small><b>A. Odporúčania</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('analyza_leady')"
				style="cursor: pointer;">
				<small><b>A. leady</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('predaj')"
				style="cursor: pointer;">
				<small><b>Predaj</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('podpis')"
				style="cursor: pointer;">
				<small><b>Podpis</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('servis')"
				style="cursor: pointer;">
				<small><b>Servis</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('odporucania')"
				style="cursor: pointer;">
				<small><b>Odporúčania</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('administrativa')"
				style="cursor: pointer;">
				<small><b>Administratíva</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('vzdelavanie')"
				style="cursor: pointer;">
				<small><b>Vzdelávanie</b></small>
			</th>
			<th class="text-right"
				@click="sortAktivityDesc('dovolenka')"
				style="cursor: pointer;">
				<small><b>Dovolenka</b></small>
			</th>
		</tr>
		{{-- sum --}}
		<tr>
			<td></td>			
			<td class="text-right">@{{sumAnalyzyAll()}}</td>
			<td class="text-right">@{{sumColByKey('analyza_studeny_trh', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('analyza_databaza', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('analyza_odporucania', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('analyza_leady', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('predaj', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('podpis', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('servis', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('odporucania', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('administrativa', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('vzdelavanie', aktivity)}}</td>
			<td class="text-right">@{{sumColByKey('dovolenka', aktivity)}}</td>
		</tr>
	</table>

	<hr>

	{{-- table 3 --}}
	<table class="table-custom mt-3">
		<tr>
			<th></th>
			<th class="text-right"
				@click="sortZmluvyAsc('nezivot')"
				style="cursor: pointer;">
				<small><b>Poistenie - neživot a BU</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyAsc('zivot')"
				style="cursor: pointer;">
				<small><b>Poistenie - život</b></small>
			</th>
				<th class="text-right"
				@click="sortZmluvyAsc('uvery')"
				style="cursor: pointer;">
				<small><b>Hypotéky a úvery</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyAsc('dochodok')"
				style="cursor: pointer;">
				<small><b>Dôchodok - SDS a DDS</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyAsc('investicie')"
				style="cursor: pointer;">
				<small><b>Investície</b></small>
			</th>
		</tr>
		<tr v-for="row in zmluvyNew">
			<td class="text-right">@{{row.id_user}}</td>
			<td class="text-right">@{{row.nezivot}}</td>
			<td class="text-right">@{{row.zivot}}</td>
			<td class="text-right">@{{row.uvery}}</td>
			<td class="text-right">@{{row.dochodok}}</td>
			<td class="text-right">@{{row.investicie}}</td>	
		</tr>
		<tr>
			<th></th>
			<th class="text-right"
				@click="sortZmluvyDesc('nezivot')"
				style="cursor: pointer;">
				<small><b>Poistenie - neživot a BU</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyDesc('zivot')"
				style="cursor: pointer;">
				<small><b>Poistenie - život</b></small>
			</th>
				<th class="text-right"
				@click="sortZmluvyDesc('uvery')"
				style="cursor: pointer;">
				<small><b>Hypotéky a úvery</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyDesc('dochodok')"
				style="cursor: pointer;">
				<small><b>Dôchodok - SDS a DDS</b></small>
			</th>
			<th class="text-right"
				@click="sortZmluvyDesc('investicie')"
				style="cursor: pointer;">
				<small><b>Investície</b></small>
			</th>
		</tr>
		<tr>
			<td></td>
			<td class="text-right">@{{kategoriaSum('nezivot')}}</td>
			<td class="text-right">@{{kategoriaSum('zivot')}}</td>
			<td class="text-right">@{{kategoriaSum('uvery')}}</td>
			<td class="text-right">@{{kategoriaSum('dochodok')}}</td>
			<td class="text-right">@{{kategoriaSum('investicie')}}</td>
		</tr>
	</table>

	</div>

</div>


@endsection

@section('script')
<script>
new Vue({
	el: '#app',
	data: () => ({		
		dateStart: null,
		dateEnd: null,
		aktivity: [],
		aktivityNew: {
			oslovene_kontakty: 0,
			analyza_studeny_trh: 0,
			analyza_databaza: 0,
			analyza_odporucania: 0,
			analyza_leady: 0,			

			termin_studeny_trh: 0,
			termin_databaza: 0,
			termin_odporucania: 0,
			termin_leady: 0,			

			predaj: 0,			
			podpis: 0,
			servis: 0,			
			odporucania: 0,
			den_bez_aktivit: 0,
			administrativa: 0,
			vzdelavanie: 0,
			dovolenka: 0,
		},
		aktivitySend: false,
		zmluvy: [],
		zmluvyNew: [],
	}),

	methods: {

		//===================================================================================
		getData() {
			axios.post('./aktivity/index', {
				dateStart: this.dateStart,
				dateEnd: this.dateEnd,
			}).then(res => {
				this.aktivity = res.data.aktivity				
				this.zmluvy = res.data.zmluvy
				this.generateZmluvyNew()
			}, err => {
				alert(err)
			})
		},


		//===================================================================================
		sumAnalyzy(row) {
			let analyzy = Number(row.analyza_studeny_trh)
			analyzy += Number(row.analyza_databaza)
			analyzy += Number(row.analyza_odporucania)
			analyzy += Number(row.analyza_leady)
			return analyzy
		},

		//===================================================================================
		sumAnalyzyAll() {
			let analyzy = 0;
			this.aktivity.forEach(row => {
				analyzy += Number(row.analyza_studeny_trh)
				analyzy += Number(row.analyza_databaza)
				analyzy += Number(row.analyza_odporucania)
				analyzy += Number(row.analyza_leady)
			})			
			return analyzy
		},

		//===================================================================================
		sumTerminy(row) {
			let ter = Number(row.termin_studeny_trh)
			ter += Number(row.termin_databaza)
			ter += Number(row.termin_odporucania)
			ter += Number(row.termin_leady)
			return ter
		},

		//===================================================================================
		sumTerminyAll() {
			let ter = 0
			this.aktivity.forEach(row => {
				ter += Number(row.termin_studeny_trh)
				ter += Number(row.termin_databaza)
				ter += Number(row.termin_odporucania)
				ter += Number(row.termin_leady)
			})			
			return ter
		},

		//===================================================================================
		sortAktivityAsc(key) {
			this.aktivity = this.aktivity.sort(function(b, a) {
				if(Number(a[key]) < Number(b[key])) return -1;
				if(Number(a[key]) > Number(b[key])) return 1;
				return 0;
			})
		},

		//===================================================================================
		sortAktivityDesc(key) {
			this.aktivity = this.aktivity.sort(function(a, b) {
				if(Number(a[key]) < Number(b[key])) return -1;
				if(Number(a[key]) > Number(b[key])) return 1;
				return 0;
			})
		},

		//===================================================================================
		sortZmluvyAsc(key) {
			this.zmluvyNew = this.zmluvyNew.sort(function(b, a) {
				if(Number(a[key]) < Number(b[key])) return -1;
				if(Number(a[key]) > Number(b[key])) return 1;
				return 0;
			})
		},

		//===================================================================================
		sortZmluvyDesc(key) {
			this.zmluvyNew = this.zmluvyNew.sort(function(a, b) {
				if(Number(a[key]) < Number(b[key])) return -1;
				if(Number(a[key]) > Number(b[key])) return 1;
				return 0;
			})
		},

		//===================================================================================
		generateZmluvyNew() {
			let array = []
			for(let i = 0; i < this.aktivity.length; i++) {
				array.push({})
				array[array.length - 1].id_user = this.aktivity[i].id_user
				array[array.length - 1].nezivot = 0
				array[array.length - 1].zivot = 0
				array[array.length - 1].uvery = 0
				array[array.length - 1].dochodok = 0
				array[array.length - 1].investicie = 0
				for(let j = 0; j < this.zmluvy.length; j++) {
					if(array[array.length - 1].id_user == this.zmluvy[j].id_user) {
						if(this.zmluvy[j].kategoria == 'Poistenie - neživot a bežný účet') array[array.length - 1].nezivot++
						if(this.zmluvy[j].kategoria == 'Poistenie - život') array[array.length - 1].zivot++
						if(this.zmluvy[j].kategoria == 'Hypotéky a úvery') array[array.length - 1].uvery++
						if(this.zmluvy[j].kategoria == 'Dôchodok - SDS a DDS') array[array.length - 1].dochodok++
						if(this.zmluvy[j].kategoria == 'Investície') array[array.length - 1].investicie++
					}
				}
			}			
			this.zmluvyNew = array
		},

		//===================================================================================
		kategoriaSum(key) {
			let sum = 0;
			this.zmluvyNew.forEach(row => {
				sum += row[key]
			})
			return sum
		},

		//===================================================================================
		sendAktivity() {
			axios.post('./aktivity/store', {
				data: this.aktivityNew
			}).then(res => {
				if(res) {
					this.aktivitySend = true
					this.getData()
				}
			}, err => {
				alert(err)
			})
			
		},

		//===================================================================================
		sumColByKey(_key, array) {
			let result = 0
			array.forEach(item => {
				for(let key in item) {
					if(key == _key) {
						result += Number(item[key])
					}
				}
			})
			return result
		},

		//===================================================================================
		sumKategoryByUser(category, user, array) {
			let result = 0
			array.forEach(item => {
				if(item.id_user == user && item.kategoria == category) {
					result++
				}
			})
			return result
		},		

		//===================================================================================
		konverziaOslTer(row) {
			let ter = Number(row.termin_studeny_trh)
			ter += Number(row.termin_databaza)
			ter += Number(row.termin_odporucania)
			ter += Number(row.termin_leady)
			let result = Number(row.oslovene_kontakty) / ter
			if(result == 'Infinity') return 0
			if(isNaN(result)) return 0
			if(result < 1) return 1
			return parseFloat(result.toFixed(1))
		},

		//===================================================================================
		avgKonverziaOslTer() {
			let sum = 0
			let sumUser = 0
			this.aktivity.forEach(row => {
				let ter = Number(row.termin_studeny_trh)
				ter += Number(row.termin_databaza)
				ter += Number(row.termin_odporucania)
				ter += Number(row.termin_leady)
				let result = Number(row.oslovene_kontakty) / ter
				if(result == 'Infinity') sum += 0
				else if(isNaN(result)) sum += 0
				else if(result < 1) {
					sum += 1
					sumUser++
				}
				else {
					sum += parseFloat(result.toFixed(1))
					sumUser++
				}
			})
			return parseFloat(sum / sumUser).toFixed(2)
		},

		//===================================================================================
		konverziaTerFa(row) {
			let ter = Number(row.termin_studeny_trh)
			ter += Number(row.termin_databaza)
			ter += Number(row.termin_odporucania)
			ter += Number(row.termin_leady)

			let analyza = Number(row.analyza_studeny_trh)
			analyza += Number(row.analyza_databaza)
			analyza += Number(row.analyza_odporucania)
			analyza += Number(row.analyza_leady)

			let result = ter / Number(analyza)
			if(result == 'Infinity') return 0
			if(isNaN(result)) return 0
			if(result < 1) return 1
			return parseFloat(result.toFixed(1))
		},

		//===================================================================================
		avgKonverziaTerFa() {
			let sum = 0
			let sumUser = 0
			this.aktivity.forEach(row => {
				let ter = Number(row.termin_studeny_trh)
				ter += Number(row.termin_databaza)
				ter += Number(row.termin_odporucania)
				ter += Number(row.termin_leady)

				let analyza = Number(row.analyza_studeny_trh)
				analyza += Number(row.analyza_databaza)
				analyza += Number(row.analyza_leady)

				let result = ter / Number(analyza)				
				if(result == 'Infinity') sum += 0
				else if(isNaN(result)) sum += 0
				else if(result < 1) {
					sum += 1
					sumUser++
				}
				else {
					sum += parseFloat(result.toFixed(1))
					sumUser++
				}
			})
			return parseFloat(sum / sumUser).toFixed(2)			
		},

		//===================================================================================
		konverziaOslFa(row) {
			let oslTer = this.konverziaOslTer(row)
			let terFa = this.konverziaTerFa(row)
			result = oslTer * terFa
			if(result == 1) return result
			if(result == 0) return result
			return parseFloat(result.toFixed(1))
		},
		
		//===================================================================================
		avgKonverziaOslFa() {
			let temp1 = this.avgKonverziaOslTer()
			let temp2 = this.avgKonverziaTerFa()
			return parseFloat(temp1 * temp2).toFixed(2)			
		},

		//===================================================================================
		dateToDMY(date) {
			if(date == "" || date == " "){
				return "";
			}
			var datum =  new Date(date);
			var d = datum.getDate();
			var m = datum.getMonth() + 1; //Month from 0 to 11
			var y = datum.getFullYear();
			let result = '' + (d <= 9 ? '0' + d : d) + '.' + (m<=9 ? '0' + m : m) + '.' + y
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		genDateStart(date) {
			if(date == "" || date == " "){
				return "";
			}
			var datum =  new Date(date);
			var index = 0;
			var d = datum.getDate();
			if(d > 20) index++;
			var m = datum.getMonth() + index; //Month from 0 to 11
			var y = datum.getFullYear();
			let result = y  + '-' + (m<=9 ? '0' + m : m) + '-' + '21'
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		genDateEnd(date) {
			if(date == "" || date == " "){
				return "";
			}
			var datum =  new Date(date);
			var index = 1;
			var d = datum.getDate();
			if(d > 20) index++;
			var m = datum.getMonth() + index; //Month from 0 to 11
			var y = datum.getFullYear();
			let result = y  + '-' + (m<=9 ? '0' + m : m) + '-' + '20'
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

	},

	//===================================================================================
	mounted() {
		this.dateStart = this.genDateStart(new Date())
		this.dateEnd = this.genDateEnd(new Date())
		this.getData();
	},
})

</script>
@endsection