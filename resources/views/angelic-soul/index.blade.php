@extends('master')
@section('title', 'Databáza')

@section('content')

	<div class="form p-2">	
		
		<label class="mb-0">Krstné meno</label>
		<input class="form-control mb-1" type="text" v-model="data.name_first">

		<label class="mb-0">Priezvisko</label>
		<input class="form-control mb-1" type="text" v-model="data.name_last">

		<label class="mb-0">Dátum narodenia</label>
		<input class="form-control mb-1" type="date" v-model="data.datum_narodenia">

		<label class="mb-0">Telefonický kontakt</label>
		<input class="form-control mb-1" type="text" v-model="data.phone">

		<label class="mb-0">Email</label>
		<input class="form-control mb-1" type="text" v-model="data.email">

		<label class="mb-0">Ulica</label>
		<input class="form-control mb-1" type="text" v-model="data.ulica">

		<label class="mb-0">PSC a Obec</label>
		<input class="form-control mb-1" type="text" v-model="data.psc_obec">

		<button class="btn btn-success mt-1" @click="update()">Uložiť</button>
	</div>
	
	{{-- Blank load screen --}}
	<div v-if="!access" class="fullscreen p-1">
		<h1 ref="msg"></h1>
	</div>
@endsection

@section('script')
<script>
	new Vue({
		el: '#app',
		data() {
			return {
				access: false,
				idPerson: "{{$data['id_person']}}",				
				data: {},
			}
		},
		methods: {
			getData() {
				axios.get('./data/'+this.idPerson).then(res => {	
					if(res.data.length > 0) {
						this.data = res.data[0]
						this.access = true		
					} else {
						this.$refs.msg.innerHTML = "Nesprávny odkaz..."
					}
				})
			},
			update() {
				axios.post('./update', {
					data: this.data
				}).then(res => {
					alert('Údaje boli úspešne uložené.')
				}, err => alert(err))
			},
		},

		mounted() {
			this.getData()
		}
	})
</script>
@endsection