@extends('master')
@section('title', 'Login')

@section('particles-js')
	<div id="particles-js" style="position: fixed; width: 100%; background:#00356b"></div>
	<script src="{{ URL::asset('/js/particles.min.js')  }}"></script>
	<script src="{{ URL::asset('/js/particles-app.js')  }}"></script>
@endsection

@section('content')

<div class="d-flex justify-content-center align-items-center p-4"
	style="position: fixed; top: 10px; right: 10px;"
	v-if="!offlineMode">	

	<div class="form-group p-3 bg-light border rounded" style="width: 400px;">

		@csrf
		
		<input 
			type="text"
			class="form-control mb-2"
			placeholder="Email"
			v-model="email"
			:disabled="emailSend"
			@keyup.enter="sendEmail()"
			ref="email"
			required
		>
		<div v-if="emailSend == false">
			<input type="submit" class="btn btn-primary" value="Odoslať prihlasovací kód" @click="sendEmail()">
		</div>
		<div v-if="emailSend">
			<input 
				type="text"
				class="form-control mb-2" 
				placeholder="Prihlasovací kód"
				v-model="pass"
				maxlength="4"
				ref="kod"
				@keyup.enter="login()"
				required
			>
			<input type="submit" class="btn btn-primary" value="Prihlásiť" @click="login()">
		</div>

	</div>

</div>

<div class="d-flex justify-content-center align-items-center p-4"
	style="position: fixed; top: 10px; right: 10px;"
	v-if="offlineMode">	

	<div class="form-group p-3 bg-light border rounded" style="width: 400px;">

		@csrf
		
		<input 
			type="text"
			class="form-control mb-2"
			placeholder="Email"
			v-model="email"
			:disabled="emailSend"
			@keyup.enter="sendEmail()"
			ref="email"
			required
		>
	
		<input 
			type="password"
			class="form-control mb-2" 
			placeholder="Password"
			v-model="pass"
			ref="kod"
			@keyup.enter="login()"
			required
		>

		<input type="submit" class="btn btn-primary" value="Prihlásiť" @click="login()">

	</div>

</div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',

	data: () => ({
		emailSend: false,		
		email: null,
		pass: null,
		appUrl: <?php echo '"' . env('APP_URL') . '"'; ?>,
		offlineMode: false,
	}),

	methods: {

		//======================================================================
		checkOfflineMode() {
			this.offlineMode = this.appUrl.includes('localhost') ? true : false
		},

		//======================================================================
		sendEmail() {
			axios.post('./auth/sendLoginKey', {
				email: this.email
			}).then(res => {
				if(!res.data.err) {
					this.emailSend = true
					$cookies.set('email', this.email, '30d')
					setTimeout(function() {
						this.$refs.kod.focus()
					}.bind(this), 100)
				} else {
					alert(res.data.err)
				}
			})
		},

		//======================================================================
		login() {
			if(this.email.length == 0 || this.pass.length == 0)
				return alert('Neplatný prihlasovací kód.')
			axios.post('./auth/login', {
				email: this.email,
				pass: this.pass,
			}).then((response) => {				
				if(response.data.access == true) {
					window.location.href = './';
				} else {
					alert('Neplatný prihlasovací kód.')
					console.log(response)
				}
			});
		},
	},

	//======================================================================
	mounted() {
		this.checkOfflineMode()
		this.email = $cookies.get('email')
		this.$refs.email.focus()
	}
	
})
</script>
@endsection