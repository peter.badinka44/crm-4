<head>
	<title>Dátum</title>
</head>

<style>
    .page_swap{
        margin-right: 0%;
    }
    .field{
        float: left;
        border: none;        
    }
    .popis {
        font-size: 18px;
        color: #8c8c8c;
    }
    .input{
        margin-bottom: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
        height: 40px;
    }
</style>
  
<fieldset class="field">
    <label class="popis">Dátum / Výročie</label></br>
    <input class="input" id="dateS" type="date" onchange="calcDate()" style="width:250px; font: 25px arial, sans-serif;">
    <br><label class="popis">Počet týždňov</label>
    <br><input class="input" id="numWeeks" type="number" value="-6" onchange="calcDate()" style="width:250px; font: 25px arial, sans-serif;">
    <br><label class="popis">Dátum - počet týždňov</label>
    <br><input class="input" id="vDate" type="text" style="width:250px; font: 25px arial, sans-serif;">
</fieldset>

<script type="text/javascript">
    dateS.value = dateToYMD(new Date());
    calcDate();

    //=================================================================
    //Uprava formatu datumu
    //=================================================================
    function calcDate() {
        if(date == "" || date == ""){
            return "";
        }
        
        var _date = dateS.value;
        var tyzdne = numWeeks.value;
        
        var now = new Date(_date);
        now.setMinutes(now.getMinutes() + (60 * 24) * Number(tyzdne * 7)); // timestamp
        var date = new Date(now);
        
        var datum =  new Date(date);
        var d = datum.getDate();
        var m = datum.getMonth() + 1; //Month from 0 to 11
        var y = datum.getFullYear();
        
        var cas =  new Date(date);
        var hod = cas.getHours();
        var min = cas.getMinutes();
        
        var result = (d <= 9 ? '0' + d : d) + "." + (m<=9 ? '0' + m : m) + "." + y;
        
        if(_date.length > 3){
            vDate.value = result;
        } else vDate.value = "";
    }

    //=======================================================================================
	// Uprava formatu datumu
	//=======================================================================================
	function dateToYMD(date) {
		if(date == "" || date == "" || date == "0000-00-00" || date == null){
		return "";
		}  
		var datum =  new Date(date);
		var d = datum.getDate();
		var m = datum.getMonth() + 1; //Month from 0 to 11
		var y = datum.getFullYear();
		return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
	}
</script>