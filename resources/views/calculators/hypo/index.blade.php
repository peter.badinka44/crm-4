@extends('master')
@section('title', 'Hypoteky')

@section('content')

<div class="fullscreen p-1">

	<ul class="nav nav-pills mb-3 border" id="pills-tab" role="tablist" style="position: fixed; top: 0px; background-color: white;">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="pill" href="#tab-index-kalkulacie" role="tab" aria-controls="tab-index-kalkulacie">
				Kalkulácie
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="pill" href="#tab-index-nastavenia" role="tab" aria-controls="tab-index-nastavenia">
				Nastavenia
			</a>
		</li>
	</ul>

	<div class="tab-content mt-5">

		<div class="tab-pane fade show active" id="tab-index-kalkulacie" role="tabpanel">			

			<div class="m-1">				
				<button class="btn btn-primary mb-1" @click="showNew()">+</button>
			
				<table class="table-custom">

					<tr v-if="kalkulacie.length > 0">
						<th>Dátum</th>
						<th>Názov</th>
						<th>Vek</th>
						<th>Príjem</th>
						<th>Splátky úverov</th>
						<th>Splátky hypoték</th>
						<th>Zostatok</th>
						<th>Osoby/Deti</th>
					</tr>

					<tr v-for="kalkulacia in kalkulacie" @click="showCalc(kalkulacia)" style="cursor: pointer;">
						<td>@{{formatDate(kalkulacia.created_at)}}</td>
						<td class="text-left">@{{kalkulacia.attr.nazovKalkulacie}}</td>
						<td>@{{kalkulacia.attr.vek}}</td>
						<td>@{{kalkulacia.attr.mesacnyPrijem}}</td>
						<td>@{{kalkulacia.attr.mesacneSplatky}}</td>
						<td>@{{kalkulacia.attr.mesacneSplatkyHypo}}</td>
						<td>@{{kalkulacia.attr.aktualnyZostatokUverov}}</td>
						<td>@{{kalkulacia.attr.pocetOsob}}/@{{kalkulacia.attr.pocetDeti}}</td>
					</tr>
				</table>				
		
			</div>

		</div>

		@include('calculators.hypo.settings')

		@include('calculators.hypo.show')

	</div>	

</div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',
	data: () => ({
		kalkulacie: [],
		kalkulaciaSelect: {},
		kalkulaciaDefault: {},

		vek: 0,
		mesacnyPrijem: 0,
		mesacneSplatky: 0,
		mesacneSplatkyHypo: 0,
		aktualnyZostatokUverov: 0,
		pocetOsob: 1,
		pocetDeti: 0,
		vztah: "manžel a manželka / druh a druzka",

		zivotneMin: {
			dospelaOsoba1: 0,
			dospelaOsoba2: 0,
			dieta: 0,
			result: 0
		},
		
		povinnaRezerva: 0,
		maxVyskaUverov: 0,
		maxSplatkaUveru: 0,
		stressTest: 0,
		defaultDtiKofecient: 8,
		dtiKofecient: 8,
		maxVyskaDostupnychUverov: 0,

		modal: {
			show: false,
			new: false,
		},
	}),
	methods: {
		prepocitajKalkulaciu() {
			this.prepocitajDtiKoficient()
			let zivMinDospeleOsoby = this.kalkulaciaSelect.pocetOsob * this.zivotneMin.dospelaOsoba1
			let zivMinDeti = this.kalkulaciaSelect.pocetDeti * this.zivotneMin.dieta
			if(this.kalkulaciaSelect.vztah == "manžel a manželka / druh a druzka" && this.kalkulaciaSelect.pocetOsob > 1) {
				zivMinDospeleOsoby = this.zivotneMin.dospelaOsoba1 + this.zivotneMin.dospelaOsoba2
			}
			this.zivotneMin.result = zivMinDospeleOsoby + zivMinDeti;
			this.povinnaRezerva = (this.kalkulaciaSelect.mesacnyPrijem - this.zivotneMin.result) * 0.4
			this.maxSplatkaUveru = ((this.kalkulaciaSelect.mesacnyPrijem - this.kalkulaciaSelect.mesacneSplatky - this.zivotneMin.result) - this.povinnaRezerva) - (this.kalkulaciaSelect.mesacneSplatkyHypo)
			this.stressTest = ((this.kalkulaciaSelect.mesacnyPrijem - this.kalkulaciaSelect.mesacneSplatky - this.zivotneMin.result) - this.povinnaRezerva) - (this.kalkulaciaSelect.mesacneSplatkyHypo * 1.306)
			this.maxVyskaUverov = this.kalkulaciaSelect.mesacnyPrijem * 12 * this.dtiKofecient
			this.maxVyskaDostupnychUverov = Number(this.maxVyskaUverov) - Number(this.kalkulaciaSelect.aktualnyZostatokUverov)
		},
		prepocitajDtiKoficient() {
			if (this.kalkulaciaSelect.vek > 41) {
				let znizenieDti = (this.kalkulaciaSelect.vek - 41) * 0.25
				znizenieDti = znizenieDti > 3 ? 3 : znizenieDti
				this.dtiKofecient = this.defaultDtiKofecient - znizenieDti
			} else {
				this.dtiKofecient = this.defaultDtiKofecient
			}
			console.log('DTI koeficient: ' + this.dtiKofecient)
		},
		formatNumber(val) {
			return Intl.NumberFormat().format(Math.round(val.toFixed(2)))
		},
		getData() {
			axios.get('./hypo/index').then(res => {
				console.log(res.data)
				this.kalkulacie = res.data.data
				this.zivotneMin.dospelaOsoba1 = res.data.default.attr.koeficienty.zivotneMin.dospelaOsoba1
				this.zivotneMin.dospelaOsoba2 = res.data.default.attr.koeficienty.zivotneMin.dospelaOsoba2
				this.zivotneMin.dieta = res.data.default.attr.koeficienty.zivotneMin.dieta
				this.defaultDtiKofecient = res.data.default.attr.koeficienty.dti
				this.dtiKofecient = res.data.default.attr.koeficienty.dti

				this.kalkulaciaDefault.vek = res.data.default.attr.default_values.vek
				this.kalkulaciaDefault.mesacnyPrijem = res.data.default.attr.default_values.mesacnyPrijem	
				this.kalkulaciaDefault.mesacneSplatky = res.data.default.attr.default_values.mesacneSplatky
				this.kalkulaciaDefault.mesacneSplatkyHypo = res.data.default.attr.default_values.mesacneSplatkyHypo
				this.kalkulaciaDefault.aktualnyZostatokUverov = res.data.default.attr.default_values.aktualnyZostatokUverov
				this.kalkulaciaDefault.pocetOsob = res.data.default.attr.default_values.pocetOsob
				this.kalkulaciaDefault.pocetDeti = res.data.default.attr.default_values.pocetDeti
				this.kalkulaciaDefault.vztah = res.data.default.attr.default_values.vztah
				this.prepocitajKalkulaciu()
			}, err => {
				alert(err)
			})
		},
		formatDate(date) {
			return moment(date).format('DD.MM.YYYY HH:mm');
		},
		showCalc(kalkulacia) {
			this.modal.show = true
			this.modal.new = false
			this.kalkulaciaSelect = kalkulacia.attr
			this.kalkulaciaSelect.id = kalkulacia.id
			this.prepocitajKalkulaciu()
		},
		showNew() {
			this.modal.show = true
			this.modal.new = true
			this.kalkulaciaSelect = this.kalkulaciaDefault
			this.prepocitajKalkulaciu()
		},
		createNew() {
			axios.post('./hypo/create', {
				data: this.kalkulaciaSelect
			}).then(res => {
				this.getData()
				this.modal.show = false
			}, err => {
				alert(err)
			})			
		},
		update() {
			axios.post('./hypo/update', {
				data: this.kalkulaciaSelect
			}).then(res => {	
				this.getData()
				this.modal.show = false
			}, err => {
				alert(err)
			})
		},
		deleteCalc() {
			axios.post('./hypo/delete', {
				id: this.kalkulaciaSelect.id
			}).then(res => {	
				this.getData()
				this.modal.show = false
			}, err => {
				alert(err)
			})
		},
		updateSettings() {
			axios.post('./hypo/updateSettings', {
				data: {
					default_values: this.kalkulaciaDefault,
					koeficienty: {
						zivotneMin: {
							dospelaOsoba1: this.zivotneMin.dospelaOsoba1,
							dospelaOsoba2: this.zivotneMin.dospelaOsoba2,
							dieta: this.zivotneMin.dieta
						},
						dti: this.defaultDtiKofecient
					}
				}
			}).then(res => {	
				console.log(res.data)
			}, err => {
				alert(err)
			})
		}
	},
	async mounted() {
		await this.getData()
		this.prepocitajKalkulaciu()
	}
})
</script>

<style>
	td { 
		border: 1px solid #ccc;
		text-align: right;
	}		
</style>
@endsection