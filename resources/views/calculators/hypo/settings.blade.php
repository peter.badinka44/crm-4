<div class="tab-pane fade" id="tab-index-nastavenia" role="tabpanel">

    <div class="row m-3">

        <div class="col-12 col-sm-6 col-lg-4"> 
            <h2>Prednastavené hodnoty</h2>           
            vek klienta
            <input v-model="kalkulaciaDefault.vek" class="form-control" type="number" step="1" min="0">
            čístý mesačný príjem
            <input v-model="kalkulaciaDefault.mesacnyPrijem" class="form-control" type="number" step="50" min="0">
            aktuálne mesačné splátky NEhypotekárnych úverov(*)
            <input v-model="kalkulaciaDefault.mesacneSplatky" class="form-control" type="number" step="50" min="0">
            aktuálne mesačné splátky hypotekárnych úverov(**)
            <input v-model="kalkulaciaDefault.mesacneSplatkyHypo" class="form-control" type="number" step="50" min="0">
            aktuálny zostatok úverov
            <input v-model="kalkulaciaDefault.aktualnyZostatokUverov" class="form-control" type="number" step="5000" min="0">
            počet dospelých osôb
            <input v-model="kalkulaciaDefault.pocetOsob" @input="test()" class="form-control" type="number" min="1" max="2">
            <div v-if="kalkulaciaDefault.pocetOsob > 1">
                vzťah
                <select v-model="kalkulaciaDefault.vztah" class="form-control">
                    <option>manžel a manželka / druh a druzka</option>
                    <option>iné</option>
                </select>
            </div>
            počet neplnoletých detí
            <input v-model="kalkulaciaDefault.pocetDeti" class="form-control" type="number" min="0">            
        </div>

        <div class="col-12 col-sm-6 col-lg-4">
            <h2>Koeficienty</h2>
            Životné minimum - dospelá osoba 1
            <input v-model="zivotneMin.dospelaOsoba1" class="form-control" type="number" min="0">
            Životné minimum - dospelá osoba 2
            <input v-model="zivotneMin.dospelaOsoba2" class="form-control" type="number" min="0">
            Životné minimum - dieťa
            <input v-model="zivotneMin.dieta" class="form-control" type="number" min="0">					
            DTI
            <input v-model="defaultDtiKofecient" class="form-control" type="text">
            <button class="btn btn-success w-100 my-3" @click="updateSettings()">Uložiť</button>
        </div>

    </div>

</div>