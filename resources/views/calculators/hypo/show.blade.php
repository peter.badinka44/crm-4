<div class="fullscreen p-1" v-if="modal.show">

    <div class="row m-0 p-0">

		@csrf

		<button
			class="btn btn-danger mr-1"
			@click="modal.show = false;"
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success mr-1"
			style="width: 75px;"
			@click="update()"
			v-if="!modal.new">
			Uložiť
		</button>

		<button class="btn btn-success mr-1" 
			@click="createNew()" 
			v-if="modal.new">
			Pridať
		</button>

		<button
			class="btn btn-danger ml-auto" 
			style="width: 100px;" 
			@click="deleteCalc()"
			v-if="!modal.new">
			Vymazať
		</button>
	</div>

    <div class="m-2">
        názov kalkulácie
        <input v-model="kalkulaciaSelect.nazovKalkulacie" class="form-control" type="text">
    </div>

    <div class="row m-2">

        <div class="col-12 col-sm-6 col-lg-4">            
            vek klienta
            <input v-model="kalkulaciaSelect.vek" @input="prepocitajKalkulaciu()" class="form-control" type="number" step="1" min="0">
            čístý mesačný príjem
            <input v-model="kalkulaciaSelect.mesacnyPrijem" @input="prepocitajKalkulaciu()" class="form-control" type="number" step="50" min="0">
            aktuálne mesačné splátky NEhypotekárnych úverov(*)
            <input v-model="kalkulaciaSelect.mesacneSplatky" @input="prepocitajKalkulaciu()" class="form-control" type="number" step="50" min="0">
            aktuálne mesačné splátky hypotekárnych úverov(**)
            <input v-model="kalkulaciaSelect.mesacneSplatkyHypo" @input="prepocitajKalkulaciu()" class="form-control" type="number" step="50" min="0">
            aktuálny zostatok úverov
            <input v-model="kalkulaciaSelect.aktualnyZostatokUverov" @input="prepocitajKalkulaciu()" class="form-control" type="number" step="5000" min="0">
            počet dospelých osôb
            <input v-model="kalkulaciaSelect.pocetOsob" @input="prepocitajKalkulaciu()" class="form-control" type="number" min="1" max="2">
            <div v-if="kalkulaciaSelect.pocetOsob > 1">
                vzťah
                <select v-model="kalkulaciaSelect.vztah" @change="prepocitajKalkulaciu()" class="form-control">
                    <option>manžel a manželka / druh a druzka</option>
                    <option>iné</option>
                </select>
            </div>
            počet neplnoletých detí
            <input v-model="kalkulaciaSelect.pocetDeti" @input="prepocitajKalkulaciu()" class="form-control" type="number" min="0">
        </div>

        <div class="col-12 col-sm-6 col-lg-4">
            <table class="table mt-4">
                <tr>
                    <td>životné minimum</td>
                    <td>@{{formatNumber(zivotneMin.result)}}</td>
                </tr>
                <tr :class="[ kalkulaciaSelect.mesacnyPrijem - zivotneMin.result < 0 ? 'text-danger' : '' ]">
                    <td>čístý príjem znížený o životné minimum</td>
                    <td>@{{formatNumber(kalkulaciaSelect.mesacnyPrijem - zivotneMin.result)}}</td>
                </tr>
                <tr :class="[ povinnaRezerva < 0 ? 'text-danger' : '' ]">
                    <td>finančná rezerva (od 1.1.2020 je 40%)</td>
                    <td>@{{formatNumber(povinnaRezerva)}}</td>
                </tr>
                <tr :class="[ maxSplatkaUveru < 0 ? 'text-danger' : '' ]">
                    <td>limit splátky - rezerva 40%</td>
                    <td>@{{formatNumber(maxSplatkaUveru)}}</td>
                </tr>
                <tr :class="[ (stressTest) < 0 ? 'text-danger' : '' ]" style="background-color: #eff2f5;">
                    <td><b>DSTI (vrátane stress testu)</b></td>
                    <td><b> @{{formatNumber(stressTest)}}</b></td>
                </tr>
                <tr>
                    <td>DTI (z príjmu)</td>
                    <td>@{{formatNumber(maxVyskaUverov)}}</td>
                </tr>
                <tr :class="[ (maxVyskaUverov - aktualnyZostatokUverov) < 0 ? 'text-danger' : '' ]">
                    <td><b>DTI (odrátené úvery)</b></td>
                    <td><b>@{{formatNumber(maxVyskaDostupnychUverov)}}</b></td>
                </tr>
            </table>
        </div>

        <p class="mb-2 mt-4">* vpíš spoločnú výšku všetkých NEhypotekárnych splátok, ktoré klientovi zostávajú a ktoré klientovi pribudnú následkom našich riešení, vrátane kreditných kariet, povolených prečerpaní<br></p>
        <p>** vpíš spoločnú výšku všetkých hypotekárnych splátok, ktoré klientovi zostávajú a ktoré klientovi pribudnú v dôsledku našich riešení</p>

    </div>

</div>