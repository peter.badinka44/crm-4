<head>
	<title>IBAN</title>
</head>

<style>
.page_swap{
    margin-right: 0%;
  }
.button {
    background-color: #008CBA; /* Green */
    border: none;
    color: white;
    padding: 12px 12px;
    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor: pointer;
	width: 280px;
    margin-bottom: 5px;
}
input[type=text] {
    width: 100%;
    padding: 12px 12px;    
    margin: 0px 0;
    box-sizing: border-box;
	width: 280px;
    margin-bottom: 5px;
	border: 1px solid #666;
}
select[type=text] {
    width: 100%;
    padding: 12px 12px;
    margin: 0px 0;
    box-sizing: border-box;
	width: 280px;
    margin-bottom: 5px;
}
</style>

<table>
	<tr>
		<td>
			<h2 style="font-family:Book Antiqua;">Kalkulačka IBAN</h2>
			<input name="M3$txtPredcislie" type="text" value="" maxlength="6" id="M3_txtPredcislie" placeholder="Predčíslie" onchange="clearResult()"/>			
			<br><input name="M3$txtCisloUctu" type="text" value="" maxlength="10" id="M3_txtCisloUctu" placeholder="Číslo účtu" onchange="clearResult()"/>
			<br><select name="kodBankySR" type="text" id="kodBankySR" onchange="clearResult()">
				<option value="0200">0200 - Všeobecn&#225; &#250;verov&#225; banka, a. s.</option>
				<option value="0720">0720 - N&#225;rodn&#225; banka Slovenska</option>	
				<option value="0900">0900 - Slovensk&#225; sporiteľňa, a. s.</option>
				<option value="1100">1100 - Tatra banka, a. s.</option>
				<option value="1111">1111 - UniCredit Bank Czech Republic and Slovakia, a.s., pobočka zahraničnej banky</option>
				<option value="3000">3000 - Slovensk&#225; z&#225;ručn&#225; a rozvojov&#225; banka, a. s.</option>
				<option value="3100">3100 - Prima banka Slovensko, a.s. (predt&#253;m Sberbank Slovensko, a.s.) - k&#243;d pre dobeh platieb do 31. 7. 2019</option>
				<option value="5200">5200 - OTP Banka Slovensko, a. s.</option>
				<option value="5600">5600 - Prima banka Slovensko, a.s</option>
				<option value="5900">5900 - Prv&#225; stavebn&#225; sporiteľňa, a. s.</option>
				<option value="6500">6500 - Poštov&#225; banka, a. s.</option>
				<option value="7300">7300 - ING Bank N. V., pobočka zahraničnej banky</option>
				<option value="7500">7500 - Československ&#225; obchodn&#225; banka, a. s.</option>
				<option value="7930">7930 - W&#252;stenrot stavebn&#225; sporiteľňa, a. s.</option>
				<option value="8050">8050 - COMMERZBANK Aktiengesellschaft, pobočka zahraničnej banky, Bratislava</option>
				<option value="8100">8100 - Komerčn&#237; banka a.s. pobočka zahraničnej banky</option>
				<option value="8120">8120 - Privatbanka, a. s.</option>
				<option value="8130">8130 - Citibank Europe plc,&#160;&#160;pobočka zahraničnej banky</option>
				<option value="8160">8160 - EXIMBANKA SR</option>
				<option value="8170">8170 - ČSOB stavebn&#225; sporiteľňa, a. s.</option>
				<option value="8180">8180 - Št&#225;tna pokladnica</option>
				<option value="8320">8320 - J &amp; T BANKA, a. s., pobočka zahraničnej banky</option>
				<option value="8330">8330 - Fio banka, a.s., pobočka zahraničnej banky</option>
				<option value="8360">8360 - mBank S.A., pobočka zahraničnej banky</option>
				<option value="8370">8370 - Oberbank AG, pobočka zahraničnej banky v Slovenskej republike</option>
				<option value="8390">8390 - Československ&#233; &#250;věrn&#237; družstvo, pobočka Slovensko</option>
				<option value="8400">8400 - COFIDIS SA, pobočka zahraničnej banky</option>
				<option value="8420">8420 - BKS Bank AG, pobočka zahraničnej banky v SR</option>
				<option value="8430">8430 - KDB Bank Europe Ltd. pobočka zahraničnej banky</option>
				<option value="8440">8440 - BNP PARIBAS PERSONAL FINANCE SA, pobočka zahraničnej banky  </option>
				<option value="9952">9952 - Trust Pay, a.s.</option>
				<option value="9953">9953 - Payment institution NFD, a.s.</option>
			</select><br>
			<input id="button1" class="button" type="button" value="Prepočítať IBAN" onClick="runScript()">
			<br><input id="txtIban" type="text" value="" readonly="true"; placeholder="IBAN elektr. formát" style="background-color: #f2f2f2;">
			<br><input id="txtIban2" type="text" value="" placeholder="IBAN tlačový formát" style="background-color: #f2f2f2;" readonly="true">
			<br><input id="txtSwift" type="text" value="" placeholder="SWIFT (BIC)" style="background-color: #f2f2f2;" readonly="true">
			<h2 style="font-family:Book Antiqua;">Kontrola IBAN</h2>		
			<input name="txtIbanValidator" type="text" value="" id="txtIbanValidator" placeholder="IBAN" onchange="clearValidate()"/>
			<br><input id="button2" class="button" type="button" value="Overiť" onClick="validateIban()">	
			<br><input name="txtVysledokVyhodnotenia" type="text" value="" id="txtVysledokVyhodnotenia" placeholder="Výsledok vyhodnotenia"  style="background-color: #f2f2f2;" readonly="true"/>
		</td>
	</tr>
 </table>

<script type="text/javascript">
function runScript(){
	prepocetIbanu();
	prepocetIbanu();
}
function prepocetIbanu(){
	var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
	var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
	var predcislieUctu  = M3_txtPredcislie.value;
	if(predcislieUctu.length == 0){
		M3_txtPredcislie.value = "000000";
		predcislieUctu = "000000"
	}
	if(predcislieUctu.length > 0 & predcislieUctu.length < 6){
		var tempChar = "0";
		var tempPredcislie = "";
		var index = 6 - predcislieUctu.length;
		for(var i = 0; i < index; i++){
			tempPredcislie += tempChar;			
		}
		tempPredcislie += predcislieUctu;
		M3_txtPredcislie.value = tempPredcislie;
		predcislieUctu = tempPredcislie;
	}
	
	var cisloUctu  = M3_txtCisloUctu.value;
	if(cisloUctu.length == 0){
		M3_txtCisloUctu.value = "0000000000";
		cisloUctu = "0000000000"
	}
	if(cisloUctu.length > 0 & cisloUctu.length < 10){
		var tempChar = "0";
		var tempPredcislie = "";
		var index = 10 - cisloUctu.length;
		for(var i = 0; i < index; i++){
			tempPredcislie += tempChar;			
		}
		tempPredcislie += cisloUctu;
		M3_txtCisloUctu.value = tempPredcislie;		
	}
	var kodBanky = kodBankySR.value;
	swiftBic(kodBanky);
	var bban = kodBanky + predcislieUctu + cisloUctu;
	var bbanISO = bban + "SK00"
	var znak1 = bbanISO.slice(20, 21);
	var znak2 = bbanISO.slice(21, 22);
	var stvrtyKrok = "";
	for(var i = 0; i < bbanISO.length; i++){
		var temp = bbanISO.slice(i, i + 1);
		if (temp == "S") {
			stvrtyKrok += S;
		} else if (temp == "K") {
			stvrtyKrok += K;
		} else {
			stvrtyKrok += temp;
		}			
	}
	var zLava8 = stvrtyKrok.slice(0, 8);
	var zvysokZlava8 = zLava8 % 97;
	var next8Zlava = stvrtyKrok.slice(8, 16);
	var kompoziciaSnavratmy = zvysokZlava8.toString() + next8Zlava.toString();
	var nextZvysok = kompoziciaSnavratmy % 97;
	var next6Zlava = stvrtyKrok.slice(16, 22);
	var kompoziciaSnavratmy2 = nextZvysok.toString() + next6Zlava.toString();
	var nextZvysok2 = kompoziciaSnavratmy2 % 97;
	var next4Zlava = stvrtyKrok.slice(22, 26);
	var kompoziciaSnavratmy3 = nextZvysok2.toString() + next4Zlava.toString();
	var nextZvysok3 = kompoziciaSnavratmy3 % 97;
	var diferencia98 = 98 - nextZvysok3;	
	var resultIban = "SK" + diferencia98 + kodBanky + predcislieUctu + cisloUctu;
	var checkTrueIban = checkIban(resultIban, S, K, diferencia98);
	if(checkTrueIban != 1){
		txtIban.value = "IBAN nie je možné prepočítať"
		txtIban2.value = "chybne zadané údaje";
	}else{
		txtIban.value = resultIban;
		stvrtyKrok = "";
		for(var i = 0; i < resultIban.length; i++){
			var temp = resultIban.slice(i, i + 1);
			if (i == 4 | i == 8 | i == 12 | i == 16 | i == 20) {
				stvrtyKrok += " ";
				stvrtyKrok += temp;
			}else {
				stvrtyKrok += temp;
			}			
		}	
	txtIban2.value = stvrtyKrok;	
	var copyText = document.getElementById("txtIban");
	copyText.select();
	document.execCommand("Copy");
	button1.focus();
	//txtIbanValidator.value = resultIban;
	//validateIban();	
	}
}
function checkIban(resultIban, S, K, diferencia98){
	var tempIban = resultIban.slice(4, 26);
	var tempIban2 = tempIban.toString() + S.toString() + K.toString() + diferencia98.toString();
	var zLava8 = tempIban2.slice(0, 8);
	var zvysokZlava8 = zLava8 % 97;
	var next8Zlava = tempIban2.slice(8, 16);
	var kompoziciaSnavratmy = zvysokZlava8.toString() + next8Zlava.toString();
	var nextZvysok = kompoziciaSnavratmy % 97;
	var next6Zlava = tempIban2.slice(16, 22);
	var kompoziciaSnavratmy2 = nextZvysok.toString() + next6Zlava.toString();
	var nextZvysok2 = kompoziciaSnavratmy2 % 97;
	var next4Zlava = tempIban2.slice(22, 26);
	var kompoziciaSnavratmy3 = nextZvysok2.toString() + next4Zlava.toString();
	var nextZvysok3 = kompoziciaSnavratmy3 % 97;	
	return nextZvysok3;	
}
function swiftBic(kodBanky){
	txtSwift.value = "";
	if(kodBanky == "0200")txtSwift.value = "SUBASKBX";
	if(kodBanky == "0720")txtSwift.value = "NBSBSKBX";
	if(kodBanky == "0900")txtSwift.value = "GIBASKBX";
	if(kodBanky == "1100")txtSwift.value = "TATRSKBX";
	if(kodBanky == "1111")txtSwift.value = "UNCRSKBX";
	if(kodBanky == "3000")txtSwift.value = "SLZBSKBA";
	if(kodBanky == "3100")txtSwift.value = "LUBASKBX";
	if(kodBanky == "5200")txtSwift.value = "OTPVSKBX";
	if(kodBanky == "5600")txtSwift.value = "KOMASK2X";
	if(kodBanky == "5900")txtSwift.value = "PRVASKBA";
	if(kodBanky == "6500")txtSwift.value = "POBNSKBA";
	if(kodBanky == "7300")txtSwift.value = "INGBSKBX";
	if(kodBanky == "7500")txtSwift.value = "CEKOSKBX";
	if(kodBanky == "7930")txtSwift.value = "WUSTSKBA";
	if(kodBanky == "8050")txtSwift.value = "COBASKBX";
	if(kodBanky == "8100")txtSwift.value = "KOMBSKBA";
	if(kodBanky == "8120")txtSwift.value = "BSLOSK22";
	if(kodBanky == "8130")txtSwift.value = "CITISKBA";
	if(kodBanky == "8160")txtSwift.value = "EXSKSKBX";
	if(kodBanky == "8170")txtSwift.value = "KBSPSKBX";
	if(kodBanky == "8180")txtSwift.value = "SPSRSKBA";
	if(kodBanky == "8320")txtSwift.value = "JTBPSKBA";
	if(kodBanky == "8330")txtSwift.value = "FIOZSKBA";
	if(kodBanky == "8360")txtSwift.value = "BREXSKBX";
	if(kodBanky == "8370")txtSwift.value = "OBKLSKBA";
	if(kodBanky == "8390")txtSwift.value = "";
	if(kodBanky == "8400")txtSwift.value = "";
	if(kodBanky == "8420")txtSwift.value = "BFKKSKBB";
	if(kodBanky == "8430")txtSwift.value = "KODBSKBX";
	if(kodBanky == "8440")txtSwift.value = "BNPASA";
	if(kodBanky == "9952")txtSwift.value = "TPAYSKBX";
	if(kodBanky == "9953")txtSwift.value = "";
}
function clearResult(){
	txtIban.value = "";
	txtIban2.value = "";
	txtSwift.value = "";
	txtIbanValidator.value = "";
	txtVysledokVyhodnotenia.value = "";
	document.getElementById("txtVysledokVyhodnotenia").style.backgroundColor = "#f2f2f2";
}
function clearValidate(){
	txtVysledokVyhodnotenia.value = "";
	document.getElementById("txtVysledokVyhodnotenia").style.backgroundColor = "#f2f2f2";
}
function validateIban(){
	var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
	var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
	var iban = txtIbanValidator.value;
	if(iban.length > 0){	
		var ibanConvert = "";
		for(var i = 0; i < iban.length; i++){
			var temp = iban.slice(i, i + 1);
			if (temp == " ") {
			} else {
				ibanConvert += temp;
			}			
		}	
		var temp4 = ibanConvert.slice(2, 4);
		var checkTrueIban = checkIban(ibanConvert, S, K, temp4);
		if(checkTrueIban == 1){
			txtVysledokVyhodnotenia.value = "IBAN je platný";		
			document.getElementById("txtVysledokVyhodnotenia").style.backgroundColor = "#f2f2f2";
		} else{
			txtVysledokVyhodnotenia.value = "IBAN je neplatný!!!";
			document.getElementById("txtVysledokVyhodnotenia").style.backgroundColor = "#ff3300";
		}	
	}else{
		txtVysledokVyhodnotenia.value = "Nie je zadaný žiadný IBAN";
		txtIbanValidator.focus();
	}	
}
</script>