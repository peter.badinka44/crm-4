@extends('master')
@section('title', 'Kalkulačky')

@section('content')

	<div class="fullscreen p-1">

		<div style="position: fixed; top: 5px; right: 5px;">
			@include('menu')
		</div>

		<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Investor</a>
			<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
			<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
		</div>
		</nav>
		<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
			@include('calculators.investor')
		</div>
		<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">Profile</div>
		<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">Contact</div>
		</div>

	</div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',
	data: () => ({
		msg : 'Ahooooooooooj'
	}),

	methods: {

	},


})
</script>
@endsection