<head>
	<title>Investor Plus</title>
</head>

<style>
  .page_swap{
    margin-right: 0%;
  }
  .vstup{
    height: 32px;
  }
    table, td, th {    
        border: 1px solid #000000;
        text-align: right;
    }

    table {
        border-collapse: collapse;    
    }

    th, td {
        padding: 5px;     
    }
    th {
        background-color: #f2f2f2;
    }
    .popText{
    font-size: 14px;
    color: #8c8c8c;
    }
    input[type=number]{
    width: 169px;
    padding: 5px 5px;
    }
    select[type=text]{
    width: 169px;
    padding: 5px 5px;
    }

    .fieldset1{
    padding: 5px 5px;
    float: left;
    width: 170px;
    border: none;  
    }

    .fieldset2{ 
    padding: 5px 5px;
    border: none;
    width: 170px;
    }

    .clear{
    clear:both;
    }

    .button {
    background: #7bbeeb;
    background-image: -webkit-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -moz-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -ms-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -o-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: linear-gradient(to bottom, #7bbeeb, #2980b9);
    -webkit-border-radius: 28;
    -moz-border-radius: 28;
    border-radius: 28px;
    font-family: Georgia;
    color: #ffffff;
    font-size: 18px;
    padding: 5px 5px 5px 5px;
    text-decoration: none;
    width: 100%;
    margin-top: 5px;
    }

    .button:hover {
    background: #3cb0fd;
    background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
    background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
    text-decoration: none;
    }
</style>
  <fieldset class="fieldset1">
  
    <font class="popText">Pravidelná investícia:</font>
    <br><input id="pravidelnaInvesticia" class="vstup" type="number" value="50" step="10" min="0" onchange="celkovyVypocet()">
    
    <br><font class="popText">Počet rokov:</font>
    <br><input id="pocetRokov"  class="vstup" type="number" value="30" min="0" onchange="celkovyVypocet()">    
  
  </fieldset>
  
  <fieldset class="fieldset2">
    
    <font class="popText">Interval:</font>
    <br><select id="akoCasto" class="vstup" type="text" onchange="celkovyVypocet()">
      <option valu="mesačne">mesačne</option>
      <option valu="štvrťročne">štvrťročne</option>
      <option valu="polročne">polročne</option>
      <option valu="ročne">ročne</option>
    </select>
    
    <br><font class="popText">Jednorazová investícia:</font>
    <br><input id="vstupnaInvesticia" class="vstup" type="number" value="0" step="500" min="0" onchange="celkovyVypocet()">
  
  </fieldset>

  <!-- <fieldset class="fieldset2"> -->
  <label class="showPar" onclick="showNastavenia()"><u>Zobraziť/upraviť vsetky vstupné parametre</u></label>
  <style>
  .showPar{
    padding: 8px 8px;
    color: blue;
    font-size: 18px;
  }
  </style>
  <!-- </fieldset> -->
  
  <hr class="clear">
  
  <div id="nastavenia" style="display:none;"> 
  
    <fieldset class="fieldset1">
  
      <font class="popText">Banky - zhodnotenie:</font>
      <br><input id="zhodnotenieBanky" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Poisťovne - zhodnotenie:</font>
      <br><input id="zhodnoteniPoistovne" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Dlhopisové f. - zhodnotenie:</font>
      <br><input id="zhodnoteniDlhopisy" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Realitné f. - zhodnotenie:</font>
      <br><input id="zhodnoteniReality" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Akciové f. - zhodnotenie:</font>
      <br><input id="zhodnoteniAkcie" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Indexové f. - zhodnotenie:</font>
      <br><input id="zhodnoteniIndexy" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Inflácia:</font>
      <input id="idInflacia" type="text" onchange="celkovyVypocet()">
    
    </fieldset>
    
    <fieldset class="fieldset2">
    
      <font class="popText">Banky - poplatky:</font>
      <br><input id="poplatkyBanky" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Poisťovne - poplatky:</font>
      <br><input id="poplatkyPoistovne" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Dlhopisové f. - poplatky:</font>
      <br><input id="poplatkyDlhopisy" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Realitné f. - poplatky:</font>
      <br><input id="poplatkyReality" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Akciové f. - poplatky:</font>
      <br><input id="poplatkyAkcie" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Indexové f. - poplatky:</font>
      <br><input id="poplatkyIndexy" type="text" onchange="celkovyVypocet()">
      <br><font class="popText">Daň zo zisku:</font>
      <input id="idDan" type="text" onchange="celkovyVypocet()">
    
    </fieldset>
    
  <hr class="clear">
  </div>

  <table id="pDataTable" class="display" text-align="left" onclick="" style="display:none;">
    <colgroup>
      <col span="7">
      <col style="background-color:WhiteSmoke">
    </colgroup>
      <thead>
      <tr> 
        <th></th>
        <th width="130px">Banky</th>
        <th width="130px">Poisťovne</th>
        <th width="130px">Dlhopisové fondy</th>
        <th width="130px">Realitné fondy</th>
        <th width="130px">Akciové fondy</th>
        <th width="130px">Indexové fondy</th>
        <th width="130px">Vklad</th>
      </tr> 
      </thead>
  </table>
  
  <button style="display:none;" accesskey="q" onclick="showNastavenia()">Click</button>
  <button style="display:none;" accesskey="w" onclick="celkovyVypocetBonus()">Click</button>
  <font class="popText" style="display:none;">Investujem:</font>
  <br><select id="investujem" type="text" onchange="celkovyVypocet()" style="display:none;">
  <option value="pravidelne">pravidelne</option>
  <option value="jednorázovo">jednorázovo</option>
  </select>
 
<script>  
  onloadFunction();
  celkovyVypocetBonus();
    var suma, zhodnotenie = 0, rocnyVkladNetto, rocnyVkladBrutto, hodnotaUctu, vstupInvesticia;

    var zBanky = 0, popBanky = 0,
    zPoistovne = 0, popPoistovne = 0,
    zDlhopisy = 0, popDlhopisy = 0,
    zReality = 0, popReality = 0,
    zAkcie = 0, popAkcie = 0,
    zIndexy = 0, popIndexy = 0;

    var danZoZisku, inflacia;

    var pBanky = [];
    var pPoistovne = [];
    var pDlhopisy = [];
    var pReality = [];
    var pAkcie = [];
    var pIndexy = [];

    var bonusVisible = true;

    //=================================================================
    // Onload
    //=================================================================
    function onloadFunction(){
    zBanky = zhodnotenieBanky.value = 0.3;
    popBanky = poplatkyBanky.value = 0.2;
    zPoistovne = zhodnoteniPoistovne.value = 7.7;
    popPoistovne = poplatkyPoistovne.value = 3.2;
    zDlhopisy = zhodnoteniDlhopisy.value = 5.6;
    popDlhopisy = poplatkyDlhopisy.value = 2.2;
    zReality = zhodnoteniReality.value = 6.3;
    popReality = poplatkyReality.value = 2.6;
    zAkcie = zhodnoteniAkcie.value = 9.7;
    popAkcie = poplatkyAkcie.value = 2.5;
    zIndexy = zhodnoteniIndexy.value = 14.3;
    popIndexy = poplatkyIndexy.value = 1.6;
    danZoZisku = idDan.value = 20;
    inflacia = idInflacia.value = 1.7;
    }

    //=================================================================
    // Hlavne funcia
    //=================================================================
    function celkovyVypocet(){
    zBanky = zhodnotenieBanky.value;
    popBanky = poplatkyBanky.value;
    zPoistovne = zhodnoteniPoistovne.value;
    popPoistovne = poplatkyPoistovne.value;
    zDlhopisy = zhodnoteniDlhopisy.value;
    popDlhopisy = poplatkyDlhopisy.value;
    zReality = zhodnoteniReality.value;
    popReality = poplatkyReality.value;
    zAkcie = zhodnoteniAkcie.value;
    popAkcie = poplatkyAkcie.value;
    zIndexy = zhodnoteniIndexy.value;
    popIndexy = poplatkyIndexy.value;
    danZoZisku = idDan.value;
    inflacia = idInflacia.value;
    
    if(pocetRokov.value > 0 && (pravidelnaInvesticia.value > 0 || vstupnaInvesticia.value > 0)){
        document.getElementById('pDataTable').style.display = 'block';
        pravidelneInvestovanie();
    } else document.getElementById('pDataTable').style.display = 'none';
    }

    //=================================================================
    // Hlavne funcia + zmena ui
    //=================================================================
    function celkovyVypocetBonus(){
    if(bonusVisible == true) bonusVisible = false; else bonusVisible = true;
    celkovyVypocet();
    }

    //=================================================================
    // Pravidelne Investovanie
    //=================================================================
    function pravidelneInvestovanie(){
    suma = pravidelnaInvesticia.value; 
    vstupInvesticia = vstupnaInvesticia.value;

    if(investujem.value == "jednorázovo"){
        rocnyVkladNetto = jednorazovyVklad.value;
    } else if(investujem.value == "pravidelne"){
        if(akoCasto.value == "mesačne"){
        rocnyVkladNetto = suma * 12;
        }else if(akoCasto.value == "štvrťročne"){
        rocnyVkladNetto = suma * 4;
        }else if(akoCasto.value == "polročne"){
        rocnyVkladNetto = suma * 2;
        }else if(akoCasto.value == "ročne"){
        rocnyVkladNetto = suma;
        }
    }
    fBanky();
    fPoistovne();
    fDlhopisy();
    fReality();
    fAkcie();
    fIndexy();
    reloadTable();  
    if(bonusVisible == true){
        tablePoplatky();
        tableDan();
        tableZisk();
        tableInflacia();
        zhodnotenieNetto();
    }
    }


    //=================================================================
    // Nacita data do tabulky
    //=================================================================
    function reloadTable(){
    var rowsNum = pDataTable.rows.length;
    for (var i = 1; i < rowsNum; i++) { //Clear table
        document.getElementById("pDataTable").deleteRow(1);
    } 
    for(var i = 1; i <= pocetRokov.value; i++){  
        var newRow = pDataTable.insertRow(1),    
        cell1 = newRow.insertCell(0),
        cell2 = newRow.insertCell(1),
        cell3 = newRow.insertCell(2),
        cell4 = newRow.insertCell(3),
        cell5 = newRow.insertCell(4),
        cell6 = newRow.insertCell(5),
        cell7 = newRow.insertCell(6),
        cell8 = newRow.insertCell(7);
        
        if(i == pocetRokov.value) newRow.style.backgroundColor = "rgb(176, 216, 255)";
        
        cell1.innerHTML = Number.parseFloat(pBanky[i][0]).toFixed(0); 
        cell2.innerHTML = numberFormat(Number.parseFloat(pBanky[i][3]).toFixed(0), " ");
        cell3.innerHTML = numberFormat(Number.parseFloat(pPoistovne[i][3]).toFixed(0), " ");
        cell4.innerHTML = numberFormat(Number.parseFloat(pDlhopisy[i][3]).toFixed(0), " ");
        cell5.innerHTML = numberFormat(Number.parseFloat(pReality[i][3]).toFixed(0), " ");
        cell6.innerHTML = numberFormat(Number.parseFloat(pAkcie[i][3]).toFixed(0), " ");
        cell7.innerHTML = numberFormat(Number.parseFloat(pIndexy[i][3]).toFixed(0), " ");
        var tempVklad = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        cell8.innerHTML = numberFormat(Number.parseFloat(tempVklad).toFixed(0), " ");
    }
    }


    var xpopBanky, xpopPoistovne, xpopDlhopisy, xpopReality, xpopAkcie, xpopIndexy;
    //=================================================================
    // Table poplatky
    //=================================================================
    function tablePoplatky(){
    var newRow = pDataTable.insertRow(1),    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3),
    cell5 = newRow.insertCell(4),
    cell6 = newRow.insertCell(5),
    cell7 = newRow.insertCell(6);  
    cell1.innerHTML = "Poplatky";  
    newRow.style.backgroundColor = "rgb(249, 209, 134)";
    var sumPopBanky = 0;
    for(var i = 1; i < pBanky.length; i++)sumPopBanky += pBanky[i][4]; xpopBanky = sumPopBanky;  
    cell2.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");  
    sumPopBanky = 0;
    for(var i = 1; i < pPoistovne.length; i++) sumPopBanky += pPoistovne[i][4];
    sumPopBanky += (rocnyVkladNetto * 2); xpopPoistovne = sumPopBanky;  
    cell3.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");  
    sumPopBanky = 0;
    for(var i = 1; i < pDlhopisy.length; i++) sumPopBanky += pDlhopisy[i][4]; xpopDlhopisy = sumPopBanky;
    cell4.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");
    sumPopBanky = 0;
    for(var i = 1; i < pReality.length; i++) sumPopBanky += pReality[i][4]; xpopReality = sumPopBanky;
    cell5.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");
    sumPopBanky = 0;
    for(var i = 1; i < pAkcie.length; i++) sumPopBanky += pAkcie[i][4]; xpopAkcie = sumPopBanky;
    cell6.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");
    sumPopBanky = 0;
    for(var i = 1; i < pAkcie.length; i++) sumPopBanky += pIndexy[i][4]; xpopIndexy = sumPopBanky;
    cell7.innerHTML = "- " + numberFormat(Number.parseFloat(sumPopBanky).toFixed(0), " ");
    }

    var xDanBanky, xDanPoistovne, xDanDlhopisy, xDanReality, xDanAkcie, xDanIndexy;
    //=================================================================
    // Table dan
    //=================================================================
    function tableDan(){
    var newRow = pDataTable.insertRow(1),    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3),
    cell5 = newRow.insertCell(4),
    cell6 = newRow.insertCell(5),
    cell7 = newRow.insertCell(6);   
    newRow.style.backgroundColor = "rgb(243, 159, 241)";
    cell1.innerHTML = "Daň";
    var i = pocetRokov.value;
    if(xpopPoistovne < 0) xpopPoistovne = xpopPoistovne * -1;
    var dan = (pBanky[i][3] - (rocnyVkladNetto * i) - vstupnaInvesticia.value) - xpopBanky; 
    dan = (dan / 100) * danZoZisku; if(dan < 0) dan = 0; xDanBanky = dan;
    cell2.innerHTML = "- " + numberFormat(Number.parseFloat(dan).toFixed(0), " "); 
    dan = (pPoistovne[i][3] - (rocnyVkladNetto * i) - vstupnaInvesticia.value) - xpopPoistovne;
    dan = (dan / 100) * danZoZisku; if(dan < 0) dan = 0; xDanPoistovne = dan;
    cell3.innerHTML = "- " + numberFormat(Number.parseFloat(dan).toFixed(0), " ");
    dan = (pDlhopisy[i][3] - (rocnyVkladNetto * i) - vstupnaInvesticia.value) - xpopDlhopisy;
    dan = (dan / 100) * danZoZisku; if(dan < 0) dan = 0; xDanDlhopisy = dan;
    cell4.innerHTML = "- " + numberFormat(Number.parseFloat(dan).toFixed(0), " ");
    dan = (pReality[i][3] - (rocnyVkladNetto * i) - vstupnaInvesticia.value) - xpopReality;
    dan = (dan / 100) * danZoZisku; if(dan < 0) dan = 0; xDanReality = dan;
    cell5.innerHTML = "- " + numberFormat(Number.parseFloat(dan).toFixed(0), " ");
    dan = (pAkcie[i][3] - (rocnyVkladNetto * i) - vstupnaInvesticia.value) - xpopAkcie;
    dan = (dan / 100) * danZoZisku; if(dan < 0) dan = 0; xDanAkcie = dan;
    cell6.innerHTML = "- " + numberFormat(Number.parseFloat(dan).toFixed(0), " ");
    dan = 0; xDanIndexy = dan;
    cell7.innerHTML = "- " + numberFormat(Number.parseFloat(0).toFixed(0), " ");  
    }

    //=================================================================
    // Table zisk
    //=================================================================
    function tableZisk(){
    var newRow = pDataTable.insertRow(1),    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3),
    cell5 = newRow.insertCell(4),
    cell6 = newRow.insertCell(5),
    cell7 = newRow.insertCell(6);
    //cell8 = newRow.insertCell(7);
    newRow.style.backgroundColor = "rgb(179, 228, 92)";
    cell1.innerHTML = "Hodnota účtu";
    var i = pocetRokov.value;
    var tempMinus = "";
    
    var tempZisk = pBanky[i][3] - xDanBanky - xpopBanky; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell2.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pPoistovne[i][3] - xDanPoistovne - xpopPoistovne;   
    
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell3.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pDlhopisy[i][3] - xDanDlhopisy - xpopDlhopisy;
    console.log("zisk... " + tempZisk);
    console.log("pDlhopisy[i][3]... " + pDlhopisy[i][3]);
    console.log("zisk... " + tempZisk);
    console.log("zisk... " + tempZisk);
    console.log("zisk... " + tempZisk);
    
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell4.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pReality[i][3] - xDanReality - xpopReality; 
    
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell5.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pAkcie[i][3] - xDanAkcie - xpopAkcie; 
    
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell6.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pIndexy[i][3] - xDanIndexy - xpopIndexy; 
    
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell7.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " "); 
    
    tempZisk = (parseFloat(vstupnaInvesticia.value) + parseFloat(rocnyVkladNetto * i)) - xInflVklad;
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    //cell8.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " "); 
    }

    var xInflBanky, xInflPoistovne, xInflDlhopisy, xInflReality, xInflAkcie, xInflIndexy, xInflVklad;
    //=================================================================
    // Table inflacia
    //=================================================================
    function tableInflacia(){
    var newRow = pDataTable.insertRow(1),    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3),
    cell5 = newRow.insertCell(4),
    cell6 = newRow.insertCell(5),
    cell7 = newRow.insertCell(6);
    
    newRow.style.backgroundColor = "rgb(251, 174, 146)";
    cell1.innerHTML = "Inflácia";
    var tempMinus = "";
    var i = pocetRokov.value;
    
    var tempInf = xInflBanky = (((Number(pBanky[i][3])) - (Number(xDanBanky) + Number(xpopBanky))) / 100) * (inflacia * i); 
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell2.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");
    
    tempInf = xInflPoistovne = (((Number(pPoistovne[i][3])) - (Number(xDanPoistovne) + Number(xpopPoistovne))) / 100) * (inflacia * i);  
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell3.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");
    
    tempInf = xInflDlhopisy = (((Number(pDlhopisy[i][3])) - (Number(xDanDlhopisy) + Number(xpopDlhopisy))) / 100) * (inflacia * i);
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell4.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " "); 
    
    tempInf = xInflReality = (((Number(pReality[i][3])) - (Number(xDanReality) + Number(xpopReality))) / 100) * (inflacia * i);
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell5.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");
    
    tempInf = xInflAkcie = (((Number(pAkcie[i][3])) - (Number(xDanAkcie) + Number(xpopAkcie))) / 100) * (inflacia * i);
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell6.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");
    
    tempInf = xInflIndexy = (((Number(pIndexy[i][3])) - (Number(xDanIndexy) + Number(xpopIndexy))) / 100) * (inflacia * i);
    if(tempInf > 0) tempMinus = "- "; else tempMinus = "";
    cell7.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");

    tempInf = parseFloat(vstupnaInvesticia.value);
    tempInf += parseFloat(rocnyVkladNetto * i);
    tempMinus = "- ";
    tempInf = (tempInf / 100) * (inflacia * i); xInflVklad = tempInf;
    //cell8.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempInf).toFixed(0), " ");

    }

    //=================================================================
    // Table zhodnotenie netto
    //=================================================================
    function zhodnotenieNetto(){
    var newRow = pDataTable.insertRow(1),    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3),
    cell5 = newRow.insertCell(4),
    cell6 = newRow.insertCell(5),
    cell7 = newRow.insertCell(6);
    //cell8 = newRow.insertCell(7);
    newRow.style.backgroundColor = "rgb(239, 253, 121)";
    cell1.innerHTML = "Kúpna sila";
    var i = pocetRokov.value;
    var tempMinus = "";
    var tempZisk = pBanky[i][3] - xInflBanky - xDanBanky - xpopBanky; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell2.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pPoistovne[i][3] - xInflPoistovne - xDanPoistovne - xpopPoistovne; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell3.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pDlhopisy[i][3] - xInflDlhopisy - xDanDlhopisy - xpopDlhopisy;
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell4.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pReality[i][3] - xInflReality - xDanReality - xpopReality; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell5.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pAkcie[i][3] - xInflAkcie - xDanAkcie - xpopAkcie; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell6.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " ");
    tempZisk = pIndexy[i][3] - xInflIndexy - xDanIndexy - xpopIndexy; 
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    cell7.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " "); 
    
    tempZisk = (parseFloat(vstupnaInvesticia.value) + parseFloat(rocnyVkladNetto * i)) - xInflVklad;
    if(tempZisk < 0){ tempZisk = tempZisk * -1; tempMinus = "- "; } else tempMinus = "";
    //cell8.innerHTML = tempMinus + numberFormat(Number.parseFloat(tempZisk).toFixed(0), " "); 
    }

    //=================================================================
    // Banky
    //=================================================================
    function fBanky(){
    pBanky = [];
    pBanky[1] = [];
    pBanky[1][0] = "1";
    pBanky[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pBanky[1][2] = "0";
    pBanky[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pBanky[1][4] = Number(pBanky[1][3] / 100) * Number(popBanky);
    for(var i = 2; i <= pocetRokov.value; i++){      
        pBanky[i] = [];
        pBanky[i][0] = i;
        pBanky[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        pBanky[i][2] = Number(pBanky[i-1][2]) + (parseFloat(pBanky[i - 1][3]) / 100) * zBanky;
        pBanky[i][3] = Number(pBanky[i][1]) + Number(pBanky[i][2]);
        pBanky[i][4] = Number(pBanky[i][3] / 100) * Number(popBanky);    
    }  
    }

    //=================================================================
    // Poistovne
    //=================================================================
    function fPoistovne(){
    pPoistovne = [];
    pPoistovne[1] = [];
    pPoistovne[1][0] = "1";
    pPoistovne[1][1] = parseFloat(vstupnaInvesticia.value);
    pPoistovne[1][2] = "0";
    pPoistovne[1][3] = parseFloat(vstupnaInvesticia.value);
    pPoistovne[1][4] = Number(pPoistovne[1][3] / 100) * Number(popPoistovne);
    pPoistovne[2] = [];
    pPoistovne[2][0] = "2";
    pPoistovne[2][1] = parseFloat(vstupnaInvesticia.value);
    pPoistovne[2][2] = pPoistovne[2][2] = Number(pPoistovne[1][2]) + (parseFloat(pPoistovne[1][3]) / 100) * zPoistovne;
    pPoistovne[2][3] = Number(pPoistovne[2][1]) + Number(pPoistovne[2][2]);
    pPoistovne[2][4] = Number(pPoistovne[2][3] / 100) * Number(popPoistovne);
    for(var i = 3; i <= pocetRokov.value; i++){      
        pPoistovne[i] = [];
        pPoistovne[i][0] = i;
        pPoistovne[i][1] = parseFloat(rocnyVkladNetto * (i - 2)) + parseFloat(vstupnaInvesticia.value);
        pPoistovne[i][2] = Number(pPoistovne[i-1][2]) + (parseFloat(pPoistovne[i - 1][3]) / 100) * zPoistovne;
        pPoistovne[i][3] = Number(pPoistovne[i][1]) + Number(pPoistovne[i][2]);
        pPoistovne[i][4] = Number(pPoistovne[i][3] / 100) * Number(popPoistovne);
    }  
    }

    //=================================================================
    // Dlhopisy
    //=================================================================
    function fDlhopisy(){
    pDlhopisy = [];
    pDlhopisy[1] = [];
    pDlhopisy[1][0] = "1";
    pDlhopisy[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pDlhopisy[1][2] = "0";
    pDlhopisy[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pDlhopisy[1][4] = Number(pDlhopisy[1][3] / 100) * Number(popDlhopisy);
    for(var i = 2; i <= pocetRokov.value; i++){      
        pDlhopisy[i] = [];
        pDlhopisy[i][0] = i;
        pDlhopisy[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        pDlhopisy[i][2] = Number(pDlhopisy[i-1][2]) + (parseFloat(pDlhopisy[i - 1][3]) / 100) * zDlhopisy;
        pDlhopisy[i][3] = Number(pDlhopisy[i][1]) + Number(pDlhopisy[i][2]);
        pDlhopisy[i][4] = Number(pDlhopisy[i][3] / 100) * Number(popDlhopisy);
    }  
    }

    //=================================================================
    // Realitne fondy
    //=================================================================
    function fReality(){
    pReality = [];
    pReality[1] = [];
    pReality[1][0] = "1";
    pReality[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pReality[1][2] = "0";
    pReality[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pReality[1][4] = Number(pReality[1][3] / 100) * Number(popReality);
    for(var i = 2; i <= pocetRokov.value; i++){      
        pReality[i] = [];
        pReality[i][0] = i;
        pReality[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        pReality[i][2] = Number(pReality[i-1][2]) + (parseFloat(pReality[i - 1][3]) / 100) * zReality;
        pReality[i][3] = Number(pReality[i][1]) + Number(pReality[i][2]);
        pReality[i][4] = Number(pReality[i][3] / 100) * Number(popReality);
    }  
    }

    //=================================================================
    // Akcie
    //=================================================================
    function fAkcie(){
    pAkcie = [];
    pAkcie[1] = [];
    pAkcie[1][0] = "1";
    pAkcie[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pAkcie[1][2] = "0";
    pAkcie[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pAkcie[1][4] = Number(pAkcie[1][3] / 100) * Number(popAkcie)
    for(var i = 2; i <= pocetRokov.value; i++){      
        pAkcie[i] = [];
        pAkcie[i][0] = i;
        pAkcie[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        pAkcie[i][2] = Number(pAkcie[i-1][2]) + (parseFloat(pAkcie[i - 1][3]) / 100) * zAkcie;
        pAkcie[i][3] = Number(pAkcie[i][1]) + Number(pAkcie[i][2]);
        pAkcie[i][4] = Number(pAkcie[i][3] / 100) * Number(popAkcie);    
    }  
    }

    //=================================================================
    // Indexy
    //=================================================================
    function fIndexy(){
    pIndexy = [];
    pIndexy[1] = [];
    pIndexy[1][0] = "1";
    pIndexy[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pIndexy[1][2] = "0";
    pIndexy[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
    pIndexy[1][4] = Number(pIndexy[1][3] / 100) * Number(popIndexy)
    for(var i = 2; i <= pocetRokov.value; i++){      
        pIndexy[i] = [];
        pIndexy[i][0] = i;
        pIndexy[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
        pIndexy[i][2] = Number(pIndexy[i-1][2]) + (parseFloat(pIndexy[i - 1][3]) / 100) * zIndexy;
        pIndexy[i][3] = Number(pIndexy[i][1]) + Number(pIndexy[i][2]);
        pIndexy[i][4] = Number(pIndexy[i][3] / 100) * Number(popIndexy);
    }  
    }

    //=================================================================
    // Uprava formatu
    //=================================================================
    function numberFormat(_number, _sep) {
        if(_number == 0){
        return 0;
        } else{
        _number = typeof _number != "undefined" && _number > 0 ? _number : "";
        _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
        if(typeof _sep != "undefined" && _sep != " ") {
            _number = _number.replace(/\s/g, _sep);
        }
        return _number;    
        }
    }

    //=================================================================
    // Zobrazit nastavenia
    //=================================================================
    function showNastavenia() {
    var tempShow = document.getElementById('nastavenia').style.display;
    if(tempShow == 'none') document.getElementById('nastavenia').style.display = 'block';
    else document.getElementById('nastavenia').style.display = 'none';
    //console.log("ccc");
    }
  
</script>