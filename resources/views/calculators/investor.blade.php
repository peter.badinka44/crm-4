<head>
	<title>Investor</title>
</head>

<style>
.page_swap{
  margin-right: 0%;
  margin-top: 0%;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: center;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 5px;
}
th {
    background-color: #f2f2f2;
}
.popText{
  font-size: 14px;
  color: #8c8c8c;
}
input[type=number]{
  width: 169px;
  padding: 5px 5px;
}
select[type=text]{
  width: 169px;
  padding: 5px 5px;
}

.fieldset1{
  float: left;
  width: 170px;
  border: 0.5px solid #666;
}

.fieldset2{  
  border: 0.5px solid #666;
  width: 600px;
}

.button {
  background: #7bbeeb;
  background-image: -webkit-linear-gradient(top, #7bbeeb, #2980b9);
  background-image: -moz-linear-gradient(top, #7bbeeb, #2980b9);
  background-image: -ms-linear-gradient(top, #7bbeeb, #2980b9);
  background-image: -o-linear-gradient(top, #7bbeeb, #2980b9);
  background-image: linear-gradient(to bottom, #7bbeeb, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Georgia;
  color: #ffffff;
  font-size: 18px;
  padding: 5px 5px 5px 5px;
  text-decoration: none;
  width: 100%;
  margin-top: 5px;
}

.button:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}

.vstup{
    height: 32px;
  }

</style>

<fieldset class="fieldset1">

  <font class="popText">Interval:</font>
  <br><select id="akoCasto" type="text" onchange="celkovyVypocet()">
    <option valu="mesačne">mesačne</option>
    <option valu="štvrťročne">štvrťročne</option>
    <option valu="polročne">polročne</option>
    <option valu="ročne">ročne</option>
  </select>
  
  <font class="popText">Pravidelná investícia:</font>
  <br><input id="pravidelnaInvesticia" class="vstup" type="number" value="50" step="10" min="0" onchange="celkovyVypocet()">
  
  <br><font class="popText">Jednorazová investícia:</font>
  <br><input id="vstupnaInvesticia" class="vstup" type="number" value="0" step="500" min="0" onchange="celkovyVypocet()">
  
  <br><font class="popText">Počet rokov:</font>
  <br><input id="pocetRokov" class="vstup" type="number" value="30" min="0" onchange="celkovyVypocet()">
  
  <br><font class="popText">Zhodnotenie (p.a.):</font>
  <br><input id="vynos" class="vstup" type="number" value="5" onchange="celkovyVypocet()">
  
  
</fieldset> 

<fieldset class="fieldset2">
  
  <table id="pDataTable" class="display" text-align="left" onclick="">
      <thead>
      <tr> 
        <th width="20px" class="num"></th>
        <th width="120px">Hodnota účtu</th>
        <th width="120px">Vklad</th>
        <th width="120px">Zhodnotenie</th>
      </tr> 
      </thead>
  </table>
  
</fieldset> 

  <font class="popText" style="display:none;">Investujem:</font>
  <br><select id="investujem" type="text" onchange="celkovyVypocet()" style="display:none;">
    <option value="pravidelne">pravidelne</option>
    <option value="jednorázovo">jednorázovo</option>
  </select>

<script>
var suma, zhodnotenie, rocnyVkladNetto, rocnyVkladBrutto, hodnotaUctu;

celkovyVypocet();
function celkovyVypocet(){  
  if(investujem.value == "pravidelne"){
     pravidelneInvestovanie();
  } else if(investujem.value == "jednorázovo"){
     jednorazoveInvestovanie();
  }
}

function pravidelneInvestovanie(){

  var rowsNum = pDataTable.rows.length;
  for (var i = 1; i < rowsNum; i++) { //Clear table
    document.getElementById("pDataTable").deleteRow(1);
  }  

  suma = pravidelnaInvesticia.value; 
  var vstupInvesticia = vstupnaInvesticia.value;

  if(investujem.value == "jednorázovo"){
    rocnyVkladNetto = jednorazovyVklad.value;
  } else if(investujem.value == "pravidelne"){
    if(akoCasto.value == "mesačne"){
      rocnyVkladNetto = suma * 12;
    }else if(akoCasto.value == "štvrťročne"){
      rocnyVkladNetto = suma * 4;
    }else if(akoCasto.value == "polročne"){
      rocnyVkladNetto = suma * 2;
    }else if(akoCasto.value == "ročne"){
      rocnyVkladNetto = suma;
    }
  }  
  
  var pole = [];
  pole[1] = [];
  pole[1][0] = "1";
  pole[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
  pole[1][2] = "0";
  pole[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
  pole[1][4] = parseFloat(pole[1][3] / 12) / 10;
  pole[1][5] = parseFloat(pole[1][3] / 12) / 15;
  pole[1][6] = parseFloat(pole[1][3] / 12) / 20;
  
  console.log(pole[1][2]);
  
  for(var i = 2; i <= pocetRokov.value; i++){      
    pole[i] = [];
    pole[i][0] = i;
    pole[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
    pole[i][2] = Number(pole[i-1][2]) + (parseFloat(pole[i - 1][3]) / 100) * vynos.value;
    pole[i][3] = Number(pole[i][1]) + Number(pole[i][2]);
    pole[i][4] = Number.parseFloat((pole[i][3] / 12) / 10);
    pole[i][5] = Number.parseFloat((pole[i][3] / 12) / 15);
    pole[i][6] = Number.parseFloat((pole[i][3] / 12) / 20);
  }
  
  for(var i = 1; i <= pocetRokov.value; i++){
  
  var newRow = pDataTable.insertRow(1),
    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3);
    //cell5 = newRow.insertCell(4),
    //cell6 = newRow.insertCell(5),
    //cell7 = newRow.insertCell(6); 
    
    cell1.innerHTML = Number.parseFloat(pole[i][0]).toFixed(0);
    cell2.innerHTML = numberFormat(Number.parseFloat(pole[i][3]).toFixed(0), " ");
    cell3.innerHTML = numberFormat(Number.parseFloat(pole[i][1]).toFixed(0), " ");
    
    if(vynos.value >= 0){
      cell4.innerHTML = numberFormat(Number.parseFloat(pole[i][2]).toFixed(0), " ");
    } else {
      cell4.innerHTML = Number.parseFloat(pole[i][2]).toFixed(0);
    }
    
    //cell4.innerHTML = numberFormat(Number.parseFloat(pole[i][2]).toFixed(0), " ");    
    //cell5.innerHTML = numberFormat(Number.parseFloat(pole[i][4]).toFixed(0), " ");
    //cell6.innerHTML = numberFormat(Number.parseFloat(pole[i][5]).toFixed(0), " ");
    //cell7.innerHTML = numberFormat(Number.parseFloat(pole[i][6]).toFixed(0), " ");
  
  }  
  
  function numberFormat(_number, _sep) {
    if(_number == 0){
      return 0;
    } else{
      _number = typeof _number != "undefined" && _number > 0 ? _number : "";
      _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
      if(typeof _sep != "undefined" && _sep != " ") {
          _number = _number.replace(/\s/g, _sep);
      }
      return _number;    
    }
  }
}

function jednorazoveInvestovanie(){

  var rowsNum = pDataTable.rows.length;
  for (var i = 1; i < rowsNum; i++) { //Clear table
    document.getElementById("pDataTable").deleteRow(1);
  }  

  suma = vstupnaInvesticia.value;
  var vstupInvesticia = vstupnaInvesticia.value;

  rocnyVkladNetto = 0;
  
  var pole = [];
  pole[1] = [];
  pole[1][0] = "1";
  pole[1][1] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
  pole[1][2] = "0";
  pole[1][3] = parseFloat(rocnyVkladNetto) + parseFloat(vstupnaInvesticia.value);
  pole[1][4] = parseFloat(pole[1][3] / 12) / 10;
  pole[1][5] = parseFloat(pole[1][3] / 12) / 15;
  pole[1][6] = parseFloat(pole[1][3] / 12) / 20;
  
  console.log(pole[1][2]);
  
  for(var i = 2; i <= pocetRokov.value; i++){      
    pole[i] = [];
    pole[i][0] = i;
    pole[i][1] = parseFloat(rocnyVkladNetto * i) + parseFloat(vstupnaInvesticia.value);
    pole[i][2] = Number(pole[i-1][2]) + (parseFloat(pole[i - 1][3]) / 100) * vynos.value;
    pole[i][3] = Number(pole[i][1]) + Number(pole[i][2]);
    pole[i][4] = Number.parseFloat((pole[i][3] / 12) / 10);
    pole[i][5] = Number.parseFloat((pole[i][3] / 12) / 15);
    pole[i][6] = Number.parseFloat((pole[i][3] / 12) / 20);
  }
  
  for(var i = 1; i <= pocetRokov.value; i++){
  
  var newRow = pDataTable.insertRow(1),
    
    cell1 = newRow.insertCell(0),
    cell2 = newRow.insertCell(1),
    cell3 = newRow.insertCell(2),
    cell4 = newRow.insertCell(3);
    //cell5 = newRow.insertCell(4),
    //cell6 = newRow.insertCell(5),
    //cell7 = newRow.insertCell(6); 
    
    cell1.innerHTML = Number.parseFloat(pole[i][0]).toFixed(0);
    cell2.innerHTML = numberFormat(Number.parseFloat(pole[i][3]).toFixed(0), " ");
    cell3.innerHTML = numberFormat(Number.parseFloat(pole[i][1]).toFixed(0), " ");
    cell4.innerHTML = numberFormat(Number.parseFloat(pole[i][2]).toFixed(0), " ");
    //cell5.innerHTML = numberFormat(Number.parseFloat(pole[i][4]).toFixed(0), " ");
    //cell6.innerHTML = numberFormat(Number.parseFloat(pole[i][5]).toFixed(0), " ");
    //cell7.innerHTML = numberFormat(Number.parseFloat(pole[i][6]).toFixed(0), " ");
  
  }
  function numberFormat(_number, _sep) {
    if(_number == 0){
      return 0;
    } else{
      _number = typeof _number != "undefined" && _number > 0 ? _number : "";
      _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
      if(typeof _sep != "undefined" && _sep != " ") {
          _number = _number.replace(/\s/g, _sep);
      }
      return _number;    
    }
  }
}
</script>