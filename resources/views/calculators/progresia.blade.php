<head>
	<title>Progresia</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
</head>

<style>
    .page_swap{
        margin-right: 0%;
        margin-top: 0%;
    }
    table, td, th {    
        border: 1px solid #ddd;
        text-align: right;
    }

    table {
        border-collapse: collapse;
    }

    th, td {
        padding: 5px;     
    }
    th {
        background-color: #f2f2f2;
    }
    .popText{
    font-size: 14px;
    color: #8c8c8c;
    }
    input[type=number]{
    width: 155px;
    padding: 5px 5px;
    }
    select[type=text]{
    width: 169px;
    padding: 5px 5px;
    }

    .fieldset1{
    float: left;
    width: 150px;
    border: 0.5px solid #888;
    }

    .fieldset2{  
        border: 0.5px solid #888;
        width: 600px;        
    }

    .button {
    background: #7bbeeb;
    background-image: -webkit-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -moz-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -ms-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: -o-linear-gradient(top, #7bbeeb, #2980b9);
    background-image: linear-gradient(to bottom, #7bbeeb, #2980b9);
    -webkit-border-radius: 28;
    -moz-border-radius: 28;
    border-radius: 28px;
    font-family: Georgia;
    color: #ffffff;
    font-size: 18px;
    padding: 5px 5px 5px 5px;
    text-decoration: none;
    width: 100%;
    margin-top: 5px;
    }

    .button:hover {
    background: #3cb0fd;
    background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
    background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
    background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
    text-decoration: none;
    }
</style>

<fieldset class="fieldset1">

<font class="popText">Percento poškodenia:</font>
<br><input id="poskodenie" type="number" value="60" step="5" min="0" max="100" onchange="main()">
<br><font class="popText">Poistná suma:</font>
<br><input id="poistnaSuma" type="number" value="10000" step="5000" min="0" onchange="main()">

</fieldset> 

<fieldset class="fieldset2">
  <table id="pDataTable" class="display" text-align="left" onclick="">
      <thead>
      <tr> 
        <th width="150px"></th>
        <th width="150px">Odškodnenie (€)</th>
      </tr> 
      </thead>
  </table>
  
</fieldset> 

  <font class="popText" style="display:none;">Investujem:</font>
  <br><select id="investujem" type="text" onchange="celkovyVypocet()" style="display:none;">
    <option value="pravidelne">pravidelne</option>
    <option value="jednorázovo">jednorázovo</option>
  </select>
 
<script>
    $(document).ready(function(){
        main();
    });    
    var dataIndex = 15;
    var data = [];
    //=================================================================
    // Deklarovanie globalneho pola a jeho velkosti
    //=================================================================
    function declareArray(){
        for(var i = 0; i < dataIndex; i++){
            data[i] = [];
        }
    }
    //=================================================================
    // Hlavna funkcia
    //=================================================================
    function main(){
    declareArray();
    
    allianz500();
    allianz700();
    AXA400();
    AXA700();
    basler650();
    generali500();
    metlife500();
    metlife1000(); 
    nn800();
    uniqa600();
    aegon600();
    wustenrot400();
    novis200();
    kooperativa350();
    kooperativa500();
    
    reloadTable();
    }
    //=================================================================
    // Aegon - 600
    //=================================================================
    function aegon600(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 20) result = (suma / 100) * posk;
    if(posk >= 21 && posk <= 40) result = (suma / 100) * (2 * posk);
    if(posk >= 41 && posk <= 60) result = (suma / 100) * ((5 * posk) - 120);
    if(posk >= 61 && posk <= 80) result = (suma / 100) * ((7 * posk) - 240);
    if(posk >= 81 && posk <= 90) result = (suma / 100) * ((9 * posk) - 400);
    if(posk >= 91 && posk <= 100) result = (suma / 100) * ((20 * posk) - 1400);
    data[14][0] = "Aegon - 600";
    data[14][1] = result; 
    }
    //=================================================================
    // Allianz - 700
    //=================================================================
    function allianz700(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 20) result = (suma / 100) * posk;
    if(posk >= 21 && posk <= 40) result = ((((4 * posk) - 60) * suma) / 100);
    if(posk >= 41 && posk <= 60) result = ((((6 * posk) - 140) * suma) / 100);
    if(posk >= 61 && posk <= 70) result = ((((8 * posk) - 260) * suma) / 100);
    if(posk >= 71 && posk <= 80) result = ((((10 * posk) - 400) * suma) / 100);
    if(posk >= 81 && posk <= 90) result = ((((13 * posk) - 640) * suma) / 100);
    if(posk >= 91 && posk <= 100) result = ((((17 * posk) - 1000) * suma) / 100);
    
    
    data[13][0] = "Allianz - 700";
    data[13][1] = result; 
    }
    //=================================================================
    // Allianz - 500
    //=================================================================
    function allianz500(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * (25 + (posk - 25) * 3);
    if(posk >= 51 && posk <= 70) result = (suma / 100) * (100 + (posk - 50) * 5);
    if(posk >= 71 && posk <= 100) result = (suma / 100) * (200 + (posk - 70) * 10);
    data[12][0] = "Allianz - 500";
    data[12][1] = result; 
    }    
    //=================================================================
    // AXA - 400
    //=================================================================
    function AXA400(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * ((3 * posk) - 50);
    if(posk >= 51 && posk <= 75) result = (suma / 100) * ((5 * posk) - 150);
    if(posk >= 76 && posk <= 100) result = (suma / 100) * ((7 * posk) - 300); 
    data[11][0] = "AXA - 400";
    data[11][1] = result; 
    }
    //=================================================================
    // AXA - 700
    //=================================================================
    function AXA700(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * ((3 * posk) - 50);
    if(posk >= 51 && posk <= 75) result = (suma / 100) * ((5 * posk) - 150);
    if(posk >= 76 && posk <= 100) result = (suma / 100) * ((19 * posk) - 1200);
    data[10][0] = "AXA - 700";
    data[10][1] = result; 
    }
    //=================================================================
    // Basler - 650
    //=================================================================
    function basler650(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * (25 + (posk - 25) * 5);
    if(posk >= 51 && posk <= 90) result = (suma / 100) * (150 + (posk - 50) * 7);
    if(posk >= 91 && posk <= 100) result = (suma / 100) * (430 + (posk - 90) * 22);
    data[9][0] = "Basler - 650";
    data[9][1] = result; 
    }
    //=================================================================
    // Generali - 500
    //=================================================================
    function generali500(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 30) result = (suma / 100) * posk;
    if(posk >= 31 && posk <= 80) result = (suma / 100) * ((5 * posk) - 12);
    if(posk >= 81 && posk <= 100) result = (suma / 100) * ((11 * posk) - 600); 
    data[8][0] = "Generali - 500";
    data[8][1] = result; 
    }
    //=================================================================
    // Kooperativa - 350
    //=================================================================
    function kooperativa350(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * (25 + (posk - 25) * 3);
    if(posk >= 51 && posk <= 100) result = (suma / 100) * (100 + (posk - 50) * 5);
    data[7][0] = "Kooperativa - 350";
    data[7][1] = result; 
    }
    //=================================================================
    // Kooperativa - 500
    //=================================================================
    function kooperativa500(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 25) result = (suma / 100) * posk;
    if(posk >= 26 && posk <= 50) result = (suma / 100) * (25 + (posk - 25) * 5);
    if(posk >= 51 && posk <= 100) result = (suma / 100) * (150 + (posk - 50) * 7);
    data[6][0] = "Kooperativa - 500";
    data[6][1] = result; 
    }
    //=================================================================
    // Metlife - 500
    //=================================================================
    function metlife500(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 20) result = (suma / 100) * posk;
    if(posk >= 21 && posk <= 40) result = (suma / 100) * ((3 * posk) - 40);
    if(posk >= 41 && posk <= 60) result = (suma / 100) * ((5 * posk) - 120);
    if(posk >= 61 && posk <= 80) result = (suma / 100) * ((7 * posk) - 240);
    if(posk >= 81 && posk <= 100) result = (suma / 100) * ((9 * posk) - 400);
    data[5][0] = "Metlife - 500";
    data[5][1] = result; 
    }
    //=================================================================
    // Metlife - 1000
    //=================================================================
    function metlife1000(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 20) result = (suma / 100) * posk;
    if(posk >= 21 && posk <= 40) result = (suma / 100) * ((4 * posk) - 60);
    if(posk >= 41 && posk <= 60) result = (suma / 100) * ((10 * posk) - 300);
    if(posk >= 61 && posk <= 80) result = (suma / 100) * ((15 * posk) - 600);
    if(posk >= 81 && posk <= 100) result = (suma / 100) * ((20 * posk) - 1000);
    data[4][0] = "Metlife - 1000";
    data[4][1] = result; 
    }
    //=================================================================
    // NN - 500
    //=================================================================
    function nn800(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 20) result = (suma / 100) * posk;
    if(posk >= 21 && posk <= 40) result = (suma / 100) * (20 + (posk - 20) * 4);
    if(posk >= 41 && posk <= 50) result = (suma / 100) * (100 + (posk - 40) * 5);
    if(posk >= 51 && posk <= 60) result = (suma / 100) * (150 + (posk - 50) * 6);
    if(posk >= 61 && posk <= 70) result = (suma / 100) * (210 + (posk - 60) * 14);
    if(posk >= 71 && posk <= 100) result = (suma / 100) * (350 + (posk - 70) * 15);
    data[3][0] = "NN - 800";
    data[3][1] = result;
    }
    //=================================================================
    // Novis - 200
    //=================================================================
    function novis200(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 35) result = (suma / 100) * posk;
    if(posk >= 36 && posk <= 100) result = ((suma / 100) * posk) * 2;
    data[2][0] = "Novis - 200";
    data[2][1] = result; 
    }
    //=================================================================
    // Uniqa - 600
    //=================================================================
    function uniqa600(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 24) result = (suma / 100) * posk;
    if(posk >= 25 && posk <= 49) result = ((suma / 100) * posk) * 2;
    if(posk >= 50 && posk <= 74) result = ((suma / 100) * posk) * 3;
    if(posk >= 75 && posk <= 89) result = ((suma / 100) * posk) * 4;
    if(posk >= 90 && posk <= 94) result = ((suma / 100) * posk) * 5;
    if(posk >= 95 && posk <= 100) result = ((suma / 100) * posk) * 6;
    data[1][0] = "Uniqa - 600";
    data[1][1] = result; 
    }
    //=================================================================
    // Wustenrot - 400
    //=================================================================
    function wustenrot400(){
    var posk = poskodenie.value;
    var suma = poistnaSuma.value;
    var result = 0;  
    if(posk >= 0 && posk <= 35) result = (suma / 100) * posk;
    if(posk >= 36 && posk <= 50) result = ((suma / 100) * posk) * 2;
    if(posk >= 51 && posk <= 100) result = ((suma / 100) * posk) * 4;
    data[0][0] = "Wüstenrot - 400";
    data[0][1] = result; 
    }


    //=================================================================
    // ReloadTable
    //=================================================================
    function reloadTable(){
    sortByCol(data, 1);

    var rowsNum = pDataTable.rows.length;
    for (var i = 1; i < rowsNum; i++) { //Clear table
        document.getElementById("pDataTable").deleteRow(1);
    } 

    for(var i = 0; i < data.length; i++){  
        var newRow = pDataTable.insertRow(1),
        
        cell1 = newRow.insertCell(0),
        cell2 = newRow.insertCell(1);
        
        cell1.innerHTML = data[i][0];
        cell2.innerHTML = numberFormat(Number(data[i][1]).toFixed(0).toString(), " ");
    }
    }
    //=================================================================
    // Sort array by col
    //=================================================================
    function sortByCol(arr, colIndex){
        arr.sort(sortFunction)
        function sortFunction(a, b) {
            a = a[colIndex]
            b = b[colIndex]
            return (a === b) ? 0 : (a < b) ? -1 : 1
        }
    }
    //=================================================================
    // Upravi format cisla
    //=================================================================
    function numberFormat(_number, _sep) {
        if(_number == 0){
        return 0;
        } else{
        _number = typeof _number != "undefined" && _number > 0 ? _number : "";
        _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
        if(typeof _sep != "undefined" && _sep != " ") {
            _number = _number.replace(/\s/g, _sep);
        }
        return _number;    
        }
    }
</script>