<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload HTML</title>
</head>
<body style="margin: 1rem;">

    @if (isset($links))

    <div style="margin-bottom: 1rem;">
    @foreach ($links as $link)        
        <a href="{{$link[0]}}" target="_blank">
            {{$link[1]}} @if(isset($link[2])), {{$link[2]}} @endif @if(isset($link[3])), {{$link[3]}} @endif
        </a><br>
    @endforeach
    </div>

    @endif

    <form action="./sl-generate-links" method="post" enctype="multipart/form-data">
        @csrf
        <label for="htmlFile">Upload HTML File:</label>
        <input type="file" name="htmlFile" id="htmlFile" accept=".html">
        <button type="submit">Upload</button>
    </form>
</body>
</html>