<div v-if="modal.contactAdd" class="fullscreen p-1">

	<div>

		<button 
			class="btn btn-danger" 
			@click="modal.contactAdd = false" 
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success" 
			style="width: 75px;" 
			@click="addContact()">
			Pridať
		</button>
		
	</div>
	
	<label class="popis">Stav</label><br>
	<input class="input" type="text" list="listStav" v-model="contactNew.stav"><br>
	
	<div class="float-left">
						
		<label class="popis">Meno</label><br>
		<input class="input" type="text" v-model="contactNew.name_full">
	
		<label class="popis">Mobil</label><br>
		<input class="input" type="text" v-model="contactNew.phone">
	
		<label class="popis">Email</label><br>
		<input class="input" type="text" v-model="contactNew.email">	
	
		<label class="popis">Dátum akcie</label><br>
		<input class="input" type="date" v-model="contactNew.datum_akcie">
	
		<label class="popis">Zdroj kontaktu</label><br>
		<input class="input" type="text" v-model="contactSelect.zdroj_kontaktu" list="list_zdroj_kontaktu">

		<datalist id="list_zdroj_kontaktu">
			<option value="Lead">Lead</option>
			<option value="Odporúčanie od klienta">Odporúčanie od klienta</option>
			<option value="Rodina, známy, kamarát">Rodina, známy, kamarát</option>
		</datalist>
	
		<div v-if="contactNew.zdroj_kontaktu == 'Tip od klienta' || contactNew.zdroj_kontaktu == 'Odporúčanie od klienta'">
			<label class="popis">Kontakt poskytol</label><br>
			<input class="input" type="text" v-model="contactNew.odporucil" list="list_odporucil">
		</div>
	
		<datalist id="list_odporucil">
			<option 
				v-for="(item, index) in listOdporucil" :key="index"
				:value="item">@{{item}}
			</option>
		</datalist>
	
		<div v-if="contactNew.zdroj_kontaktu == 'Kampaňový kontakt'">
			<label class="popis">Názov kampane</label><br>
			<input class="input" type="text" v-model="contactNew.kampan_nazov" list="list_kampan_nazov">
		</div>
		
		<datalist id="list_kampan_nazov">
	
		</datalist>	
	
		<label class="popis">Ulica</label><br>
		<input class="input" type="text" v-model="contactNew.ulica">
	
		<label class="popis">PSC obec</label><br>
		<input class="input" type="text" v-model="contactNew.psc_obec" list="dataObec">
	
		<label class="popis">Príjmy</label><br>
		<input class="input" type="text" v-model="contactNew.prijmy">	
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Rodné číslo</label><br>
		<input 			
			class="input"
			:style="style.contactNewRC"
			type="text" 
			v-model="contactNew.rodne_cislo" 
			@input="validateRodneCisloContactNew()">
	
		<label class="popis">Dátum narodenia</label><br>
		<input class="input" type="date" v-model="contactNew.datum_narodenia">
	
		<label class="popis">Miesto narodenia</label><br>
		<input class="input" type="text" v-model="contactNew.miesto_narodenia">
	
		<label class="popis">Číslo OP</label><br>
		<input class="input" type="text" v-model="contactNew.cislo_op">
	
		<label class="popis">Platnosť dokladu od</label><br>
		<input 
			class="input" 
			type="date" 
			v-model="contactNew.platnost_op_od">
	
		<label class="popis">Platnosť dokladu do</label><br>
		<input class="input" type="date" v-model="contactNew.platnost_op_do">	
	
		<label class="popis">Doklad vydal (mesto)</label><br>
		<input class="input" type="text" v-model="contactNew.doklad_vydal">
	
		<label class="popis">Výdavky</label><br>
		<input class="input" type="text" v-model="contactNew.vydavky">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Vzdelanie</label><br>
		<input class="input" type="text" v-model="contactNew.vzdelanie" list="listVzdelanie">
	
		<label class="popis">Rodinný stav</label><br>
		<input class="input" type="text" v-model="contactNew.rodinny_stav" list="listRodStav">
		
		<label class="popis">Povolanie</label><br>
		<input class="input" type="text" v-model="contactNew.specifikacia">
	
		<label class="popis">Zamestnávateľ</label><br>
		<input class="input" type="text" v-model="contactNew.zamestnanie">
	
		<label class="popis">Typ prac. zmluvy</label><br>
		<input class="input" type="text" v-model="contactNew.typ_prac_zmluvy" list="listPracZmluva">
	
		<label class="popis">Dátum nástupu do práce</label><br>
		<input class="input" type="date" v-model="contactNew.zaciatok_zamestnania">
	
		<label class="popis">Počet nezaopatrených detí</label><br>
		<input class="input" type="text" v-model="contactNew.pocet_deti">
	
		<label class="popis">Bývanie</label><br>
		<input class="input" type="text" v-model="contactNew.byvanie" list="listTypyByvania">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Zdravotná poisťovňa</label><br>
		<input class="input" type="text" v-model="contactNew.zdravotna_poistovna" list="listZdravPoistovne">	
	
		<label class="popis">Obvodný lekár - Meno</label><br>
		<input class="input" type="text" v-model="contactNew.lekar_meno">
	
		<label class="popis">Obvodný lekár - Adresa</label><br>
		<input class="input" type="text" v-model="contactNew.lekar_adresa">
	
		<label class="popis">Obvodný lekár - Kontakt</label><br>
		<input class="input" type="text" v-model="contactNew.lekar_kontakt">
	
		<label class="popis">Výška (cm)</label><br>
		<input class="input" type="text" v-model="contactNew.vyska">
	
		<label class="popis">Váha (kg)</label><br>
		<input class="input" type="text" v-model="contactNew.hmotnost">
	
		<label class="popis">Bežný účet - banka</label><br>
		<input class="input" type="text" v-model="contactNew.bezny_ucet_banka" list="listBanky">
	
		<label class="popis">Číslo účtu</label><br>
		<input 
			class="input" 
			type="text" 
			v-model="contactNew.cislo_uctu"
			@input="validateIbanContactNew()"
			:style="style.contactNewCisloUctu">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Oslovenie</label><br>
		<select class="input" v-model="contactNew.oslovenie">
			<option value=""></option>
			<option value="pán">pán</option>
			<option value="pani">pani</option>
		</select>
	
		<label class="popis">Priezvisko</label><br>
		<input class="input" type="text" v-model="contactNew.name_last">
	
		<label class="popis">Krstné meno</label><br>
		<input class="input" type="text" v-model="contactNew.name_first" list="dataKrstneMena">
	
		<label class="popis">Interval servisných mailov</label><br>
		<input class="input" type="number" v-model="contactNew.func_servis_email" min="0">
	
		<label class="popis">Narodeniny a meniny</label><br>
		<select class="input" type="text" v-model="contactNew.func_narodeniny_meniny">
			<option value=""></option>
			<option value="áno">áno</option>
		</select>
	
		{{-- <label class="popis">Google Sync</label><br>
		<select class="input" type="text" v-model="contactNew.android_sync">
			<option value=""></option>
			<option value="áno">áno</option>
			<option value="KL - Uložený v telefóne">KL - Uložený v telefóne</option>
		</select> --}}
	
	</div>
	
	<div class="float-left-100">
		<label class="popis">Poznámky</label><br>
		<textarea 
			class="poznamka"
			v-model="contactNew.poznamka"
		></textarea>
	</div>
	
</div>