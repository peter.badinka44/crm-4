{{-- database.contacts --}}
<div v-if="modal.contactIndex" class="fullscreen p-1">

	<div class="d-flex">

		{{-- akcia --}}		
		<button 
			type="button"
			class="btn btn-primary dropdown-toggle" 
			data-toggle="dropdown" 
			aria-haspopup="true" 
			aria-expanded="false">
			Akcia
		</button>
		<div class="dropdown-menu">
			<a 
				class="dropdown-item cursor-pointer text-success" 
				@click="modal.contactAdd = !modal.contactAdd; contactNew = {}">
				<b>Pridať nový kontakt</b>
			</a>
			<div class="dropdown-divider"></div>			
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.contractsAll = !modal.contractsAll"
				v-if="user.role != 'dog'">
				Zobraziť zmluvy
			</a>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.dogsAll = !modal.dogsAll"
				v-if="user.role == 'dog'">
				Zobraziť všetkých psov
			</a>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.emailHistory = !modal.emailHistory"
				v-if="!offlineMode">
				Zobraziť históriu odoslaných e-malov
			</a>	
			{{-- <a class="dropdown-item cursor-pointer">Zobraziť odporúčania <b class="text-warning">(x)</b></a> --}}		
			{{-- <a class="dropdown-item cursor-pointer">Zobraziť štatistiku pridaných kontaktov <b class="text-warning">(x)</b></a> --}}
			<div class="dropdown-divider"></div>
			<a class="dropdown-item cursor-pointer  text-secondary" @click="copyEmails()">Skopírovať e-maily klientov</a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="checkDelete = !checkDelete; getContacts();">
				Filtrovať vymazané kontakty
				<b v-if="checkDelete" class="text-danger">(Active)</b>
			</a>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="checkKlient = !checkKlient; getContacts();">
				Filtrovať klientov
				<b v-if="checkKlient" class="text-danger">(Active)</b>
			</a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="changeAutofocus()">
				Autofocus
				<b v-if="databazaSearchAutofocus" class="text-danger">(Active)</b>
			</a>
		</div>

		{{-- filterDate --}}
		<select class="form-control ml-1" style="width: 275px;" 
			v-model="filterDate"
			@change="filterDateChange()">
			<option value="-">-</option>
			<option value="0">dátum akcie &lt;= dnes</option>
			<option value="1">dátum akcie &lt;= zajtra</option>
			<option value="7">dátum akcie &lt;= dnes + 7 dní</option>
			<option value="30">dátum akcie &lt;= dnes + 30 dní</option>
			<option value="narodeniny">najbližšie narodeniny</option>
			<option value="op_expire">končiaca platnosť OP</option>
			<option value="owner" v-if="!offlineMode">kontakty mimo moju databázu</option>
		</select>

		{{-- search --}}
		<input
			class="form-control ml-1" 
			style="width: 275px;"
			type="text" 
			v-model="search.contact"
			ref="refAutofocus"
			v-if="contacts.length > 0"
			placeholder="Vyhľadať">

		<div class="form-check d-flex ml-2 align-items-center" style="white-space: nowrap;">
			<div>
				<input type="checkbox" class="form-check-input" id="checkSearchName" v-model="search.name">
				<label class="form-check-label" for="checkSearchName" style="cursor: pointer;">vyhľadať meno</label>
			</div>
		</div>

		{{-- filterUser --}}
		<select class="form-control mr-1 ml-auto custom-checkbox"
			style="width: 275px;"
			v-model="filterUser"
			@change="filterUserChange()">
			{{-- <option value="-">-</option> --}}
			<option v-for="email in user.email_array"
				:value="email">@{{email}}</option>		
		</select>
		
		@include('menu')

	</div>	

	{{-- table --}}
	<div v-if="contacts.length > 0" class="mt-1" style="float: none;">	
		<table class="table-custom">
			<tr>
				<th><input type="checkbox" v-model="selectedContactsAll" @cllick="test()"></th>
				<th>Meno</th>
				<th>Dátum</th>
				<th>Stav</th>
				<th>Vek</th>
				<th>Okres</th>
				<th class="text-right">BEB</th>
				<th>Kontakt vytvoril</th>
			</tr>
			<tr class="cursor-pointer"
				v-for="row in rows" 
				:key="row.id">
				<td>
					<input type="checkbox" v-model="selectedContacts" :value="row.id" number @click="test()">
				</td>
				<td @click="showContact(row)">@{{row.name_full}}</td>
				<td @click="showContact(row)" v-if="filterDate == 'narodeniny'" nowrap>					
					@{{getDatumNarodenia(row.datum_narodenia)}}
				</td>
				<td @click="showContact(row)" v-if="filterDate == 'op_expire'">
					@{{dateToDMY(row.platnost_op_do)}}
				</td>
				<td @click="showContact(row)" v-if="filterDate == '-' 
					|| filterDate == '0'
					|| filterDate == '1'
					|| filterDate == '7'
					|| filterDate == '30'
					|| filterDate == 'owner'">
					@{{dateToDMY(row.datum_akcie)}}
				</td>
				<td @click="showContact(row)">@{{row.stav}}</td>
				<td @click="showContact(row)" class="text-center">@{{getAge(row.datum_narodenia)}}</td>
				<td @click="showContact(row)">@{{row.psc_obec}}</td>
				<td class="text-right"
					@click="showContact(row)">
					@{{row.beb_zmluvy}}
				</td>
				<td @click="showContact(row)" :title="
					'Kontakt bol vytvorený: ' + dateToDMY(row.date_reg) + ' (' + row.id_user_reg +  ')\n'
					+ 'Súčasný účet: ' + row.id_user
					">
					@{{row.id_user_reg}}
				</td>
			</tr>
		</table>
	</div>
	<div v-if="contacts.length > 0" class="popText mx-1">
		Počet riadkov: @{{rows.length}}
	</div>

	<div v-if="selectedContacts.length > 0" class="massChange border">
		<h6>
			<input type="checkbox" class="" id="exampleCheck1" v-model="massChange.checkdDatumAkcie">
			<label class="" for="exampleCheck1">Zmeniť dátum akcie</label>
			<div v-if="massChange.checkdDatumAkcie">				
				<input type="date" class="form-control" v-model="massChange.datumAkcie">
			</div>
		</h6>
		
		<h6>
			<input type="checkbox" class="" id="exampleCheck2" v-model="massChange.checkStav">
			<label class="" for="exampleCheck2">Doplniť do stavu</label>
			<div v-if="massChange.checkStav">				
				<input type="text" class="form-control" v-model="massChange.stav">
			</div>
		</h6>
		
		<h6>
			<input type="checkbox" class="" id="exampleCheck3" v-model="massChange.checkEmail">
			<label class="" for="exampleCheck3">Priradiť k inému účtu</label>	

			<div v-if="massChange.checkEmail">
				<select type="text" class="form-control" v-model="massChange.email">
					<option v-for="email in user.email_array"
						:value="email">@{{email}}</option>
				</select>
			</div>
		</h6>
		
		<h6>
			<input type="checkbox" class="" id="exampleCheck4" v-model="massChange.checkIntervalServis">
			<label class="" for="exampleCheck4">Interval servisných e-mailov</label>
			<div v-if="massChange.checkIntervalServis">
				<input type="number" class="form-control" v-model="massChange.intervalServis">
			</div>
		</h6>
		
		<h6>
			<input type="checkbox" class="" id="exampleCheck5" v-model="massChange.checkDelete">
			<label class="" for="exampleCheck5">Vymazať kontakt</label>
		</h6>

		<h6>
			<input type="checkbox" class="" id="exampleCheck6" v-model="massChange.checkCopyEmails">
			<label class="" for="exampleCheck6">Skopírovať e-maily</label>
		</h6>

		<button class="btn btn-primary" @click="handleMassChange()">
			Uložiť (@{{selectedContacts.length}})
		</button>
	</div>

</div>