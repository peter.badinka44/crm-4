<div v-if="modal.customFields" class="fullscreen p-1">

    <button
        class="btn btn-danger" 
        @click="modal.customFields = false;" 
        style="width: 75px;">
        Späť
    </button>

    <button 
        class="btn btn-success" 
        style="width: 75px;" 
        @click="saveCustomFields()">
        Uložiť
    </button>

    <table class="my-1">
        <tr style="background-color: rgb(239 239 239 / 21%)">
            <th></th>
            <th>Názov</th>            
            <th>Typ</th>
            <th>Šírka(%)</th>
            <th style="min-width: 300px;">
                Možnosti výberu
            </th>
            <th></th>
        </tr>
        <tr v-for="field in customFields" :key="field.id">
            <td>
                <img 
                    src="{{ asset('images/up-arrow.png') }}" alt="settings"
                    style="width: 20px; height: 20px; cursor: pointer;"
                    v-if="field.order > 1"
                    @click="changeOrder(field.order)"
                >
            </td>
            <td><input type="text" v-model="field.name" class="form form-control w-100"></td>
            <td>
                <select v-model="field.type" class="form form-control w-100">
                    <option value="text">text</option>
                    <option value="select">rozbaľovací zoznam</option>
                    <option value="date">dátum</option>
                </select>
            </td>
            <td>
                <input type="number" 
                    v-model="field.width"
                    class="form form-control w-100">
            </td>
            <td>
                <textarea 
                    rows="1" class="form form-control w-100"
                    v-model="field.options" 
                    v-if="field.type !== 'date'">
                </textarea>
            </td>
            <td>
                <img 
                    src="{{ asset('images/up-arrow.png') }}" alt="settings"
                    style="width: 20px; height: 20px; cursor: pointer; transform: rotate(180deg);"
                    v-if="field.order < customFields.length"
                    @click="changeOrder(field.order, false)"
                >
            </td>
        </tr>
        <tr>
            <td></td>        
            <td>
                <input type="text" 
                    v-model="newCustomField.name" 
                    class="form form-control w-100"
                    style="background-color: rgb(239 239 239 / 21%)">
            </td>            
            <td>
                <select 
                    v-model="newCustomField.type" 
                    class="form form-control w-100" 
                    style="background-color: rgb(239 239 239 / 21%)"
                    value="text">
                        <option value="text">text</option>
                        <option value="select">rozbaľovací zoznam</option>
                        <option value="date">dátum</option>
                </select>
            </td>
            <td>
                <input type="number" 
                    v-model="newCustomField.width"
                    class="form form-control w-100"
                    style="background-color: rgb(239 239 239 / 21%)">
            </td>
            <td>
                <textarea 
                    rows="1" class="form form-control w-100"
                    v-if="newCustomField.type !== 'date'"
                    v-model="newCustomField.options"
                    style="background-color: rgb(239 239 239 / 21%)">
                </textarea>
            </td>                
            <td>
                <button 
                    class="btn btn-primary" 
                    style="width: 75px;" 
                    @click="addCustomField()">
                    Pridať
                </button>
            </td>
        </tr>
    </table>
  
</div>