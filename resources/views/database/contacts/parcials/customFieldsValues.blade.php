<div class="float-left-100">
    <hr>
    <h4 @click="modal.customFields = true;" style="cursor: pointer;">
        Dynamické polia
        <img src="{{ asset('images/settings.png') }}" alt="settings"
            style="width: 20px; height: 20px;">
    </h4>
    <div class="custom-fields d-flex align-content-space-between flex-wrap">
        <div class="custom-field" v-for="field in customFields" :key="field.id"
            v-bind:style="{ width: field.width + '%' }">

            <div class="custom-field-text" v-if="field.type === 'text'">
                <label class="popis cursor-pointer">@{{field.name}}</label><br>
                <input class="input" v-model="field.value" :list="`datalist-${field.id}`">
                <datalist :id="`datalist-${field.id}`">
                    <option v-for="option in getOptionsFromString(field.options)" :value="option">
                        @{{option}}
                    </option>
                </datalist>
            </div>				

            <div class="custom-field-select" v-if="field.type === 'select'">
                <label class="popis cursor-pointer">@{{field.name}}</label><br>
                <select class="input" v-model="field.value">
                    <option v-for="option in getOptionsFromString(field.options)" :value="option">
                        @{{option}}
                    </option>
                </select>
            </div>

            <div class="custom-field-text" v-if="field.type === 'date'">
                <label class="popis cursor-pointer">@{{field.name}}</label><br>
                <input class="input" type="date" v-model="field.value">
            </div>

        </div>
    </div>		
</div>