<div class="row ml-0">

	{{-- add --}}
	<button 
	class="btn btn-success" 
	style="width: 50px;" 
	@click="requestNewShow()">
	+
	</button>

	{{-- search --}}
	<input class="form-control ml-1" 
	type="text"
	v-model="search.request"
	v-if="contactSelectRequests.length > 0" 
	style="width: 275px;"
	placeholder="Vyhľadať">

</div>

{{-- table --}}
<table v-if="contactSelectRequests.length > 0" class="table-custom mt-1">
	<tr>
	<th></th>
	<th>Meno</th>
	<th>Typ žiadosti</th>
	<th>Číslo zmluvy</th>
	<th>Inštitúcia</th>
	<th>Program</th>
	</tr>
	<tr v-for="row in rowsContactSelectRequests"
	:key="row.id" 
	class="cursor-pointer"
	@click="showRequest(row)">
	<td>@{{dateToDMY(row.date_reg)}}</td>
	<td>@{{row.name_full}}</td>
	<td>@{{row.typ_ziadosti}}</td>
	<td>@{{row.cislo_zmluvy}}</td>
	<td>@{{row.institucia}}</td>
	<td>@{{row.program}}</td>
	</tr>
</table>
