<div v-if="modal.contactShow" class="fullscreen p-1">

	<div>
		<button 
			class="btn btn-danger" 
			@click="backContact()"
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success" 
			style="width: 75px;" 
			@click="updateContact()">
			Uložiť
		</button>

		<button v-if="contactSelect.check_delete == 'false' || contactSelect.check_delete == false"
			class="btn btn-danger float-right"
			style="width: 100px;" 
			@click="destroyContact()">Vymazať
		</button>

		<button v-if="contactSelect.check_delete == 'ok'"
			class="btn btn-success float-right"
			style="width: 100px;"
			@click="restoreContact()">Obnoviť
		</button>
		
		{{-- akcia --}}
		<button 
			type="button" 
			class="btn btn-info dropdown-toggle ml-0" 
			data-toggle="dropdown"
			aria-haspopup="true"
			aria-expanded="false"
			v-if="user.role != 'dog'">
			Akcia
		</button>
		<div class="dropdown-menu">			
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.contracts = true"
				title="Ctrl+;">
				Zobraziť zmluvy
			</a>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.requests = true"
				title="Ctrl+;">
				Zobraziť žiadosti
			</a>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="modal.subContactIndex = true"
				title="Ctrl+;">
				Zobraziť osoby
			</a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item cursor-pointer text-secondary"
				@click="userSwap()">
				Priradiť k inému účtu
			</a>
			{{-- <div class="dropdown-divider"></div>
			<a class="dropdown-item cursor-pointer">Zobraziť históriu zmien <b class="text-warning">(x)</b></a>
			<a class="dropdown-item cursor-pointer">Zobraziť históriu e-malov <b class="text-warning">(x)</b></a> --}}
			<div class="dropdown-divider"></div>
			<!-- <a v-if="contactSelect.zapis_link == 'false'"
				class="dropdown-item cursor-pointer text-secondary"
				{{-- @click="createOpenZapis()" --}}
				>
				Vytvoriť zápis zo stretnutia
			</a>
			<a v-if="contactSelect.zapis_link != 'false'"
				class="dropdown-item cursor-pointer text-success"
				@click="createOpenZapis()">
				Otvoriť zápis zo stretnutia
			</a>
			<a v-if="contactSelect.financny_plan_link == 'false'"
				class="dropdown-item cursor-pointer text-secondary"
				{{-- @click="createOpenFinPlan()" --}}
				>
				Vytvoriť finančný plán
			</a>
			<a v-if="contactSelect.financny_plan_link != 'false'"
				class="dropdown-item cursor-pointer text-success"
				@click="createOpenFinPlan()">
				Otvoriť finančný plán
			</a>
			<div class="dropdown-divider"></div> -->
			<a class="dropdown-item cursor-pointer" 
				@click="markKlient()"
				@click="modal.contactAdd = !modal.contactAdd">
				<b class="text-info">
					Klient
					<label v-if="contactSelect.check_klient == 'true'" class="text-danger">(Active)</label>					
				</b>				
			</a>
		
		</div>

		<span v-if="user.role == 'dog'">
			<button 
				class="btn btn-info" 
				@click="showContactDogs()"
				style="width: 75px;">
				Psy
			</button>
	
			<button 
				class="btn btn-warning" 
				@click="openExternyPristup()"
				style="width: 150px;">
				Externý prístup
			</button>
		</span>

	</div>
	
	<label class="popis">Stav</label><br>
	<input class="input" type="text" list="listStav" v-model="contactSelect.stav"><br>
	
	<div class="float-left">
						
		<label class="popis cursor-pointer" @click="copyToClipboard('{name_full}')">Meno</label><br>
		<input class="input" type="text" v-model="contactSelect.name_full">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{phone}')">Mobil</label><br>
		<input class="input" type="text" v-model="contactSelect.phone">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{email}')">Email</label><br>
		<input class="input" type="text" v-model="contactSelect.email">	
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{datum_akcie}')">Dátum akcie</label><br>
		<input class="input" type="date" v-model="contactSelect.datum_akcie">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{zdroj_kontaktu}')">Zdroj kontaktu</label><br>
		<input class="input" type="text" v-model="contactSelect.zdroj_kontaktu" list="list_zdroj_kontaktu">

		<datalist id="list_zdroj_kontaktu">
			<option value="Lead">Lead</option>
			<option value="Odporúčanie od klienta">Odporúčanie od klienta</option>
			<option value="Rodina, známy, kamarát">Rodina, známy, kamarát</option>
		</datalist>
	
		<div v-if="contactSelect.zdroj_kontaktu == 'Tip od klienta' || contactSelect.zdroj_kontaktu == 'Odporúčanie od klienta'">
			<label class="popis cursor-pointer" @click="copyToClipboard('{odporucil}')">Kontakt poskytol</label><br>
			<input class="input" type="text" v-model="contactSelect.odporucil" list="list_odporucil">
		</div>
	
		<datalist id="list_odporucil">
			<option 
				v-for="(item, index) in listOdporucil" :key="index"
				:value="item">@{{item}}
			</option>
		</datalist>
	
		<div v-if="contactSelect.zdroj_kontaktu == 'Kampaňový kontakt'">
			<label class="popis cursor-pointer" @click="copyToClipboard('{kampan_nazov}')">Názov kampane</label><br>
			<input class="input" type="text" v-model="contactSelect.kampan_nazov" list="list_kampan_nazov">
		</div>
		
		<datalist id="list_kampan_nazov">
			<option
				v-for="(item, index) in listKampan" :key="index"
				:value="item">@{{item}}</option>
		</datalist>	
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{ulica}')">Ulica</label><br>
		<input class="input" type="text" v-model="contactSelect.ulica">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{psc_obec}')">PSC obec</label><br>
		<input class="input" type="text" v-model="contactSelect.psc_obec" list="dataObec">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{prijmy}')">Príjmy</label><br>
		<input class="input" type="text" v-model="contactSelect.prijmy">	
	
	</div>
	
	<div class="float-left">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{rodne_cislo}')">Rodné číslo</label><br>
		<input 
			class="input"
			:style="style.contactSelectRC"
			type="text" 
			v-model="contactSelect.rodne_cislo" 
			@input="validateRodneCisloContactSelect()">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{datum_narodenia}')">
			Dátum narodenia <span v-if="contactSelect.datum_narodenia !== '0000-00-00'">(<b>@{{getAge(contactSelect.datum_narodenia)}}</b>)</span>
		</label><br>
		<input class="input" type="date" v-model="contactSelect.datum_narodenia">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{miesto_narodenia}')">Miesto narodenia</label><br>
		<input class="input" type="text" v-model="contactSelect.miesto_narodenia">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{cislo_op}')">Číslo OP</label><br>
		<input class="input" type="text" v-model="contactSelect.cislo_op">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{platnost_op_od}')">Platnosť dokladu od</label><br>
		<input 
			class="input" 
			type="date" 
			v-model="contactSelect.platnost_op_od"
			@change="contactSelect.platnost_op_do = datePlus10YearsYMD(contactSelect.platnost_op_od)">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{platnost_op_do}')">Platnosť dokladu do</label><br>
		<input class="input" type="date" v-model="contactSelect.platnost_op_do">	
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{doklad_vydal}')">Doklad vydal (mesto)</label><br>
		<input class="input" type="text" v-model="contactSelect.doklad_vydal">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{vydavky}')">Výdavky</label><br>
		<input class="input" type="text" v-model="contactSelect.vydavky">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{vzdelanie}')">Vzdelanie</label><br>
		<input class="input" type="text" v-model="contactSelect.vzdelanie" list="listVzdelanie">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{rodinny_stav}')">Rodinný stav</label><br>
		<input class="input" type="text" v-model="contactSelect.rodinny_stav" list="listRodStav">
		
		<label class="popis cursor-pointer" @click="copyToClipboard('{specifikacia}')">Povolanie</label><br>
		<input class="input" type="text" v-model="contactSelect.specifikacia">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{zamestnanie}')">Zamestnávateľ</label><br>
		<input class="input" type="text" v-model="contactSelect.zamestnanie">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{typ_prac_zmluvy}')">Typ prac. zmluvy</label><br>
		<input class="input" type="text" v-model="contactSelect.typ_prac_zmluvy" list="listPracZmluva">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{zaciatok_zamestnania}')">Dátum nástupu do práce</label><br>
		<input class="input" type="date" v-model="contactSelect.zaciatok_zamestnania">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{pocet_deti}')">Počet nezaopatrených detí</label><br>
		<input class="input" type="text" v-model="contactSelect.pocet_deti">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{byvanie}')">Bývanie</label><br>
		<input class="input" type="text" v-model="contactSelect.byvanie" list="listTypyByvania">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{zdravotna_poistovna}')">Zdravotná poisťovňa</label><br>
		<input class="input" type="text" v-model="contactSelect.zdravotna_poistovna" list="listZdravPoistovne">	
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{lekar_meno}')">Obvodný lekár - Meno</label><br>
		<input class="input" type="text" v-model="contactSelect.lekar_meno">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{lekar_adresa}')">Obvodný lekár - Adresa</label><br>
		<input class="input" type="text" v-model="contactSelect.lekar_adresa">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{lekar_kontakt}')">Obvodný lekár - Kontakt</label><br>
		<input class="input" type="text" v-model="contactSelect.lekar_kontakt">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{vyska}')">Výška (cm)</label><br>
		<input class="input" type="text" v-model="contactSelect.vyska">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{hmotnost}')">Váha (kg)</label><br>
		<input class="input" type="text" v-model="contactSelect.hmotnost">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{bezny_ucet_banka}')">Bežný účet - banka</label><br>
		<input class="input" type="text" v-model="contactSelect.bezny_ucet_banka" list="listBanky">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{cislo_uctu}')">Číslo účtu</label><br>
		<input 
			class="input" 
			type="text" 
			v-model="contactSelect.cislo_uctu"
			@input="validateIbanContactSelect()"
			:style="style.contactSelectCisloUctu">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{oslovenie}')">Oslovenie</label><br>
		<select class="input" v-model="contactSelect.oslovenie">
			<option value=""></option>
			<option value="pán">pán</option>
			<option value="pani">pani</option>
		</select>
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{name_last}')">Priezvisko</label><br>
		<input class="input" type="text" v-model="contactSelect.name_last">
	
		<label class="popis cursor-pointer" @click="copyToClipboard('{name_first}')">Krstné meno</label><br>
		<input class="input" type="text" v-model="contactSelect.name_first" list="dataKrstneMena">

		<div v-if="!offlineMode">
			<label class="popis cursor-pointer" @click="copyToClipboard('{func_servis_email}')">Interval servisných mailov</label><br>
			<input class="input" type="number" v-model="contactSelect.func_servis_email" min="0">
		
			<label class="popis cursor-pointer" @click="copyToClipboard('{func_narodeniny_meniny}')">Narodeniny a meniny</label><br>
			<select class="input" type="text" v-model="contactSelect.func_narodeniny_meniny">
				<option value=""></option>
				<option value="áno">áno</option>
			</select>
		</div>	
		
		<label class="popis cursor-pointer" @click="copyToClipboard('{zlozka_klienta}')">Zložka klienta</label><br>
		<select class="input" type="text" v-model="contactSelect.zlozka_klienta">
			<option value=""></option>
			<option value="u mňa/v trezore">u mňa/v trezore</option>
			<option value="u klienta">u klienta</option>
		</select>
	
		{{-- <label class="popis cursor-pointer" @click="copyToClipboard('{android_sync}')">Google Sync</label><br>
		<select class="input" type="text" v-model="contactSelect.android_sync">
			<option value=""></option>
			<option value="áno">áno</option>
			<option value="KL - Uložený v telefóne">KL - Uložený v telefóne</option>
		</select> --}}
	
	</div>

	@include('database.contacts.parcials.customFieldsValues')

	@include('database.contacts.parcials.customFieldsSettings')

	<div class="float-left-100">
		<hr>
		<h4>Zmluvy</h4>
		@include('database.contacts.parcials.contracrs')
	</div>

	<div class="float-left-100">
		<hr>
		<h4>Žiadosti</h4>
		@include('database.contacts.parcials.requests')
	</div>

	<div class="float-left-100">
		<hr>
		<h4>Osoby</h4>
		@include('database.contacts.parcials.subContacts')
	</div>
	
	<div class="float-left-100">
		<label class="popis cursor-pointer" @click="copyToClipboard('{poznamka}')">Poznámky</label><br>
		<textarea 
			class="poznamka"
			v-model="contactSelect.poznamka"
		></textarea>
	</div>
	
</div>