<div v-if="modal.contractsAll" class="fullscreen p-1">

	<div class="row ml-0">
  
	  {{-- back --}}
	  <button 
		class="btn btn-danger mr-1" 
		@click="modal.contractsAll = false" 
		style="width: 75px;">
		Späť
	  </button> 
  
	  {{-- search --}}
	  <input class="form-control" 
		type="text" 
		v-model="search.contractsAll"
		v-if="contracts.length > 0"
		style="width: 275px;"	
		placeholder="Vyhľadať">
		
		
		{{-- allContractsFilter --}}
		<select class="form-control ml-1" style="width: 275px;" 
			v-model="allContractsFilter"
			@change="filterAllContracts()">
			<option value="-">-</option>
			<option value="0">výročie zmluvy</option>
		</select>
	</div>
  
	{{-- table --}}
	<table v-if="contracts.length > 0" class="table-custom mt-1">
	  <tr>
		<th>Meno</th>
		<th>Dátum</th>
		<th class="text-right">Plán</th>
		<th class="text-right">Skut.</th>
		<th>Produkt</th>
		<th>Spoločnosť</th>
		<th>Číslo zmluvy</th>
		<th>Stav</th>
		<th>Servis</th>
	  </tr>
	  <tr v-for="row in rowsContractsAll"
		:key="row.id"
		class="cursor-pointer"
		@click="contractSelect = row; modal.contractsAllShow = true;">
		<td>@{{row.name_full}}</td>
		<td v-if="allContractsFilter == '-'">
			@{{dateToDMY(row.date_start)}}
		</td>
		<td nowrap v-if="allContractsFilter == '0'">
			@{{dateToDMY(row.date_start)}} [@{{row.ltt}}]
		</td>
		<td class="text-right">@{{row.beb_plan}}</td>
		<td class="text-right">@{{row.beb_skut}}</td>
		<td>@{{row.produkt}}</td>
		<td>@{{row.spolocnost}}</td>
		<td>@{{row.cislo_zmluvy}}</td>
		<td>@{{row.stav}}</td>
		<td>@{{row.stav_servis}}</td>
	  </tr>
	</table>
	<div v-if="rowsContractsAll.length > 0" class="popText mx-1">
		Počet riadkov: @{{rowsContractsAll.length}}
	</div>
  
  </div>