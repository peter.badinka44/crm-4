<div v-if="modal.contracts" class="fullscreen p-1">

  <div class="row ml-0">

    {{-- back --}}
    <button 
      class="btn btn-danger mr-1" 
      @click="modal.contracts = false" 
      style="width: 75px;">
      Späť
    </button>

    {{-- add --}}
    <button 
      class="btn btn-success"
      style="width: 75px;" 
      @click="modal.contractAdd = true; contractNew = {}; contractNew.name_full = contactSelect.name_full"> 
      Pridať
    </button>

    {{-- search --}}
    <input class="form-control ml-1" 
      type="text" 
      v-model="search.contract"
      v-if="contactSelectContracts.length > 0" 
      style="width: 275px;"      
      placeholder="Vyhľadať">

  </div>

  {{-- table --}}
  <table v-if="contactSelectContracts.length > 0" class="table-custom mt-1">
    <tr>
      <th>Meno</th>
      <th>Dátum</th>
      <th class="text-right">Plán</th>
      <th class="text-right">Skut.</th>
      <th>Produkt</th>
      <th>Spoločnosť</th>
      <th>Číslo zmluvy</th>
      <th>Stav</th>
      <th>Servis</th>
    </tr>
    <tr v-for="row in rowsContactSelectContracts"
      :key="row.id"       
      class="cursor-pointer"
      @click="showContract(row)">
      <td>@{{row.name_full}}</td>
      <td>@{{dateToDMY(row.date_start)}}</td>
      <td class="text-right">@{{row.beb_plan}}</td>
      <td class="text-right">@{{row.beb_skut}}</td>
      <td :class="{ 'color-moja': row.stav == 'v mojej správe', 'color-cudzia': row.stav == 'v cudzej správe' }">
        @{{row.produkt}}
      </td>
      <td :class="{ 'color-moja': row.stav == 'v mojej správe', 'color-cudzia': row.stav == 'v cudzej správe' }">
        @{{row.spolocnost}}
      </td>
      <td :class="{ 'color-moja': row.stav == 'v mojej správe', 'color-cudzia': row.stav == 'v cudzej správe' }">
        @{{row.cislo_zmluvy}}
      </td>
      <td :class="{ 'color-moja': row.stav == 'v mojej správe', 'color-cudzia': row.stav == 'v cudzej správe' }">   
        @{{row.stav}}
      </td>
      <td>@{{row.stav_servis}}</td>
    </tr>
    <tr v-if="contactSelectContracts.length > 1">
      <th>Meno</th>
      <th>Dátum</th>
      <th class="text-right">Plán</th>
      <th class="text-right">Skut.</th>
      <th>Produkt</th>
      <th>Spoločnosť</th>
      <th>Číslo zmluvy</th>
      <th>Stav</th>
      <th>Servis</th>
    </tr>
    <tr v-if="contactSelectContracts.length > 1">
      <td></td>
      <td></td>
      <td class="text-right">@{{ oContract.sumBebPlan }}</td>
      <td class="text-right">@{{ oContract.sumBebSkut }}</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>

</div>