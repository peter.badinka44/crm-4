<div v-if="modal.dogAdd" class="fullscreen p-1">

  <button
    class="btn btn-danger" 
    @click="modal.dogAdd = false;"
    style="width: 75px;">
    Späť
  </button>
  
  <button 
    class="btn btn-success"
    style="width: 75px;"
    @click="addDog()">
    Pridať
  </button>
  
  <form>
    
    <div class="float-left-zmluva">
    
      <label class="popis">Meno</label><br>
      <input class="input" type="text" v-model="dogs.new.meno"><br>

      <label class="popis">Dátum narodenia</label><br>
      <input class="input" type="date" v-model="dogs.new.datum_narodenia"><br>       
    
    </div>

    <div class="float-left-zmluva"> 
      
      <label class="popis">Otec</label><br>      
      <input class="input" type="text" v-model="dogs.new.meno_otec" list="dataOtec"><br> 
      
      <datalist id="dataOtec">
        <option value="Sky Pride Baikal"></option>
        <option value="Nadir Des Jardins D´ Epona"></option>
        <option value="Iron Diamont in the heart"></option>
      </datalist>

      <label class="popis">Číslo čípu</label><br>
      <input class="input" type="text" v-model="dogs.new.cislo_cipu" list="datalistCisoCipu"><br>

      <datalist id="datalistCisoCipu">
        <option           
          v-for="(opt, index) in dogs.cislaCipov"
          :key="index"
          :value="opt">
          @{{ opt }}
        </option>
      </datalist>

    </div>
    
    <div class="float-left-zmluva"> 

      <label class="popis">Matka</label><br>
      <input class="input" type="text" v-model="dogs.new.meno_matka" list="dataMatka"><br>

      <datalist id="dataMatka">
        <option value="Juliette Diamont in the heart"></option>
        <option value="Kiara Diamont in the heart"></option>
        <option value="Nira Des Jardins D´ Epona"></option>
        <option value="Nira Des Jardins D´ Epona"></option>
        <option value="Orsay Des Jardins D´ Epona"></option>
        <option value="Reginn Diamont in the heart"></option>
        <option value="Michelle Sneh Tatier"></option> 
        <option value="Comet Halley Angelic Soul"></option>
      </datalist>

      <label class="popis">Vrh</label><br>
      <input class="input" type="text" v-model="dogs.new.vrh"><br> 
    
    </div>

    <div class="float-left-zmluva"> 

      <label class="popis">Cena</label><br>
      <input class="input" type="number" step="100" v-model="dogs.new.cena"><br>

      <label class="popis">Rezervačný poplatok</label><br>
      <input class="input" type="text" v-model="dogs.new.rezervacny_poplatok"><br>
    
    </div>
    
    <div class="float-left-100">
      <label class="popis">Poznámky</label><br>
      <textarea class="poznamka" v-model="dogs.new.poznamky" rows="3"></textarea>
    </div>
  
  </form>
  
</div>