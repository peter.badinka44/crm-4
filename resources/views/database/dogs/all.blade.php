<div v-if="modal.dogsAll" class="fullscreen p-1">

	<div class="row ml-0">
  
	  {{-- back --}}
	  <button 
		class="btn btn-danger mr-1" 
		@click="modal.dogsAll = false;"
		style="width: 75px;">
		Späť
	  </button> 
  
	  {{-- search --}}
	  <input class="form-control" 
		type="text" 
		v-model="search.dogsAll"
		v-if="dogs.data.length > 0"
		style="width: 275px;"	
		placeholder="Vyhľadať">  
	</div>
  
	{{-- table --}}
	<table v-if="dogs.data.length > 0" class="table-custom mt-1">
		<tr>
			<th>Majiteľ</th>
			<th>Meno</th>
			<th>Dátum nar.</th>
			<th>Číslo čipu</th>
			<th>Vrh</th>
			<th>Otec</th>
			<th>Matka</th>
			<th>Cena</th>
			<th>Rezerv. poplatok</th>
		</tr>
		<tr v-for="row in rowsDogsAll"
			:key="row.id"
			class="cursor-pointer"
			@click="showDog(row)">
			<td>@{{getDogOwner(row)}}</td>
			<td>@{{row.meno}}</td>
			<td>@{{dateToDMY(row.datum_narodenia)}}</td>
			<td>@{{row.cislo_cipu}}</td>
			<td>@{{row.vrh}}</td>
			<td>@{{row.meno_otec}}</td>
			<td>@{{row.meno_matka}}</td>
			<td>@{{row.cena}}</td>
			<td>@{{row.rezervacny_poplatok}}</td>
		</tr>
	</table>  
</div>
