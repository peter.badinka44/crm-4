<div v-if="modal.dogsAllShow" class="fullscreen p-1">

	<button
	  class="btn btn-danger" 
	  @click="modal.contractsAllShow = false;"
	  style="width: 75px;">
	  Späť
	</button>
	
	<button 
	  class="btn btn-info"
	  style="width: 150px;"
	  @click="showContactById()">
	  Otvoriť klienta
	</button>

	<form autocomplete="off">
	
	<div class="float-left-zmluva">
	
		<label class="popis text-color-red">Kategória produktu</label></br>
		<select class="input border-red" v-model="contractSelect.kategoria">
			<option value="-">-</option>
			<option value="Poistenie - neživot a bežný účet">Poistenie - neživot a bežný účet</option>
			<option value="Poistenie - život">Poistenie - život</option>
			<option value="Hypotéky a úvery">Hypotéky a úvery</option>
			<option value="Dôchodok - SDS a DDS">Dôchodok - SDS a DDS</option>
			<option value="Investície">Investície</option>
		</select>
	
		<label class="popis text-color-red">Dátum podpisu</label></br>
		<input class="input border-red" type="date" v-model="contractSelect.date_podpis">
	
		<label class="popis text-color-red">BEB - Plán</label></br>
		<input class="input border-red" type="number" value="0" min="0" v-model="contractSelect.beb_plan">
	
		<label class="popis text-color-red">BEB - Skutočnosť</label></br>
		<input class="input border-red" type="number" value="0" min="0" v-model="contractSelect.beb_skut">
	
	</div>
	
	<div class="float-left-zmluva">
	
		<label class="popis text-color-green">Poistník/Vlastník zmluvy</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.name_full">
	
		<label class="popis text-color-green">Výročný deň / dátum začiatku</label></br>
		<input class="input border-green" type="date" v-model="contractSelect.date_start">
	
		<label class="popis text-color-green">Spoločnosť</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.spolocnost" list="list_institucie">

		<datalist id="list_institucie">
			<option v-for="company in companies" :value="company.address_1">@{{company.address_1}}</option>
		</datalist>
	
		<label class="popis text-color-green">Produkt</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.produkt" list="datalist_produkty">
	
		<datalist id="datalist_produkty">
	
		</datalist>    
	
	</div>
	
	<div class="float-left-zmluva">
	
		<label class="popis text-color-green">Číslo zmluvy</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.cislo_zmluvy">
	
		<label class="popis text-color-green">Stav</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.stav" list="dataStav">
	
		<label class="popis text-color-green">Platba / splátka</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.platba">
	
		<label class="popis text-color-green">Interval</label></br>
		<input class="input border-green" type="text" v-model="contractSelect.platba_interval" list="dataPlatba">
	
	</div>
	
	<div class="float-left-zmluva">
	
	  <label class="popis text-color-blue">Servis</label></br>
	  <select class="input border-blue"v-model="contractSelect.stav_servis">
		  <option value=""></option>
		  <option value="vyriešené">vyriešené</option>
		  <option value="riešiť v budúcnosti">riešiť v budúcnosti</option>
		  <option value="neriešiť">neriešiť</option>
	  </select> 
	
	  <label class="popis text-color-blue">Upozornenie - maklér</label></br>
	  <select class="input border-blue" v-model="contractSelect.upozornenie_makler">	
		  <option value=""></option>
		  <option value="áno">áno</option>
	  </select>
	
	  <label class="popis text-color-blue">Upozornenie - klient</label></br>
	  <select class="input border-blue" v-model="contractSelect.upozornenie_klient">
		  <option value=""></option>
		  <option value="áno">áno</option>
	  </select>
	
	  <label class="popis text-color-blue">Počet týždňov</label></br>
	  <input class="input border-blue" type="number" v-model="contractSelect.pocet_tyzdnov" min="0">    
	
	  </div>
	
	  <div class="float-left-zmluva" style="display: none;">
	
	  <label class="popis text-color-green">Dátum ukončenia</label></br>
	  <input class="input border-green" type="date" v-model="contractSelect.date_end"> 
	
	  </div>
	
	  <div class="float-left-100">
	  <label class="popis">Poznámky</label></br>
	  <textarea class="poznamka" v-model="contractSelect.poznamka" rows="3"></textarea>
	  </div>
	
	</form>
	
	</div>