<div v-if="modal.dogShow" class="fullscreen p-1">

<button
  class="btn btn-danger" 
  @click="modal.dogShow = false" 
  style="width: 75px;">
  Späť
</button>

<button 
  class="btn btn-success" 
  style="width: 75px;"
  @click="updateDog()">
  Uložiť
</button>

<button 
  class="btn btn-info"
  style="width: 150px;"
  @click="showContactById_dog()"
  v-if="!modal.contactShow">
  Otvoriť klienta
</button>

{{-- <button 
  class="btn btn-info" 
  style="width: 150px;"
  @click="createRequest()">
  Vytvoriť žiadosť
</button> --}}

<button 
    class="btn btn-danger float-right" 
    style="width: 100px;" 
    @click="deleteDog()">
    Vymazať
</button>

<form autocomplete="off">
    
  <div class="float-left-zmluva">
  
    <label class="popis">Meno</label><br>
    <input class="input" type="text" v-model="dogs.select.meno"><br>

    <label class="popis">Dátum narodenia</label><br>
    <input class="input" type="date" v-model="dogs.select.datum_narodenia"><br>       
  
  </div>

  <div class="float-left-zmluva"> 
    
    <label class="popis">Otec</label><br>      
    <input class="input" type="text" v-model="dogs.select.meno_otec" list="dataOtec"><br> 
    
    <datalist id="dataOtec">
      <option value="Sky Pride Baikal"></option>
      <option value="Nadir Des Jardins D´ Epona"></option>
      <option value="Iron Diamont in the heart"></option>
    </datalist>

    <label class="popis">Číslo čípu</label><br>
    <input class="input" type="text" v-model="dogs.select.cislo_cipu"><br>         

  </div>
  
  <div class="float-left-zmluva"> 

    <label class="popis">Matka</label><br>
    <input class="input" type="text" v-model="dogs.select.meno_matka" list="dataMatka"><br>

    <datalist id="dataMatka">
      <option value="Juliette Diamont in the heart"></option>
      <option value="Kiara Diamont in the heart"></option>
      <option value="Nira Des Jardins D´ Epona"></option>
      <option value="Nira Des Jardins D´ Epona"></option>
      <option value="Orsay Des Jardins D´ Epona"></option>
      <option value="Reginn Diamont in the heart"></option>
      <option value="Michelle Sneh Tatier"></option> 
      <option value="Comet Halley Angelic Soul"></option>
    </datalist>

    <label class="popis">Vrh</label><br>
    <input class="input" type="text" v-model="dogs.select.vrh"><br> 
  
  </div>

  <div class="float-left-zmluva"> 

    <label class="popis">Cena</label><br>
    <input class="input" type="text" v-model="dogs.select.cena"><br>

    <label class="popis">Rezervačný poplatok</label><br>
    <input class="input" type="text" v-model="dogs.select.rezervacny_poplatok"><br>
  
  </div>
  
  <div class="float-left-100">
    <label class="popis">Poznámky</label><br>
    <textarea class="poznamka" v-model="dogs.select.poznamky" rows="3"></textarea>
  </div>

</form>

</div>