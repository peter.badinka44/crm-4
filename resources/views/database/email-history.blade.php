<div v-if="modal.emailHistory" class="fullscreen p-1">

	<div class="d-flex mb-1">

		<button 
			class="btn btn-danger"
			style="width: 75px;"
			@click="modal.emailHistory = false">
			Späť
		</button>

		<input
			v-if="emailHistory.length > 0"
			v-model="search.emailHistory"
			class="form-control ml-1"
			type="text"
			style="width: 275px;"
			placeholder="Vyhľadať"
		>

	</div>

	<table 
		v-if="emailHistory.length > 0"
		class="table-custom">

		<tr>
			<th>Dátum</th>
			<th>Klient</th>
			<th>Email</th>
			<th>Akcia</th>
			<th>Produkt</th>
			<th>Číslo zmluvy</th>
		</tr>
				
		<tr v-for="row in rowsEmailHistory" :key="row.id">
			<td>@{{dateToDMY(row.date_reg)}}</td>
			<td>@{{row.name_full}}</td>
			<td>@{{row.email}}</td>
			<td>@{{row.akcia}}</td>
			<td>@{{row.produkt}}</td>
			<td>@{{row.cislo_zmluvy}}</td>
		</tr>

	</table>

	<div v-if="rowsEmailHistory.length > 0" class="popText mx-1">
		Počet riadkov: @{{rowsEmailHistory.length}}
	</div>

</div>