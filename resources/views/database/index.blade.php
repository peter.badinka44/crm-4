@extends('master')
@section('title', 'Databáza')

@section('content')

	@include('datalists')
	
	@include('database.contacts.index')
	
	@include('database.contracts.all')
	@include('database.contracts.allShow')
	@include('database.dogs.all')	
	
	@include('database.contacts.add')
	@include('database.contacts.show')
	
	@include('database.contracts.index')
	@include('database.contracts.add')
	@include('database.contracts.show')

	@include('database.requests.index')
	@include('database.requests.add')
	@include('database.requests.show')

	@include('database.subContacts.index')
	@include('database.subContacts.add')
	@include('database.subContacts.show')
	
	@include('database.email-history')

	@include('database.dogs.index')
	@include('database.dogs.add')
	@include('database.dogs.show')
	
	{{-- Blank load screen --}}
	<div v-if="modal.database" class="fullscreen p-1"></div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',

	//===================================================================================
	data() {
		return {
			user: {},
			contacts: [],
			contactNew: {},
			contactSelect: {},
			contactSelectOld: {},
			selectedContacts: [],

			customFields: [],
			newCustomField: {
				id: null,
				name: '',
				type: 'text',
				width: 19,
				options: '',
				order: null,
			},

			subContacts: [],
			subContactNew: {},
			subContactSelect: {},

			companies: [],
			databazaSearchAutofocus: null,				
			
			contracts: [],
			contractNew: {},
			contractSelect: {},

			contactSelectContracts: [],
			contactSelectRequests: [],
			contactSelectSubContacts: [],

			dogs: {
				data: [],				
				new: {},
				select: {},
				cislaCipov: [],
				contactSelect: [],
			},

			oContract: {
				sumBebPlan: 0,
				sumBebSkut: 0,
			},
			
			requests: [],
			requestNew: {},
			requestSelect: {},
			requestsTemplates: [],

			filterDate: '-',
			filterUser: '-',

			checkDelete: false,
			checkKlient: false,

			listOdporucil: [],
			listKampan: [],
			emailHistory: [],

			search: {
				name: false,
				contact: '',
				contract: null,
				contractsAll: null,
				request: null,
				subContact: null,
				emailHistory: null,
				dogsContactSelect: null,			
			},

			massChange: {
				datumAkcie: null,			
				stav: null,
				email: null,
				intervalServis: 0,			

				checkdDatumAkcie: false,
				checkStav: false,
				checkEmail: false,
				checkIntervalServis: false,
				checkDelete: false,
				checkCopyEmails: false,
			},
			
			modal: {
				contactIndex: true,
				contactAdd: false,
				contactShow: false,
				contracts: false,
				contractsAll: false,
				contractsAllShow: false,
				contractAdd: false,
				contractShow: false,
				requests: false,
				requestAdd: false,
				requestShow: false,
				subContactIndex: false,
				subContactShow: false,
				subContactAdd: false,
				emailHistory: false,
				database: false,
				dogs: false,
				dogAdd: false,
				dogShow: false,
				dogsAll: false,				
				dogsAllShow: false,
				customFields: false,
			},		

			style: {
				contactNewRC: null,
				contactSelectRC: null,
				contactNewCisloUctu: null,
				contactSelectCisloUctu: null,
				subContactNewRC: null,
				subContactSelectRC: null,
				subContactNewCisloUctu: null,
				subContactSelectCisloUctu: null,			
			},

			allContractsFilter: '-',
			
			appUrl: <?php echo '"' . env('APP_URL') . '"'; ?>,
			offlineMode: false,
		}
	},
	
	//===================================================================================
	methods: {

		saveCustomFields() {
			axios.post('./custom-fields/update-attribute', this.customFields).then(res => {
				this.modal.customFields = false;
				this.modal.contactShow = false;
			});
		},

		//===================================================================================
		addCustomField() {			
			this.newCustomField.id = this.getCustomId();
			this.newCustomField.order = this.getCustomFieldsLength();
			this.customFields.push(JSON.parse(JSON.stringify(this.newCustomField)));
			this.newCustomField = {};
			this.newCustomField = {
				name: '',
				type: 'text',
				width: 19,
				options: '',
			};
		},

		//===================================================================================
		getCustomFieldsLength() {
			return this.customFields.length + 1;
		},

		//===================================================================================
		getOptionsFromString(string) {
			if (string === null || string === '' || string.length === 0) {
				return [];
			}
			return string.split(/\r?\n/);
		},

		//===================================================================================
		changeOrder(order, increase = true) {	
			for(let i = 0; i < this.customFields.length; i++) {

				if (this.customFields[i].order !== order) {
					continue;
				}

				if (increase) {
					let temp = this.customFields[i].order;
					this.customFields[i].order = this.customFields[i - 1].order;
					this.customFields[i - 1].order = temp;
				} else {
					let temp = this.customFields[i].order;
					this.customFields[i].order = this.customFields[i + 1].order;
					this.customFields[i + 1].order = temp;
				}
				
				this.customFields = this.customFields.sort((a, b) => a.order - b.order);
				return;
			}	
		},

		getCustomFields() {
			axios.get('./custom-fields/index').then(res => {
				this.customFields = res.data;
			});
		},

		//===================================================================================
		checkOfflineMode() {
			this.offlineMode = this.appUrl.includes('localhost') ? true : false
		},

		//===================================================================================
		logout() {
			axios.get('./api/logout').then(res => {
				location.reload();
			})
		},

		//===================================================================================
		getUser() {
				axios.get('./users/show').then(res => {
				this.user = res.data
				let validFilterUserValue = false;
				this.user.email_array.forEach(email => {
					if(email == $cookies.get('databazaFilterUser')) {
						validFilterUserValue = true;
					}
				})
				if(validFilterUserValue) this.filterUser = $cookies.get('databazaFilterUser')
				else this.filterUser = this.user.email				
			})			
		},

		//===================================================================================
		handleMassChange() {
			axios.post('./contacts/mass-change', {
				data: this.massChange,
				id: this.selectedContacts,
				filterUser: this.filterUser,
				filterDate: this.filterDate,
				checkDelete: this.checkDelete,
				checkKlient: this.checkKlient,
				checkCopyEmails: this.massChange.checkCopyEmails,
			}).then(res => {
				this.copyEmailsMultiSelect(res.data.emails);
				if(!res.data.err) {
					this.contacts = res.data.contacts
					this.selectedContacts = [],
					this.massChange = {
						datumAkcie: this.dateTodayYMD(),			
						stav: null,
						email: null,
						intervalServis: 0,
						checkdDatumAkcie: false,
						checkStav: false,
						checkEmail: false,
						checkIntervalServis: false,
						checkDelete: false,
						checkCopyEmails: false,
					}					
				} else {
					alert(res.data.err)
				}			
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		// Contacts
		//===================================================================================
		getContacts() {
			axios.post('./contacts/index', {
				filterUser: this.filterUser,
				filterDate: this.filterDate,
				checkDelete: this.checkDelete,
				checkKlient: this.checkKlient,
			}).then(res => {			
				this.contacts = res.data
				this.getContracts()
				this.createListOdporucil()
				this.createListKampan()
			})
		},

		filterAllContracts() {
			if(this.allContractsFilter == '-') this.getContracts()
			if(this.allContractsFilter == '0') this.getContractsVyrocie()
		},

		getContractsVyrocie() {
			axios.get('./requests/indexVyrocie').then(res => {	
				this.contracts = res.data
				console.log(this.contracts)
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		getOnlyContacts() {
			axios.post('./contacts/index', {
				filterUser: this.filterUser,
				filterDate: this.filterDate,
				checkDelete: this.checkDelete,
				checkKlient: this.checkKlient,
			}).then(res => {			
				this.contacts = res.data
			})
		},

		//===================================================================================
		addContact() {
			axios.post('./contacts/store', {
				data: this.contactNew
			}).then(res => {
				if(res) {
					this.modal.contactAdd = false;
					this.contactNew = {}
					this.getOnlyContacts()
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		showContact(row) {
			this.contactSelect = JSON.parse(JSON.stringify(row))
			this.contactSelectOld = JSON.parse(JSON.stringify(row))
			this.modal.contactShow = true
			console.log(row)
			// contracts
			this.contactSelectContracts = [];
			this.oContract.sumBebPlan = 0
			this.oContract.sumBebSkut = 0
			for(let i = 0; i < this.contracts.length; i++) {
				if(this.contracts[i].id_osoba == this.contactSelect.id_person) {
					this.contactSelectContracts.push(JSON.parse(JSON.stringify(this.contracts[i])))
					this.oContract.sumBebPlan += Number(this.contracts[i].beb_plan)
					this.oContract.sumBebSkut += Number(this.contracts[i].beb_skut)
				}
			}			
			// requests
			this.contactSelectRequests = [];
			for(let i = 0; i < this.requests.length; i++) {
				if(this.requests[i].id_person == this.contactSelect.id_person) {
					this.contactSelectRequests.push(JSON.parse(JSON.stringify(this.requests[i])))
				}
			}
			// subContacts
			this.contactSelectSubContacts = [];	
			for(let i = 0; i < this.subContacts.length; i++) {
				if(this.subContacts[i].id_person == this.contactSelect.id_person) {
					this.contactSelectSubContacts.push(JSON.parse(JSON.stringify(this.subContacts[i])))
				}
			}
			//customFields
			if (row.custom_fields && this.customFields.length > 0) {
				this.customFields.forEach(field => {
					field.value = null
				})
				row.custom_fields.forEach(field => {
					Object.keys(field).forEach(key => {
						let value = field[key];
						this.customFields.forEach(customField => {
							if (customField.id === key) {
								customField.value = value;
							}
						});
					});
				})
			} else {
				if (this.customFields.length > 0) {
					this.customFields.forEach(field => {
						field.value = null;
					});
				}
			}
			this.validateRodneCisloContactSelect()
			this.validateIbanContactSelect();
		},

		//===================================================================================
		showContactById() {
			this.contacts.forEach(row => {
				if(row.id_person == this.contractSelect.id_osoba) {
					this.showContact(row);
					return;
				}
			})
		},

		//===================================================================================
		updateContact() {
			this.contactSelect.custom_fields = this.getCustomFieldsValues();
			console.log(this.contactSelect)
			axios.post('./contacts/update', {
				data: this.contactSelect
			}).then((res) => {
				if(res.data) {
					this.getOnlyContacts()
					this.modal.contactShow = false
					if(this.databazaSearchAutofocus) this.$refs.refAutofocus.focus()
				}
			}, (err) => {
				alert(err)
			})
		},

		getCustomFieldsValues() {
			let values = [];
			this.customFields.forEach(field => {
				values.push({[field.id]: field.value});
			});
			return values;
		},

		//===================================================================================
		destroyContact() {
			axios.get('./contacts/destroy/'+this.contactSelect.id_person).then(res => {				
				if(res) {
					this.getOnlyContacts()
					this.modal.contactShow = false
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		restoreContact() {
			axios.get('./contacts/restore/'+this.contactSelect.id_person).then(res => {
				if(res) {
					this.getOnlyContacts()
					this.modal.contactShow = false
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		backContact() {
			if(JSON.stringify(this.contactSelect) === JSON.stringify(this.contactSelectOld)) {
				this.modal.contactShow = false
				if(this.databazaSearchAutofocus) this.$refs.refAutofocus.focus()
			} else {
				if(confirm('Neboli uložené zmeny. Je potrebne potvrdiť akciu.')) {
					this.modal.contactShow = false
					if(this.databazaSearchAutofocus) this.$refs.refAutofocus.focus()
				}
			}
		},

		//===================================================================================
		userSwap() {
			let _email = prompt('Zadaj email: ')
			axios.post('./contacts/user-swap', {
				email: _email,
				contact: this.contactSelect,
			}).then(res => {
				if(res.data.action == true) {
					this.getOnlyContacts()
					this.modal.contactShow = false
				} else {
					alert(`Kontakt sa nepodrailo priradit k uctu [${_email}]. Skontrolujte pripojenie k internetu alebo spravnost emailu.`)
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		markKlient() {
			// let phone = prompt(`Aby bolo možné označiť kontakt ako klienta, je potrebné zadať telefonné číslo v tvare '0912345678'. Následne sa tóto číslo označí aj v databázach Call-Page a Kataster.`, this.contactSelect.phone)
			// if(phone.length == 10) {
				axios.post('/contacts/mark-klient', {
					contact: this.contactSelect,
					phone: null,
				}).then(res => {	
					for(let i = 0; i < this.contacts.length; i++) {
						if(this.contacts[i]['id_person'] == this.contactSelect['id_person']) {					
							this.contactSelect['check_klient'] = res.data.check_klient
							this.contacts[i]['check_klient'] = res.data.check_klient
							break
						}
						this.getOnlyContacts()
					}
				}, err => {
					alert(err)
				})
			// } else {
			// 	alert('Neplatné číslo...')
			// }			
		},

		//===================================================================================
		createOpenZapis() {			
			if(this.contactSelect.zapis_link.length > 10) 
			{
				let win = window.open(this.contactSelect.zapis_link, '_blank')
				win.focus()
			} 			
			else if(this.contactSelect.zapis_link == 'true')
			{				
				axios.get('./contacts/'+this.contactSelect.id_person).then(res => {						
					if(res.data[0].zapis_link.length > 10) {
						this.contactSelect = res.data[0]
						this.getContacts()
						let win = window.open(this.contactSelect.zapis_link, '_blank')
						win.focus()
					}
				})
			}
			else if(this.contactSelect.zapis_link == 'false')
			{
				axios.post('./contacts/create-zapis', {
				contact: this.contactSelect
				}).then(res => {
					let url = 'https://script.google.com/macros/s/AKfycbzrhsC5A7QYPfFEdEgBmoDyjICml_Vgzk1BAsq26dYbrytGBgsd/exec'
					url += '?track=create_zapis';
					url += '&id=' + this.contactSelect['id'];
					url += '&name=' + this.contactSelect['name_full'];
					var win = window.open(url, '_blank');
					win.focus();
					this.contactSelect.zapis_link = 'true'
				}, err => {
					alert(err)
				})
			}			
		},

		//===================================================================================
		createOpenFinPlan() {			
			if(this.contactSelect.financny_plan_link.length > 10) 
			{
				let win = window.open(this.contactSelect.financny_plan_link, '_blank')
				win.focus()
			} 			
			else if(this.contactSelect.financny_plan_link == 'true')
			{				
				axios.get('./contacts/'+this.contactSelect.id_person).then(res => {						
					if(res.data[0].financny_plan_link.length > 10) {
						this.contactSelect = res.data[0]
						this.getContacts()
						let win = window.open(this.contactSelect.financny_plan_link, '_blank')
						win.focus()
					}
				})
			}
			else if(this.contactSelect.financny_plan_link == 'false')
			{
				axios.post('./contacts/create-fin-plan', {
				contact: this.contactSelect
				}).then(res => {
					let url = 'https://script.google.com/macros/s/AKfycbyn1D23Nf-591c9LBFrUAWpQZUcnRUKLSa1BQYj7LPXFNKmdfX7/exec'
					url += '?track=create_fp';
					url += '&id=' + this.contactSelect['id'];
					url += '&name=' + this.contactSelect['name_full'];
					var win = window.open(url, '_blank');
					win.focus();
					this.contactSelect.financny_plan_link = 'true'
				}, err => {
					alert(err)
				})
			}			
		},

		//===================================================================================
		// Contracts
		//===================================================================================
		getContracts() {
			axios.get('./contracts/index/'+this.filterUser).then(res => {
				this.contracts = res.data
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		addContracts() {
			this.contractNew.id_osoba = this.contactSelect.id_person
			axios.post('./contracts/store', {
				data: this.contractNew
			}).then(res => {
				this.contactSelectContracts = res.data
				this.getContacts()
				this.modal.contractAdd = false
				// this.modal.contracts = true
				this.contractNew = {}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		createRequest() {
			this.requestNew = {}
			this.requestNew.typ_ziadosti = '-'
			this.requestNew.institucia = this.contractSelect.spolocnost
			this.requestNew.program = this.contractSelect.produkt
			this.requestNew.cislo_zmluvy = this.contractSelect.cislo_zmluvy
			this.requestNew.name_full = this.contractSelect.name_full
			this.requestNew.datum_narodenia = dateToDMY(this.contactSelect.datum_narodenia)
			this.requestNew.ulica = this.contactSelect.ulica
			this.requestNew.psc_obec = this.contactSelect.psc_obec
			this.requestNew.cislo_uctu = this.contactSelect.cislo_uctu
			this.modal.requests = true
			this.modal.requestAdd = true
			this.modal.contracts = false
			this.modal.contractShow = false
		},

		//===================================================================================
		showContract(row) {
			this.contractSelect = row
			this.modal.contractShow = true
		},		

		//===================================================================================
		updateContract() {
			axios.post('./contracts/update', {
				data: this.contractSelect
			}).then(res => {
				this.getContracts()
				for(let i = 0; i < this.contacts.length; i++) {
					if(this.contacts[i].id_person == this.contractSelect.id_osoba) {
						this.contacts[i].beb_zmluvy = res.data.beb
					}
				}
				this.modal.contractShow = false
				this.contractSelect.beb
			}, err => {
				alert(err)
			})
		},
		
		//===================================================================================
		destroyContract() {
			axios.post('./contracts/destroy', {
				data: this.contractSelect
			}).then(res => {
				if(res) {
					let newContracts = this.contactSelectContracts.filter(
						x => x.id != this.contractSelect.id
					)
					this.contactSelectContracts = newContracts
					this.getContracts()
					for(let i = 0; i < this.contacts.length; i++) {
						if(this.contacts[i].id_person == this.contractSelect.id_osoba) {
							this.contacts[i].beb_zmluvy = res.data.beb
						}
					}
					this.modal.contractShow = false
					this.modal.contracts = true
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		// Requests
		//===================================================================================
		getRequests() {
			axios.get('./requests/index').then(res => {
				this.requests = res.data
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		getRequestsTemplates() {
			axios.get('./requests/companies').then(res => {
				this.companies = res.data
			})
			axios.get('./requests/templates').then(res => {
				this.requestsTemplates = res.data
			})
		},

		//===================================================================================
		addRequest() {
			this.requestNew.t_miesto_datum_podpisu = this.requestNew.miesto_podpisu + ', dňa ' + dateToDMY(new Date())
			this.requestNew.t_odosielatel = this.requestNew.name_full + ', ' + this.requestNew.ulica + ', ' + this.requestNew.psc_obec
			this.updateTemplateNew();
			axios.post('./requests/store', {
				contact: this.contactSelect,
				request: this.requestNew,
			}).then(res => {
				this.contactSelectRequests = res.data	
				this.getRequests()
				this.modal.requestAdd = false
				// this.modal.requests = true
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		showRequest(row) {
			this.requestSelect = row
			this.requestSelect.datum_podpisu = dateToDMY(this.requestSelect.datum_podpisu)
			// this.updateTemplate()
			this.modal.requestShow = true
		},

		//===================================================================================
		updateTemplate() {
			this.companies.forEach(row => {
				if(row.name == this.requestSelect.institucia) {
					this.requestSelect.address_1 = row.address_1
					this.requestSelect.address_2 = row.address_2
					this.requestSelect.address_3 = row.address_3
					this.requestSelect.address_4 = row.address_4
					this.requestSelect.address_5 = row.address_5
				}
			})
			this.requestsTemplates.forEach(row => {
				if(row.typ == this.requestSelect.typ_ziadosti) {
					this.requestSelect.t_vec = row.vec
					this.requestSelect.body = row.body
				}
			})
		},

		//===================================================================================
		updateTemplateNew() {
			this.companies.forEach(row => {
				if(row.address_1 == this.requestNew.institucia) {
					this.requestNew.address_1 = row.address_1
					this.requestNew.address_2 = row.address_2
					this.requestNew.address_3 = row.address_3
					this.requestNew.address_4 = row.address_4
					this.requestNew.address_5 = row.address_5
				}
			})
			this.requestsTemplates.forEach(row => {
				if(row.typ == this.requestNew.typ_ziadosti) {
					this.requestNew.t_vec = row.vec
					this.requestNew.body = row.body
				}
			})
		},

		//===================================================================================
		destroyRequest() {
			axios.post('./requests/destroy', {
				data: this.requestSelect
			}).then(res => {
				this.contactSelectRequests = res.data
				this.modal.requestShow = false
				this.getRequests()
			})
		},

		//===================================================================================
		updateRequestTyp(value) {
			this.requestsTemplates.forEach(row => {
				if(row.typ == value) {
					this.requestSelect.t_vec = row.vec
					this.requestSelect.body = row.body
				}
			})
		},

		//===================================================================================
		updateRequest() {
			axios.post('./requests/update', {
				data: this.requestSelect
			}).then(res => {
				this.requestSelect = res.data
				this.modal.requestShow = false				
				this.getRequests()
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		navigateRequest(href, newTab) {
			axios.post('./requests/update', {
				data: this.requestSelect
			}).then(res => {
				this.contactSelectRequests = res.data
				this.getRequests()
				this.navigate(href, newTab)
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		requestNewShow() {
			this.requestNew = {
				typ_ziadosti: '-',
				name_full: this.contactSelect.name_full,
				datum_narodenia: dateToDMY(this.contactSelect.datum_narodenia),
				ulica: this.contactSelect.ulica,
				psc_obec: this.contactSelect.psc_obec,
				cislo_uctu: this.contactSelect.cislo_uctu,
			}
			this.modal.requestAdd = true
		},

		//===================================================================================
		// SubContacts
		//===================================================================================
		getSubContacts(){
			axios.get('./sub-contacts/index').then(res => {
				this.subContacts = res.data
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		addSubContact() {
			axios.post('./sub-contacts/store', {
				contact: this.contactSelect,
				subContact: this.subContactNew,
			}).then(res => {
				this.contactSelectSubContacts = res.data	
				this.getSubContacts()
				this.modal.subContactAdd = false
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		destroySubContact() {
			axios.post('./sub-contacts/destroy', {
				data: this.subContactSelect
			}).then(res => {
				if(res) {
					this.contactSelectSubContacts = res.data
					this.getSubContacts()
					this.modal.subContactShow = false
				}
			}, err => {
				alert(err)
			})
		},

		//===================================================================================
		showSubContact(row) {
			this.subContactSelect = row
			this.modal.subContactShow = true
			this.validateRodneCisloSubContactSelect()
			this.validateIbanSubContactSelect()
		},

		//===================================================================================
		updateSubContact() {
			axios.post('./sub-contacts/update', {
				data: this.subContactSelect
			}).then((res) => {
				if(res.data) {
					this.getSubContacts()
					this.modal.subContactShow = false
				}
			}, (err) => {
				alert(err)
			})
		},

		//===================================================================================
		// Others
		//===================================================================================
		validateRodneCisloContactNew(){
			let num = '';
			let rodneCislo = this.contactNew.rodne_cislo
			for(var i = 0; i < rodneCislo.length; i++){			
				if(isNaN(rodneCislo[i]) == false){
					num += rodneCislo[i];
				}
			}
			if(num.length > 0){
				num = num % 11;
				if(num == 0){
					this.style.contactNewRC = { backgroundColor: '#c6ffb3' }
					var datNar = this.RC_rokNarodenia(rodneCislo);
					this.contactNew.datum_narodenia = this.RC_rokNarodenia(rodneCislo);
				} else {
					this.style.contactNewRC = { backgroundColor: '#ff3300' }
				}
			}
			if(rodneCislo.length == 0) {
				this.style.contactNewRC = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateRodneCisloContactSelect(){
			let num = '';
			rodneCislo = this.contactSelect.rodne_cislo
			for(var i = 0; i < rodneCislo.length; i++){			
				if(isNaN(rodneCislo[i]) == false){
					num += rodneCislo[i];
				}
			}
			if(num.length > 0){
				num = num % 11;
				if(num == 0){
					this.style.contactSelectRC = { backgroundColor: '#c6ffb3' }
					var datNar = this.RC_rokNarodenia(rodneCislo);
					this.contactSelect.datum_narodenia = this.RC_rokNarodenia(rodneCislo);
				} else {
					this.style.contactSelectRC = { backgroundColor: '#ff3300' }
				}
			}
			if(rodneCislo.length == 0) {
				this.style.contactSelectRC = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateRodneCisloSubContactNew(){
			let num = '';
			let rodneCislo = this.subContactNew.rodne_cislo
			for(var i = 0; i < rodneCislo.length; i++){			
				if(isNaN(rodneCislo[i]) == false){
					num += rodneCislo[i];
				}
			}
			if(num.length > 0){
				num = num % 11;
				if(num == 0){
					this.style.subContactNewRC = { backgroundColor: '#c6ffb3' }
					var datNar = this.RC_rokNarodenia(rodneCislo);
					this.subContactNew.datum_narodenia = this.RC_rokNarodenia(rodneCislo);
				} else {
					this.style.subContactNewRC = { backgroundColor: '#ff3300' }
				}
			}
			if(rodneCislo.length == 0) {
				this.style.subContactNewRC = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateRodneCisloSubContactSelect(){
			let num = '';
			let rodneCislo = this.subContactSelect.rodne_cislo
			for(var i = 0; i < rodneCislo.length; i++){			
				if(isNaN(rodneCislo[i]) == false){
					num += rodneCislo[i];
				}
			}
			if(num.length > 0){
				num = num % 11;
				if(num == 0){
					this.style.subContactSelectRC = { backgroundColor: '#c6ffb3' }
					var datNar = this.RC_rokNarodenia(rodneCislo);
					this.subContactSelect.datum_narodenia = this.RC_rokNarodenia(rodneCislo);
				} else {
					this.style.subContactSelectRC = { backgroundColor: '#ff3300' }
				}
			}
			if(rodneCislo.length == 0) {
				this.style.subContactSelectRC = { backgroundColor: '#fff' }
			}
		},		

		//===================================================================================
		RC_rokNarodenia(rodneCislo) { 
			var datum =  new Date();
			var d = datum.getDate();  
			var m = datum.getMonth() + 1;
			var y = datum.getFullYear();

			y += " ";  

			var yy = y[2] + y[3];

			if(rodneCislo != ""){
				var datumNarodenia = "";

				var rok = rodneCislo[0] + rodneCislo[1];
				if(rok >= 0 && rok <= yy){
					datumNarodenia += "20";
					datumNarodenia += rok;
				} else {
					datumNarodenia += "19";
					datumNarodenia += rok;
				}
				datumNarodenia += "-";			

				if(rodneCislo[2] == 5) datumNarodenia += "0";
				if(rodneCislo[2] == 6) datumNarodenia += "1";
				if(rodneCislo[2] == 0 || rodneCislo[2] == 1){
					datumNarodenia += rodneCislo[2];			
				}
				datumNarodenia += rodneCislo[3];
				datumNarodenia += "-";

				datumNarodenia += rodneCislo[4];
				datumNarodenia += rodneCislo[5];			
				return datumNarodenia;						
			} else return "";
		},

		//===================================================================================
		validateIbanContactNew(){
			var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
			var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
			var iban = this.contactNew.cislo_uctu;
			if(iban.length > 0){	
				var ibanConvert = "";
				for(var i = 0; i < iban.length; i++){
					var temp = iban.slice(i, i + 1);
					if (temp == " ") {
					} else {
						ibanConvert += temp;
					}			
				}	
				var temp4 = ibanConvert.slice(2, 4);
				var checkTrueIban = this.checkIban(ibanConvert, S, K, temp4);
				if(checkTrueIban == 1){
					this.style.contactNewCisloUctu = { backgroundColor: '#c6ffb3' }
				} else{
					this.style.contactNewCisloUctu = { backgroundColor: '#ff3300' }
				}	
			}
			if(iban.length == 0) {
				this.style.contactNewCisloUctu = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateIbanContactSelect(){
			var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
			var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
			var iban = this.contactSelect.cislo_uctu;
			if(iban.length > 0){	
				var ibanConvert = "";
				for(var i = 0; i < iban.length; i++){
					var temp = iban.slice(i, i + 1);
					if (temp == " ") {
					} else {
						ibanConvert += temp;
					}			
				}	
				var temp4 = ibanConvert.slice(2, 4);
				var checkTrueIban = this.checkIban(ibanConvert, S, K, temp4);
				if(checkTrueIban == 1){
					this.style.contactSelectCisloUctu = { backgroundColor: '#c6ffb3' }
				} else{
					this.style.contactSelectCisloUctu = { backgroundColor: '#ff3300' }
				}	
			}
			if(iban.length == 0) {
				this.style.contactSelectCisloUctu = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateIbanSubContactNew(){
			var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
			var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
			var iban = this.subContactNew.cislo_uctu;
			if(iban.length > 0){	
				var ibanConvert = "";
				for(var i = 0; i < iban.length; i++){
					var temp = iban.slice(i, i + 1);
					if (temp == " ") {
					} else {
						ibanConvert += temp;
					}			
				}	
				var temp4 = ibanConvert.slice(2, 4);
				var checkTrueIban = this.checkIban(ibanConvert, S, K, temp4);
				if(checkTrueIban == 1){
					this.style.subContactNewCisloUctu = { backgroundColor: '#c6ffb3' }
				} else{
					this.style.subContactNewCisloUctu = { backgroundColor: '#ff3300' }
				}	
			}
			if(iban.length == 0) {
				this.style.subContactNewCisloUctu = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		validateIbanSubContactSelect(){
			var A=10; var B=11; var C=12; var D=13; var E=14; var F=15; var G=16; var H=17; var I=18; var J=19; var K=20; var L=21; var M=22;
			var N=23; var O=24; var P=25; var Q=26; var R=27; var S=28; var T=29; var U=30; var V=31; var W=32; var X=33; var Y=34; var Z=35;
			var iban = this.subContactSelect.cislo_uctu;
			if(iban.length > 0){	
				var ibanConvert = "";
				for(var i = 0; i < iban.length; i++){
					var temp = iban.slice(i, i + 1);
					if (temp == " ") {
					} else {
						ibanConvert += temp;
					}			
				}	
				var temp4 = ibanConvert.slice(2, 4);
				var checkTrueIban = this.checkIban(ibanConvert, S, K, temp4);
				if(checkTrueIban == 1){
					this.style.subContactSelectCisloUctu = { backgroundColor: '#c6ffb3' }
				} else{
					this.style.subContactSelectCisloUctu = { backgroundColor: '#ff3300' }
				}	
			}
			if(iban.length == 0) {
				this.style.subContactSelectCisloUctu = { backgroundColor: '#fff' }
			}
		},

		//===================================================================================
		checkIban(resultIban, S, K, diferencia98){
			var tempIban = resultIban.slice(4, 26);
			var tempIban2 = tempIban.toString() + S.toString() + K.toString() + diferencia98.toString();
			var zLava8 = tempIban2.slice(0, 8);
			var zvysokZlava8 = zLava8 % 97;
			var next8Zlava = tempIban2.slice(8, 16);
			var kompoziciaSnavratmy = zvysokZlava8.toString() + next8Zlava.toString();
			var nextZvysok = kompoziciaSnavratmy % 97;
			var next6Zlava = tempIban2.slice(16, 22);
			var kompoziciaSnavratmy2 = nextZvysok.toString() + next6Zlava.toString();
			var nextZvysok2 = kompoziciaSnavratmy2 % 97;
			var next4Zlava = tempIban2.slice(22, 26);
			var kompoziciaSnavratmy3 = nextZvysok2.toString() + next4Zlava.toString();
			var nextZvysok3 = kompoziciaSnavratmy3 % 97;	
			return nextZvysok3;	
		},

		getDatumNarodenia(date) {			
			return this.dateToDMY(date) + ' (' + this.getAge(date) + ')';
		},

		getAge(date) {
			if (date == '0000-00-00') {
				return '';
			}
			let today = new Date();
			let birthDate = new Date(date);
			let age = today.getFullYear() - birthDate.getFullYear();
			let m = today.getMonth() - birthDate.getMonth();
			if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
				age--;
			}
			return age;
		},
		
		//===================================================================================
		dateToDMY(date) {
			if(date == "" || date == " "){
				return "";
			}
			var datum =  new Date(date);
			var d = datum.getDate();
			var m = datum.getMonth() + 1; //Month from 0 to 11
			var y = datum.getFullYear();
			let result = '' + (d <= 9 ? '0' + d : d) + '.' + (m<=9 ? '0' + m : m) + '.' + y
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		dateTodayYMD() {
			var datum =  new Date();
			var d = datum.getDate();
			var m = datum.getMonth() + 1; //Month from 0 to 11
			var y = datum.getFullYear();
			let result = y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d)
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		getCustomId() {
			var datum =  new Date();
			var d = datum.getDate();
			var m = datum.getMonth() + 1; //Month from 0 to 11
			var y = datum.getFullYear();	
			let result = y + (m<=9 ? '0' + m : m) + (d <= 9 ? '0' + d : d)
			result = result + datum.getHours() + datum.getMinutes() + datum.getSeconds() + datum.getMilliseconds()
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		datePlus10YearsYMD(date) {	
			var datum =  new Date(date);
			var d = datum.getDate();
			var m = datum.getMonth() + 1; //Month from 0 to 11
			var y = datum.getFullYear(); y += 10;			
			let result = y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d)
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		dateToY(date) {
			if(date == "" || date == " "){
				return "";
			}
			let datum = new Date(date)
			let y = datum.getFullYear();
			let result = y
			if(result == 'NaN.NaN.NaN') result = ''
			return result
		},

		//===================================================================================
		navigate(href, newTab) {
			var a = document.createElement('a');
			a.href = href;
			if (newTab) {
				a.setAttribute('target', '_blank');
			}
			a.click();
		},

		//===================================================================================
		copyEmails() {
			let emails = ''
			this.contacts.forEach(row => {
				if(row.check_klient == 'true') {
					if(row.email.length > 0) {
						emails += row.email + ', '
					}
				}
			})
			emails = emails.substring(0, emails.length - 2)
			this.copyToClipboard(emails)
			alert('Emaily boli úspešné skopirované. Pre vloženie je možné použiť klavesovú skratku "CTRL + V".')
		},

		//===================================================================================
		copyEmailsMultiSelect(emails) {
			let _emails = '';
			if (emails.length > 0) {
				emails.forEach(row => {				
					if(row.email.length > 0) {
						_emails += row.email + ', '
					}
				})
				_emails = _emails.substring(0, _emails.length - 2)
				this.copyToClipboard(_emails)
				alert('Emaily boli úspešné skopirované. Pre vloženie je možné použiť klavesovú skratku "CTRL + V".')
			}			
		},

		//===================================================================================
		copyToClipboard(text) {
			var dummy = document.createElement("textarea"); 
			document.body.appendChild(dummy);    
			dummy.value = text;
			dummy.select();
			document.execCommand("copy");
			document.body.removeChild(dummy);
		},

		//===================================================================================
		createListOdporucil() {
			this.contacts.forEach(row => {
				if(row.check_klient == 'true') {
					let item = ''
					item += row.name_full
					item += ' [' + this.dateToY(row.datum_narodenia) + ']'
					item += ', <' + row.id_user + '>'
					item += ', •' + row.id_person
					this.listOdporucil.push(item)
				}
			})
		},

		//===================================================================================
		createListKampan() {
			let array = []
			this.contacts.forEach(row => {
				if(row.kampan.length > 0) {
					array.push(row.kampan)
				}				
			})
			this.listKampan = this.uniq(array)
		},

		//===================================================================================
		uniq(a) {
			return a.sort().filter(function(item, pos, ary) {
				return !pos || item != ary[pos - 1];
			});
		},

		//===================================================================================
		getEmailHistory() {
			axios.get('./users/get-email-history').then(res => {
				this.emailHistory = res.data
			}, err => {
				aler(err)
			})
		},

		//===================================================================================
		filterDateChange() {
			$cookies.set('databazaFilterDate', this.filterDate, '30d')			
			this.getOnlyContacts()
		},

		//===================================================================================
		filterUserChange() {
			$cookies.set('databazaFilterUser', this.filterUser, '30d')
			this.getContacts()
		},

		//===================================================================================
		changeAutofocus() {
			this.databazaSearchAutofocus = !this.databazaSearchAutofocus
			$cookies.set('databazaAutofocus', this.databazaSearchAutofocus, '30d')
			this.$refs.refAutofocus.focus()
		},

		//===================================================================================
		getDogs() {
			axios.get('./dogs/getAll').then(res => {	
				this.dogs.data = res.data
			}, err => alert(err))
		},

		//===================================================================================
		addDogShow() {
			this.modal.dogAdd = true;
			this.dogs.new = { 
				meno: ' Angelic Soul',
				cena: 1300,
				rezervacny_poplatok: '100',
			};
			this.dogs.cislaCipov = []
			this.dogs.data.forEach(row => {
				this.dogs.cislaCipov.push(row.cislo_cipu)
			})
		},

		//===================================================================================
		addDog() {
			axios.post('./dogs/store', {
				dog: this.dogs.new,
				contact: this.contactSelect
			}).then(res => {
				this.dogs.contactSelect = res.data
				this.modal.dogAdd = false
				this.getDogs()
			}, err => alert(err))
		},

		//===================================================================================
		showDog(row) {			
			this.dogs.select = row
			this.modal.dogShow = true
			console.log(this.dogs.select)
		},

		//===================================================================================
		updateDog() {
			axios.post('./dogs/update', {
				data: this.dogs.select
			}).then(res => {
				if(res.data) {
					this.modal.dogShow = false
				}
			}, err => alert(err))
		},

		//===================================================================================
		deleteDog() {
			axios.post('./dogs/delete', {
				data: this.dogs.select
			}).then(res => {
				if(res.data) {
					this.dogs.contactSelect = this.dogs.contactSelect.filter(row => row.id != this.dogs.select.id)
					this.dogs.data = this.dogs.data.filter(row => row.id != this.dogs.select.id)
					this.modal.dogShow = false
				}
			}, err => alert(err))
		},

		//===================================================================================
		showContactDogs() {
			this.dogs.contactSelect = this.dogs.data.filter(row => row.id_person == this.contactSelect.id_person)
			console.log(this.dogs.contactSelect)
			this.modal.dogs = true
		},

		//===================================================================================
		getDogOwner(row) {
			let owner = ''
			this.contacts.forEach(contact => {
				if(contact.id_person == row.id_person) {
					owner = contact.name_full
				}
			})
			return owner
		},

		//===================================================================================
		getAllDogs() {
			// axios.get('./dogs/' + this.dogs.select.id_person).then(res => {
			// 	console.log(res.data)
			// }, err => alert(err))
		},

		//===================================================================================
		showContactById_dog() {
			this.contacts.forEach(row => {
				if(row.id_person == this.dogs.select.id_person) {
					this.showContact(row);
					this.modal.dogShow = false;
					return;
				}
			})
		},

		//===================================================================================
		openExternyPristup() {
			let win = window.open('./angelic-soul/'+this.contactSelect.id_person, '_blank')
			win.focus()
		},

		//===================================================================================
		test() {
			
		},

	},

	//===================================================================================
	computed: {	

		//===================================================================================
		selectedContactsAll: {
			get: function () {
				return this.rows ? this.selectedContacts.length == this.rows.length : false;
			},
			set: function (value) {
				var selected = [];
				if (value) {
					this.rows.forEach(function (row) {
						selected.push(row.id);
					});
				}
				this.selectedContacts = selected;
			}
		},


		//===================================================================================
		cols () {
			return this.contacts.length >= 1 ? Object.keys(this.contacts[0]) : []
		},
		rows () {
			if (!this.contacts.length) {
				return []
			}

			if (this.search.name) {
				return this.contacts.filter(item => {
					return item.name_full.toLowerCase().includes(this.search.contact.toLowerCase()) 
				})
			}

			return this.contacts.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.contact || ((typeof prop === 'string')
					? prop.toLowerCase().includes(this.search.contact.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.contact.toLowerCase())))
			})
		},

		//===================================================================================
		colsContractsAll () {
			return this.contracts.length >= 1 ? Object.keys(this.contracts[0]) : []
		},
		rowsContractsAll () {
			if (!this.contracts.length) {
				return []
			}
			return this.contracts.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.contractsAll || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.contractsAll.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.contractsAll.toLowerCase())))
			})
		},

		//===================================================================================
		colsContactSelectContracts () {
			return this.contactSelectContracts.length >= 1 ? Object.keys(this.contactSelectContracts[0]) : []
		},
		rowsContactSelectContracts () {
			if (!this.contactSelectContracts.length) {
				return []
			}
			return this.contactSelectContracts.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.contract || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.contract.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.contract.toLowerCase())))
			})
		},

		//===================================================================================
		colsContactSelectRequests () {
			return this.contactSelectRequests.length >= 1 ? Object.keys(this.contactSelectRequests[0]) : []
		},
		rowsContactSelectRequests () {
			if (!this.contactSelectRequests.length) {
				return []
			}
			return this.contactSelectRequests.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.request || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.request.toLowerCase())
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.request.toLowerCase())))
			})
		},

		//===================================================================================
		colsContactSelectSubContacts () {
			return this.contactSelectSubContacts.length >= 1 ? Object.keys(this.contactSelectSubContacts[0]) : []
		},
		rowsContactSelectSubContacts () {
			if (!this.contactSelectSubContacts.length) {
				return []
			}
			return this.contactSelectSubContacts.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.subContact || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.subContact.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.subContact.toLowerCase())))
			})
		},

		//===================================================================================
		colsDogsContactSelect () {
			return this.dogs.contactSelect.length >= 1 ? Object.keys(this.dogs.contactSelect[0]) : []
		},
		rowsDogsContactSelect () {
			if (!this.dogs.contactSelect.length) {
				return []
			}
			return this.dogs.contactSelect.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.dogsContactSelect || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.dogsContactSelect.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.dogsContactSelect.toLowerCase())))
			})
		},

		//===================================================================================
		colsDogsAll () {
			return this.dogs.data.length >= 1 ? Object.keys(this.dogs.data[0]) : []
		},
		rowsDogsAll () {
			if (!this.dogs.data.length) {
				return []
			}
			return this.dogs.data.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.dogsAll || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.dogsAll.toLowerCase()) 
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.dogsAll.toLowerCase())))
			})
		},

		//===================================================================================
		colsEmailHistory () {
			return this.emailHistory.length >= 1 ? Object.keys(this.emailHistory[0]) : []
		},
		rowsEmailHistory () {
			if (!this.emailHistory.length) {
				return []
			}
			return this.emailHistory.filter(item => {
				let props = Object.values(item)
				return props.some(prop => !this.search.emailHistory || ((typeof prop === 'string') 
					? prop.toLowerCase().includes(this.search.emailHistory.toLowerCase())
					: ''))
					// : prop.toString(10).toLowerCase().includes(this.search.emailHistory.toLowerCase())))
			})
		},

	},

  	//===================================================================================
	mounted() {
		this.checkOfflineMode()
		this.getUser()		
		if($cookies.get('databazaAutofocus') == 'true') this.databazaSearchAutofocus = true
		else this.databazaSearchAutofocus = false
		if($cookies.get('databazaFilterDate') == null) this.filterDate = '-'
		else this.filterDate = $cookies.get('databazaFilterDate')		

		setTimeout(function() {
			this.getContacts()
			this.getRequests()
			this.getRequestsTemplates()
			this.getSubContacts()
			this.getEmailHistory()
			// this.getDogs()
			this.getCustomFields();
		}.bind(this), 800)

		this.massChange.datumAkcie = this.dateTodayYMD()

		//===================================================================================
		// ESC
		//===================================================================================
		var vm = this;	
		var ctrlQ = 1;
		window.addEventListener('keyup', function(event) {   
			if (event.keyCode == 27) {

				if(vm.modal.emailHistory) { vm.modal.emailHistory = false; return }
				if(vm.modal.contactAdd) { vm.modal.contactAdd = false; return }

				if(vm.modal.contractAdd) { vm.modal.contractAdd = false; return }
				if(vm.modal.contractShow) { vm.modal.contractShow = false; return }				
				
				if(vm.modal.requestAdd) { vm.modal.requestAdd = false; return }
				if(vm.modal.requestShow) { vm.modal.requestShow = false; return }
				
				if(vm.modal.subContactAdd) { vm.modal.subContactAdd = false; return }
				if(vm.modal.subContactShow) { vm.modal.subContactShow = false; return }
				
				if(vm.modal.requests) { vm.modal.requests = false; ctrlQ = 1; return }
				if(vm.modal.contracts) { vm.modal.contracts = false; ctrlQ = 1; return }				
				if(vm.modal.subContactIndex) { vm.modal.subContactIndex = false; ctrlQ = 1; return }

				if(vm.modal.contactShow) {
					if(JSON.stringify(vm.contactSelect) === JSON.stringify(vm.contactSelectOld)) {
						vm.modal.contactShow = false
						if(vm.databazaSearchAutofocus) vm.$refs.refAutofocus.focus()							
					} else {
						if(confirm('Neboli uložené zmeny. Je potrebne potvrdiť akciu.')) {
							vm.modal.contactShow = false
							if(vm.databazaSearchAutofocus) vm.$refs.refAutofocus.focus()
						}
					}
				}
				
				if(vm.modal.contractsAllShow) { vm.modal.contractsAllShow = false; return }
				if(vm.modal.contractsAll) { vm.modal.contractsAll = false; return }
				
			}
		});

		//===================================================================================
		// CTRL
		//===================================================================================
		var vm = this;
		var keydown_ctrl = false;
		window.addEventListener('keydown', function(event) {
			if (event.keyCode == 17) keydown_ctrl = true;	
		});
		window.addEventListener('keyup', function(event) {
			if (event.keyCode == 17) keydown_ctrl = false;
		});

		//===================================================================================
		// CTRL + ;
		//===================================================================================
		window.addEventListener('keydown', function(event) {
			if(event.keyCode == 192 && keydown_ctrl == true) {
				if(vm.modal.contactShow) {
					if(ctrlQ == 1) {
						vm.modal.contracts = true;
						vm.modal.requests = false;
						vm.modal.subContactIndex = false;
					}
					if(ctrlQ == 2) {
						vm.modal.contracts = false;
						vm.modal.requests = true;
						vm.modal.subContactIndex = false;
					}
					if(ctrlQ == 3) {
						vm.modal.contracts = false;
						vm.modal.requests = false;
						vm.modal.subContactIndex = true;
					}

					ctrlQ++;
					if(ctrlQ > 3) ctrlQ = 1;
				}
			}
		});
		
	},
	
})
</script>
@endsection