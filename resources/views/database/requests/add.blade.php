<div class="fullscreen p-1" v-if="modal.requestAdd">

	@csrf

	<button
		class="btn btn-danger"
		@click="modal.requestAdd = false;"
		style="width: 75px;">
		Späť
	</button>

	<button 
		class="btn btn-success" 
		style="width: 75px;"
		@click="addRequest()">
		Pridať
	</button>

	<div class="float-left-100">
		<label class="popis">Typ žiadosti</label></br>
		<select class="input" type="text" v-model="requestNew.typ_ziadosti">
			<option>-</option>
			<option v-for="req in requestsTemplates" :value="req.typ">@{{req.typ}}</option>
		</select>
	</div>

	<div class="float-left-zmluva">    

		<label class="popis">Spoločnosť</label></br>
		<input class="input" type="text" v-model="requestNew.institucia" list="list_institucie">

		<datalist id="list_institucie">
			<option v-for="company in companies" :value="company.address_1">@{{company.address_1}}</option>
		</datalist>

		<label class="popis">Program</label></br>
		<input class="input" type="text" v-model="requestNew.program" list="dataProdukty">

		<label class="popis">Číslo zmluvy</label></br>
		<input class="input" type="text" v-model="requestNew.cislo_zmluvy">

		<label class="popis">Miesto podpisu</label></br>
		<input class="input" type="text" v-model="requestNew.miesto_podpisu" list="list_miesto_podpisu">       

	</div>

	<div class="float-left-zmluva">

		<label class="popis">Celé meno</label></br>
		<input class="input" type="text" 
			v-model="requestNew.name_full">

		<label class="popis">Dátum narodenia</label></br>
		<input class="input" type="text" 
			v-model="requestNew.datum_narodenia">

		<label class="popis">Ulica, súpisné/orientačné číslo</label></br>
		<input class="input" type="text" 
			v-model="requestNew.ulica">

		<label class="popis">PSČ Obec</label></br>
		<input class="input" type="text" 
			v-model="requestNew.psc_obec" 
			list="dataObec">

	</div>

	<div class="float-left-zmluva">

		<label class="popis">Číslo účtu</label></br>
		<input class="input" type="text" 
			v-model="requestNew.cislo_uctu">

		<label class="popis">IČO</label></br>
		<input class="input" type="text" 
			v-model="requestNew.ico">

	</div>	

</div>