<style>
	.custom {
		font-size: 13px;
		font-family: DejaVu Sans !important;
	}
	.font-dejavu { 
		font-family: DejaVu Sans !important;
	}
</style>

<div class="font-dejavu">

	{{-- odosielatel --}}
	<div class="custom" style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
		<b>{{ $data['t_odosielatel'] }}</b>
	</div>
	<br>
	<br>
	<br>

	{{-- adresat --}}
	<div class="custom" style="display: block; margin-left: 65%; text-align: left;">
		<br>{{ $data['address_1'] }}
		<br>{{ $data['address_2'] }}
		<br>{{ $data['address_3'] }}
		<br>{{ $data['address_4'] }}
		<br>{{ $data['address_5'] }}
		<br>
	</div>
	<br>
	<br>

	{{-- mesto, datum --}}
	<div class="custom" style="display: block; margin-left: 50%; text-align: left;">
		{{ $data['t_miesto_datum_podpisu'] }}
	</div>
	<br>

	{{-- vec --}}
	<div class="custom" style="display: block; margin-left: 0%; text-align: left;">
		<b>{{ $data['t_vec'] }}</b>
	</div>
	<br>	

	{{-- body --}}
	<table class="custom" style="margin-left: 10%;">
		<tbody>

			@if ($data['address_1'] != '')
			<tr>
				<td>Spoločnosť:</td>
				<td>{{ $data['address_1'] }}</td>
			</tr>
			@endif

			@if ($data['program'] != '')
			<tr>
				<td>Program:</td>
				<td>{{ $data['program'] }}</td>
			</tr>
			@endif	
			
			@if ($data['cislo_zmluvy'] != '')
			<tr>
				<td>Číslo návrhu:</td>
				<td>{{ $data['cislo_zmluvy'] }}</td>
			</tr>				
			@endif

			@if ($data['name_full'] != '')
			<tr>
				<td>Klient:</td>
				<td>{{ $data['name_full'] }}</td>
			</tr>
			@endif

			@if ($data['datum_narodenia'] != '')
			<tr>
				<td>Dátum narodenia:    </td>
				<td>{{ $data['datum_narodenia'] }}</td>
			</tr>
			@endif

			@if ($data['ico'] != '')
			<tr>
				<td>IČO:</td>
				<td>{{ $data['ico'] }}</td>
			</tr>
			@endif

		</tbody>
	</table>
	<br>

	{{-- body --}}
	<div class="custom" style="margin-left: 0%;">
		@php
			echo $data['body'];
		@endphp
	</div>
	<br>

	<div class="custom" style="margin-left: 0%;">
		Ďakujem za vybavenie
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>

	<table class="custom" style="margin-left: 350px; text-align: center">
		<tr><td>.................................................................</td></tr>
		<tr><td>Podpis klienta</td></tr>
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>

	<div style="display: block; font-size: 11px; margin-left: auto; margin-right: auto; text-align: center;">
		Žiadosť vyhotovená v dvoch originálnych vyhotoveniach, pričom každá zmluvná strana obdrží jedno vyhotovenie.
	</div>

</div>