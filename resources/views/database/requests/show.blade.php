<div class="fullscreen p-1" v-if="modal.requestShow">

	<div class="row m-0 p-0">

		@csrf

		<button
			class="btn btn-danger mr-1"
			@click="modal.requestShow = false;"
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success mr-1"
			style="width: 75px;"
			@click="updateRequest()">
			Uložiť
		</button>

		<button 
			class="btn btn-info mr-1"
			style="width: 100px;"
			@click="navigateRequest('./requests/stream/'+requestSelect.id_string, true)">
			Otvoriť
		</button>

		<button
			class="btn btn-secondary" 
			style="width: 100px;"
			@click="navigateRequest('./requests/download/'+requestSelect.id_string, true)">
			Stiahnúť
		</button>

		<button
			class="btn btn-danger ml-auto" 
			style="width: 100px;" 
			@click="destroyRequest()">
			Vymazať
		</button>

		<div class="float-left-100">
			<label class="popis">Typ žiadosti</label></br>
			<select class="input" type="text" v-model="requestSelect.typ_ziadosti" @input="updateRequestTyp($event.target.value)">
				<option>-</option>
				<option v-for="req in requestsTemplates" :value="req.typ">@{{req.typ}}</option>
			</select>
		</div>

		<div class="float-left-zmluva">    

			<label class="popis">Spoločnosť</label></br>
			<input class="input" type="text" v-model="requestSelect.institucia" 
				@input="updateTemplate()"
				list="list_institucie">

			<datalist id="list_institucie">
				<option v-for="company in companies" :value="company.address_1">@{{company.address_1}}</option>
			</datalist>

		</div>

		<div class="float-left-zmluva">
			<label class="popis">Číslo účtu</label></br>
			<input class="input" type="text" 
				v-model="requestSelect.cislo_uctu">

		</div>

	</div>

	<hr>

	{{-- Custom template --}}
	<div 
		class="row m-0 p-0 mt-3 p-2 border"
		style="width: 800px; background-color: #f9f9f9; border-radius: 10px;">

		<label class="popis">Odosielateľ</label></br>
		<input class="input" type="text" v-model="requestSelect.t_odosielatel">

		<div style="margin-left: 65%; width: 250px;">
			<label class="popis">Adresat</label></br>
			<input class="input" type="text" v-model="requestSelect.address_1"></br>
			<input class="input" type="text" v-model="requestSelect.address_2"></br>
			<input class="input" type="text" v-model="requestSelect.address_3"></br>
			<input class="input" type="text" v-model="requestSelect.address_4"></br>
			<input class="input" type="text" v-model="requestSelect.address_5"></br>
		</div>
		
		<div style="margin-left: 55%; width: 300px;">
			<label class="popis">Miesto, dátum</label></br>
			<input class="input" type="text" v-model="requestSelect.t_miesto_datum_podpisu">
		</div>

		<div style="width: 100%">
			<label class="popis">VEC</label></br>
			<input class="input" type="text" v-model="requestSelect.t_vec">
		</div>

		<div style="width: 500px;">
			<label class="popis">Spoločnosť</label></br>
			<input class="input" type="text" v-model="requestSelect.institucia">
		</div>

		<div style="width: 500px;">
			<label class="popis">Program</label></br>
			<input class="input" type="text" v-model="requestSelect.program">
		</div>

		<div style="width: 500px;">
			<label class="popis">Číslo návrhu</label></br>
			<input class="input" type="text" v-model="requestSelect.cislo_zmluvy">
		</div>

		<div style="width: 500px;">
			<label class="popis">Klient</label></br>
			<input class="input" type="text" v-model="requestSelect.name_full">
		</div>

		<div clas="row" style="width: 500px;">
			<label class="popis">Dátum narodenia</label></br>
			<input class="input" type="text" v-model="requestSelect.datum_narodenia">
		</div>

		<div clas="row" style="width: 500px;">
			<label class="popis">IČO</label></br>
			<input class="input" type="text" v-model="requestSelect.ico">
		</div>

		<div class="float-left-100">
			<label class="popis">Text</label></br>
			<textarea 
				class="poznamka" 
				style="height: 500px;"
				v-model="requestSelect.body" rows="3"
			></textarea>
		</div>

	</div>			

</div>