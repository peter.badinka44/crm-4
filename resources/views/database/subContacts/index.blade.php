{{-- database.subContacts --}}
<div v-if="modal.subContactIndex" class="fullscreen p-1">

	<div class="row ml-0">

		{{-- back --}}
		<button 
		  class="btn btn-danger mr-1" 
		  @click="modal.subContactIndex = false" 
		  style="width: 75px;">
		  Späť
		</button>
	
		{{-- add --}}
		<button 
		  class="btn btn-success" 
		  style="width: 75px;"
		  @click="modal.subContactAdd = true; subContactNew = {};">
		  Pridať
		</button>
	
		{{-- search --}}
		<input class="form-control ml-1" 
		  type="text" 
		  v-model="search.subContact"
		  v-if="contactSelectSubContacts.length > 0"
		  style="width: 275px;"	
		  placeholder="Vyhľadať">
	
	</div>

	{{-- table --}}
	<table v-if="contactSelectSubContacts.length > 0" class="table-custom mt-1">
		<tr>
			<th>Vzťah</th>
			<th>Meno</th>
			<th>Číslo</th>
			<th>Email</th>
			<th>Stav</th>
		</tr>
		<tr v-for="row in rowsContactSelectSubContacts"
			:key="row.id"
			class="cursor-pointer"
			@click="showSubContact(row)">
			<td>@{{row.vztah}}</td>
			<td>@{{row.name_full}}</td>
			<td>@{{row.phone}}</td>
			<td>@{{row.email}}</td>
			<td>@{{row.stav}}</td>
		</tr>
	</table>

</div>