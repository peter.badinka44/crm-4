<div v-if="modal.subContactShow" class="fullscreen p-1">

	<div>
		<button 
			class="btn btn-danger"
			@click="modal.subContactShow = false"
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success" 
			style="width: 75px;" 
			@click="updateSubContact()">
			Uložiť
		</button>

		<button v-if="subContactSelect.check_delete == 'false'"
			class="btn btn-danger float-right" 
			style="width: 100px;" 
			@click="destroySubContact()">Vymazať
		</button>

		<button v-if="subContactSelect.check_delete == 'ok'"
			class="btn btn-success float-right" 
			style="width: 100px;" 
			@click="restoreContact()">Obnoviť
		</button>
		
		{{-- akcia --}}
		<button 
			type="button" 
			class="btn btn-info dropdown-toggle ml-0" 
			data-toggle="dropdown" 
			aria-haspopup="true"
			aria-expanded="false">
			Akcia
		</button>
		<div class="dropdown-menu">			
			<a class="dropdown-item cursor-pointer" 
				@click="modal.contactAdd = !modal.contactAdd">
				<b class="text-info">
					Klient
					<label v-if="subContactSelect.check_klient == 'true'" class="text-danger">(Active)</label>
					<b class="text-warning">(x)</b>
				</b>				
			</a>
		</div>

	</div>
	
	<label class="popis">Stav</label><br>
	<input class="input" type="text" list="listStav" v-model="subContactSelect.stav"><br>
	
	<div class="float-left">

		<label class="popis">Vzťah</label><br>
		<input class="input" type="text" v-model="subContactSelect.vztah">
						
		<label class="popis">Meno</label><br>
		<input class="input" type="text" v-model="subContactSelect.name_full">
	
		<label class="popis">Mobil</label><br>
		<input class="input" type="text" v-model="subContactSelect.phone">
	
		<label class="popis">Email</label><br>
		<input class="input" type="text" v-model="subContactSelect.email">	
	
		<label class="popis">Dátum akcie</label><br>
		<input class="input" type="date" v-model="subContactSelect.datum_akcie">
	
		<label class="popis">Ulica</label><br>
		<input class="input" type="text" v-model="subContactSelect.ulica">
	
		<label class="popis">PSC obec</label><br>
		<input class="input" type="text" v-model="subContactSelect.psc_obec" list="dataObec">
	
		<label class="popis">Príjmy</label><br>
		<input class="input" type="text" v-model="subContactSelect.prijmy">	
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Rodné číslo</label><br>
		<input 
			class="input"
			:style="style.subContactSelectRC"
			type="text" 
			v-model="subContactSelect.rodne_cislo" 
			@input="validateRodneCisloSubContactSelect()">
	
		<label class="popis">
			Dátum narodenia <span v-if="subContactSelect.datum_narodenia !== '0000-00-00'">(<b>@{{getAge(subContactSelect.datum_narodenia)}}</b>)
		</label><br>
		<input class="input" type="date" v-model="subContactSelect.datum_narodenia">
	
		<label class="popis">Miesto narodenia</label><br>
		<input class="input" type="text" v-model="subContactSelect.miesto_narodenia">
	
		<label class="popis">Číslo OP</label><br>
		<input class="input" type="text" v-model="subContactSelect.cislo_op">
	
		<label class="popis">Platnosť dokladu od</label><br>
		<input 
			class="input" 
			type="date" 
			v-model="subContactSelect.platnost_op_od"
			@change="subContactSelect.platnost_op_do = datePlus10YearsYMD(subContactSelect.platnost_op_od)">
	
		<label class="popis">Platnosť dokladu do</label><br>
		<input class="input" type="date" v-model="subContactSelect.platnost_op_do">	
	
		<label class="popis">Doklad vydal (mesto)</label><br>
		<input class="input" type="text" v-model="subContactSelect.doklad_vydal">
	
		<label class="popis">Výdavky</label><br>
		<input class="input" type="text" v-model="subContactSelect.vydavky">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Vzdelanie</label><br>
		<input class="input" type="text" v-model="subContactSelect.vzdelanie" list="listVzdelanie">
	
		<label class="popis">Rodinný stav</label><br>
		<input class="input" type="text" v-model="subContactSelect.rodinny_stav" list="listRodStav">
		
		<label class="popis">Povolanie</label><br>
		<input class="input" type="text" v-model="subContactSelect.specifikacia">
	
		<label class="popis">Zamestnávateľ</label><br>
		<input class="input" type="text" v-model="subContactSelect.zamestnanie">
	
		<label class="popis">Typ prac. zmluvy</label><br>
		<input class="input" type="text" v-model="subContactSelect.typ_prac_zmluvy" list="listPracZmluva">
	
		<label class="popis">Dátum nástupu do práce</label><br>
		<input class="input" type="date" v-model="subContactSelect.zaciatok_zamestnania">
	
		<label class="popis">Počet nezaopatrených detí</label><br>
		<input class="input" type="text" v-model="subContactSelect.pocet_deti">
	
		<label class="popis">Bývanie</label><br>
		<input class="input" type="text" v-model="subContactSelect.byvanie" list="listTypyByvania">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Zdravotná poisťovňa</label><br>
		<input class="input" type="text" v-model="subContactSelect.zdravotna_poistovna" list="listZdravPoistovne">	
	
		<label class="popis">Obvodný lekár - Meno</label><br>
		<input class="input" type="text" v-model="subContactSelect.lekar_meno">
	
		<label class="popis">Obvodný lekár - Adresa</label><br>
		<input class="input" type="text" v-model="subContactSelect.lekar_adresa">
	
		<label class="popis">Obvodný lekár - Kontakt</label><br>
		<input class="input" type="text" v-model="subContactSelect.lekar_kontakt">
	
		<label class="popis">Výška (cm)</label><br>
		<input class="input" type="text" v-model="subContactSelect.vyska">
	
		<label class="popis">Váha (kg)</label><br>
		<input class="input" type="text" v-model="subContactSelect.hmotnost">
	
		<label class="popis">Bežný účet - banka</label><br>
		<input class="input" type="text" v-model="subContactSelect.bezny_ucet_banka" list="listBanky">
	
		<label class="popis">Číslo účtu</label><br>
		<input 
			class="input" 
			type="text" 
			v-model="subContactSelect.cislo_uctu"
			@input="validateIbanSubContactSelect()"
			:style="style.subContactSelectCisloUctu">
	
	</div>
	
	<div class="float-left">
	
		<label class="popis">Oslovenie</label><br>
		<select class="input" v-model="subContactSelect.oslovenie">
			<option value=""></option>
			<option value="pán">pán</option>
			<option value="pani">pani</option>
		</select>
	
		<label class="popis">Priezvisko</label><br>
		<input class="input" type="text" v-model="subContactSelect.name_last">
	
		<label class="popis">Krstné meno</label><br>
		<input class="input" type="text" v-model="subContactSelect.name_first" list="dataKrstneMena">
	
		<label class="popis">Interval servisných mailov</label><br>
		<input class="input" type="number" v-model="subContactSelect.func_servis_email" min="0">
	
		<label class="popis">Narodeniny a meniny</label><br>
		<select class="input" type="text" v-model="subContactSelect.func_narodeniny_meniny">
			<option value=""></option>
			<option value="áno">áno</option>
		</select>
	
		{{-- <label class="popis">Google Sync</label><br>
		<select class="input" type="text" v-model="subContactSelect.android_sync">
			<option value=""></option>
			<option value="áno">áno</option>
			<option value="KL - Uložený v telefóne">KL - Uložený v telefóne</option>
		</select> --}}
	
	</div>
	
	<div class="float-left-100">
		<label class="popis">Poznámky</label><br>
		<textarea 
			class="poznamka"
			v-model="subContactSelect.poznamka"
		></textarea>
	</div>
	
</div>