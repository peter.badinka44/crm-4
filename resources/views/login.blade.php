@extends('master')
@section('title', 'Login')

@section('content')

<div class="d-flex justify-content-center align-items-center m-5 p-4"> 

	<div class="form-group p-3 bg-light border rounded" style="width: 400px;">

		@csrf

		<input 
			type="text"
			class="form-control mb-2"
			placeholder="Email"
			v-model="email"
			required
		>
		<input 
			type="password" 
			class="form-control mb-2" 
			placeholder="Password"
			v-model="pass"
			required
		>
		<input type="submit" class="btn btn-primary" value="Prihlásiť" @click="login()">

	</div>

</div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',

	data: () => ({
		email: null,
		pass: null,
		appUrl: <?php echo '"' . env('APP_URL') . '"'; ?>,
		offlineMode: false,
	}),	

	methods: {

		checkOfflineMode() {
			this.offlineMode = this.appUrl.includes('localhost') ? true : false
		},

		login() {
			if(this.email.length == 0 || this.pass.length == 0)
				return alert('Wrong username or password...')
			axios.post('./auth/login', {
				email: this.email,
				pass: this.pass,
			}).then((response) => {				
				if(response.data.access == true) {
					window.location.href = './';
				} else {
					alert('Wrong username or password...')
				}
			});
		},
	},

	mounted() {
		this.checkOfflineMode()
	}
	
})
</script>
@endsection