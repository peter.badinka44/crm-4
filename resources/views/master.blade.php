<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="UTF-8-mb4-general">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="icon" href="{{ URL::asset('favicon.png') }}" type="png"/>
	<link href="{{ URL::asset('css/google-fonts.css') }}"	rel="stylesheet"/>
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
	
	<title>@yield('title')</title>
</head>
<body>

	@yield('particles-js')

<div id="app">	
	<div class="template">

		{{-- Content --}}
		@yield('content')

	</div>
</div>

{{-- External scripts --}}
<script src="{{ URL::asset('./js/vue-dev.js') }}"></script>
{{-- <script src="{{ URL::asset('js/vue.js') }}"></script> --}}
<script src="{{ URL::asset('js/vue-cookies.js') }}"></script>
<script src="{{ URL::asset('js/axios.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ URL::asset('js/popper.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/moment.min.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>

{{-- Scripts --}}
@yield('script')

</body>
</html>