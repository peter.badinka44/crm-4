{{-- Menu --}}
<div class="btn-group" style="">
	<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Menu
	</button>
	<div class="dropdown-menu">		
		<a class="dropdown-item  text-secondary" href="./">Databáza</a>
		
		<!-- @if(isset($data['callPage']))
			<a class="dropdown-item text-secondary" href="https://app-44.herokuapp.com/" target="_blank">Call-Page</a>
		@endif	 -->

		<div class="dropdown-divider"></div>

		@if(isset($data['sl-spravca-klienta']))
			<a class='dropdown-item text-secondary' href='./sl-spravca-klienta'>Správca klienta</b></a>
			<div class="dropdown-divider"></div>
		@endif
		
		@if(isset($data['aktivity']))
			<a class='dropdown-item text-secondary' href='./aktivity'>Aktivity</b></a>
			<div class="dropdown-divider"></div>
		@endif

		@if(isset($data['calculators']))
			<a class="dropdown-item text-secondary" href="./calculators/hypoteky" target="_blank">Hypotéky</a>
			<a class="dropdown-item text-secondary" href="./calculators/investor" target="_blank">Investor</a>
			<a class="dropdown-item text-secondary" href="./calculators/investor-plus" target="_blank">Investor Plus</a>
			<a class="dropdown-item text-secondary" href="./calculators/iban" target="_blank">IBAN kalkulačka</a>
			<a class="dropdown-item text-secondary" href="./calculators/date" target="_blank">Dátumová kalkulačka</a>
			<a class="dropdown-item text-secondary" href="./calculators/progresia" target="_blank">TN - Porovnanie progresie</a>
			<div class="dropdown-divider"></div>
		@endif
		<a class="dropdown-item text-secondary" href="./settings">Nastavenia účtu</a>
		<a class="dropdown-item text-danger" href="./auth/logout"><b>Odhlásiť</b></a>
	</div>
</div>