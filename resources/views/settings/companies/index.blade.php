<div class="tab-pane fade" id="tab-companies" role="tabpanel">

	<button class="btn btn-primary mb-1" @click="addCompany()">+</button>
	
	<table class="table-custom">
		<tr v-for="company in companies" style="cursor: pointer;" @click="onCapmpanyEdit(company)">
			<td>@{{company.address_1}}</td>
			<td>@{{company.address_2}}</td>
			<td>@{{company.address_3}}</td>
			<td>@{{company.address_4}}</td>
			<td>@{{company.address_5}}</td>
		</tr>
	</table>

</div>