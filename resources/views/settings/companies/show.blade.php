<div class="fullscreen p-1" v-if="modal.company">
    <button class="btn btn-danger mr-1 mb-1" @click="modal.company = false">
        Späť
    </button>
    <button class="btn btn-success mr-1 mb-1" @click="editCompany()" v-if="!modal.newCompany">
        Uložiť
    </button>
    <button class="btn btn-success mr-1 mb-1" @click="insertCompany()" v-if="modal.newCompany">
        Pridať
    </button>
    <button class="btn btn-danger mr-1 mb-1 float-right" @click="deleteCompany()" v-if="!modal.newCompany">
        Vymazať
    </button>
    <input type="text" class="form-control mb-1" v-model="selectedCompany.address_1">
    <input type="text" class="form-control mb-1" v-model="selectedCompany.address_2">
    <input type="text" class="form-control mb-1" v-model="selectedCompany.address_3">
    <input type="text" class="form-control mb-1" v-model="selectedCompany.address_4">
    <input type="text" class="form-control mb-1" v-model="selectedCompany.address_5">
</div>