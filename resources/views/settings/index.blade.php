@extends('master')
@section('title', 'Nastavenia')

@section('content')

<div class="fullscreen p-1" v-if="modal.show">

	<div style="position: fixed; right: 5px; top: 5px;">
		@include('menu')
	</div>
	
	<ul class="nav nav-pills mb-3 border" id="pills-tab" role="tablist" style="position: fixed; top: 0px; background-color: white;">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="pill" href="#tab-access" role="tab" aria-controls="tab-access">
				Account
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="pill" href="#tab-login-history" role="tab" aria-controls="tab-login-history">
				Login history
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="pill" href="#tab-requests" role="tab" aria-controls="tab-requests">
				Žiadosti
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="pill" href="#tab-companies" role="tab" aria-controls="tab-companies">
				Adresy
			</a>
		</li>
	</ul>

	<div class="tab-content mt-5">	

		<div class="tab-pane fade show active" id="tab-access" role="tabpanel">

			<div v-if="offlineMode">
				<h3 class="text-secondary">Change password</h3>

				<div class="row">
					<div class="col mb-2 p-0 col-10 col-sm-8 col-md-6 col-lg-4 col-xl-3">
						<input class="form-control"	type="password" placeholder="Old password" v-model="password.old" id="passOld">
					</div>
					<div class="col mb-2 ml-1 p-0 col-2">
						<button class="btn btn-light togglePassword" @click="togglePassword">Show</button>
					</div>
				</div>

				<div class="row">
					<div class="col mb-2 p-0 col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
						<input class="form-control"	type="password" placeholder="New password" v-model="password.new" id="passNew">
					</div>
					<div class="col mb-2 ml-1 p-0 col-2">
						<button class="btn btn-light  togglePassword" @click="togglePassword">Show</button>
					</div>
				</div>			

				<div class="row">
					<div class="col mb-2 p-0 col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
						<input class="form-control"	type="password" placeholder="New password" v-model="password.confirm" id="passConfirm">
					</div>
					<div class="col mb-2 ml-1 p-0 col-2">
						<button class="btn btn-light togglePassword" @click="togglePassword">Show</button>
					</div>
				</div>

				<div class="row">
					<button class="btn btn-primary mr-2" @click="changePassword">Change password</button>
					<button class="btn btn-light" @click="generatePassword">Generate password</button>			
				</div>
			</div>			

			<hr>
			
			<div class="d=flex mb-1">
				<h3 class="text-secondary">Prístup k môjmu účtu</h3>
				<table class="table-custom pt-1">
					<tr v-for="row in shareUsers" v-if="row.email != user.email">
						<td>@{{row.email}}</td>
						<td><button class="btn btn-danger" @click="removeUser(row.email)">x</button></td>
					</tr>
				</table>
			</div>
			<div class="d-flex" style="width: 400px;">
				<input 
					type="text" 
					class="form-control" 
					v-model="userAdd"
					placeholder=""
					autocomplete="off"
					@keyup.enter="addUser()">
				<button class="btn btn-primary ml-1" @click="addUser()">Pridať</button>
			</div>
			<hr v-if="user.email_array.length > 1">
			<div class="d=flex mb-1" v-if="user.email_array.length > 1">
				<h3 class="text-secondary">Prístup k iným účtom</h3>
				<table class="table-custom pt-1">
					<tr v-for="email in user.email_array" v-if="email != user.email">
						<td>@{{email}}</td>
						<td><button class="btn btn-danger" @click="removeAccess(email)">x</button></td>
					</tr>
				</table>
			</div>

		</div>

		<div class="tab-pane fade" id="tab-login-history" role="tabpanel">

			<table class="table-custom">
				<tr v-for="history in loginHistory" style="cursor: pointer;">
					<td :class="{ 'text-danger': history.access == 'false' }">
						@{{history.date}}
					</td>
					<td :class="{ 'text-danger': history.access == 'false' }">
						@{{history.email}}
					</td>
					<td :class="{ 'text-danger': history.access == 'false' }">
						@{{history.access}}
					</td>
				</tr>
				
			</table>

		</div>
		
		@include('settings.companies.index')
		@include('settings.requests.index')

	</div>	

	
	@include('settings.companies.show')
	@include('settings.requests.show')

</div>

@endsection

@section('script')
<script>
new Vue({
	el: '#app',
	data: () => ({
		user: {
			email_array: []
		},
		userAdd: null,
		shareUsers: [],
		companies: [],
		selectedCompany: {},
		requests: [],
		requestSelect: {},
		loginHistory: [],
		password: {
			old: '',
			new: '',
			confirm: '',
		},
		modal: {
			company: false,
			newCompany: false,
			requestShow: false,	
			requestNew: false,
			show: true,
		},
		appUrl: <?php echo '"' . env('APP_URL') . '"'; ?>,
		offlineMode: false,
	}),

	methods: {

		checkOfflineMode() {
			this.offlineMode = this.appUrl.includes('localhost') ? true : false
		},

		changePassword() {
			if (this.password.new !== this.password.confirm) {
				alert('Heslá sa nezhodujú.')
				return;
			}

			axios.post('./auth/changePassword', this.password).then(res => {
				if (res.data.err) {
					alert(res.data.msg)
				} else {
					this.password.old = ''
					this.password.new = ''
					this.password.confirm = ''
					alert('Heslo bolo úspešne zmenené.')
				}				
			})
		},

		getUser() {
			axios.get('./users/show').then(res => {
				this.user = res.data
			}, err => {
				alert(err)
			})
		},

		removeAccess(email) {
			axios.get('./settings/removeAccess/'+email).then(res => {
				this.user.email_array = res.data.email_array
			}, err => {
				alert(err)
			})
		},

		getShareUsers() {
			axios.get('./settings/getShareUsers').then(res => {
				this.shareUsers = res.data
			})
		},

		removeUser(email) {
			axios.get('./settings/removeShareUser/'+email).then(res => {
				if(!res.data.err) {
					this.shareUsers = this.shareUsers.filter(function(x) {
						if(x.email != email) return x.email
					})
				}
			}, err => alert(err))
		},

		addUser() {
			axios.get('./settings/addShareUser/'+this.userAdd).then(res => {
				if(!res.data.err) {
					this.shareUsers.push({
						email: this.userAdd
					})
					this.userAdd = null
				} else {
					alert(res.data.err)
				}
			})
		},

		getCompanies() {
			axios.get('./settings/getCompanies').then(res => {
				this.companies = res.data
			});
		},

		editCompany() {
			axios.post('./settings/editCompany', this.selectedCompany).then(res => {
				if (!res.data) {
					alert('Udaje sa nepodarilo uložiť.');
				}
				this.modal.company = false
			});
		},

		addCompany() {
			this.selectedCompany = {
				address_1: '',
				address_2: '',
				address_3: '',
				address_4: '',
				address_5: '',
			}
			this.modal.company = true
			this.modal.newCompany = true			
		},

		addRequest() {
			this.modal.requestShow = true
			this.modal.requestNew = true
			this.requestSelect = {
				body: '',
				typ: '',
				vec: ''
			}
		},

		insertRequest() {
			console.log(this.requestSelect)
			axios.post('./settings/insertRequest', this.requestSelect).then(res => {
				if (!res.data) {
					alert('Žiadosť sa nepodarilo pridať...');
				} else {					
					this.modal.requestNew = false
					this.modal.requestShow = false
					this.getRequests();
				}				
			});
		},

		insertCompany() {
			axios.post('./settings/insertCompany', this.selectedCompany).then(res => {
				if (!res.data) {
					alert('Udaje sa nepodarilo uložiť.');
				}				
				this.modal.company = false
				this.modal.newCompany = false
				this.getCompanies();
			});
		},

		deleteCompany() {
			axios.post('./settings/deleteCompany', this.selectedCompany).then(res => {
				if (!res.data) {
					alert('Udaje sa nepodarilo vymazať.');
				}
				this.companies = this.companies.filter(row => row.id != this.selectedCompany.id)
				this.modal.company = false
				this.modal.newCompany = false
			});
		},

		getRequests() {
			axios.get('./settings/getRequests').then(res => {
				this.requests = res.data
			});
		},

		showRequest(request) {
			this.modal.requestShow = true
			this.modal.requestNew = false
			this.requestSelect = request
		},

		updateRequest() {
			axios.post('./settings/editRequest', this.requestSelect).then(res => {
				if (!res.data) {
					alert('Udaje sa nepodarilo uložiť.')
				}
				this.getRequests()
				this.modal.requestShow = false				
			});
		},

		deleteRequest() {
			axios.post('./settings/deleteRequest', this.requestSelect).then(res => {
				if (!res.data) {
					alert('Udaje sa nepodarilo vymazať.');
				} else {
					this.getRequests()
					this.modal.requestShow = false
					this.modal.requestNew = false
				}				
			});
		},

		getLoginHistory() {
			axios.get('./settings/getLoginHistory').then(res => {
				this.loginHistory = res.data
			});
		},


		generatePassword() {
			let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_!@#$%^&*()_+[]{};,.<>/?";
			let password = "";
			let charsLength = chars.length;
			for(let i = 0; i < 12; i++){
				password += chars[Math.floor(Math.random() * charsLength)];
			}
			this.password.new = password
			this.password.confirm = password
		},

		togglePassword() {
			let passOld = document.getElementById('passOld')
			if (passOld.type == "text") {				
				passOld.type = "password"
				document.getElementById('passNew').type = "password"
				document.getElementById('passConfirm').type = "password"
				document.querySelectorAll('.togglePassword').forEach(el => {
					el.innerText = "Show"
				})
			} else {				
				passOld.type = "text"
				document.getElementById('passNew').type = "text"
				document.getElementById('passConfirm').type = "text"
				document.querySelectorAll('.togglePassword').forEach(el => {
					el.innerText = "Hide"
				})
			}
		},

		onCapmpanyEdit(company) {
			this.selectedCompany = company			
			this.modal.company = true
		},

	},

	mounted() {
		this.checkOfflineMode()
		this.getUser()
		this.getShareUsers()
		this.getCompanies()
		this.getRequests()
		this.getLoginHistory()
	},

})
</script>
@endsection