<div class="tab-pane fade" id="tab-requests" role="tabpanel">

	<button class="btn btn-primary mb-1" @click="addRequest()">+</button>

	<table class="table-custom">
		<tr v-for="request in requests" @click="showRequest(request)" style="cursor: pointer;">
			<td>@{{request.typ}}</td>
		</tr>
	</table>

</div>