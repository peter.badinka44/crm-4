<div class="fullscreen p-1" v-if="modal.requestShow">

	<div class="row m-0 p-0">

		@csrf

		<button
			class="btn btn-danger mr-1"
			@click="modal.requestShow = false;"
			style="width: 75px;">
			Späť
		</button>

		<button 
			class="btn btn-success mr-1"
			style="width: 75px;"
			@click="updateRequest()"
			v-if="!modal.requestNew">
			Uložiť
		</button>

		<button class="btn btn-success mr-1" 
			@click="insertRequest()" 
			v-if="modal.requestNew">
			Pridať
		</button>

		<button
			class="btn btn-danger ml-auto" 
			style="width: 100px;" 
			@click="deleteRequest()"
			v-if="!modal.requestNew">
			Vymazať
		</button>
	</div>

	<hr>

	{{-- Custom template --}}
	<div 
		class="row m-0 p-0 mt-3 p-2 border"
		style="width: 800px; background-color: #f9f9f9; border-radius: 10px;">

		<div style="width: 100%">
			<label class="popis">Typ žiadosti</label></br>
			<input class="input" type="text" v-model="requestSelect.typ">
		</div>

		<div style="width: 100%">
			<label class="popis">VEC</label></br>
			<input class="input" type="text" v-model="requestSelect.vec">
		</div>

		<div class="float-left-100">
			<label class="popis">Text</label></br>
			<textarea 
				class="poznamka" 
				style="height: 500px;"
				v-model="requestSelect.body" rows="3"
			></textarea>
		</div>		

	</div>
</div>
