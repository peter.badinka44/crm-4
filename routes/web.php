<?php

use App\Http\Controllers\AktivityController;
use App\Http\Controllers\AngelicSoulController;
use App\Http\Controllers\ApiSupportController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CustomFields;
use App\Http\Controllers\DogController;
use App\Http\Controllers\GlobalAttributeController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\SlSpravcaKlientaController;
use App\Http\Controllers\SubContactController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AuthUser;

//======================================================================================
// Login/out, change password
//======================================================================================
Route::get('/login', [UserController::class, 'view']);
Route::post('/auth/login', [UserController::class, 'login']);
Route::get('/auth/logout', [UserController::class, 'logout']);
Route::post('/auth/sendLoginKey', [UserController::class, 'sendLoginKey']);
Route::post('/auth/changePassword', [UserController::class, 'changePassword']);

//======================================================================================
// Auth group
//======================================================================================
Route::middleware([AuthUser::class])->group(function () {
        
    //======================================================================================
    // Database - Contacts
    //======================================================================================
    Route::get('/', [ ContactController::class, 'view']);
    Route::get('/contacts/{id}', [ContactController::class, 'get']);
    Route::post('/contacts/index', [ContactController::class, 'index']);
    Route::post('/contacts/store', [ContactController::class, 'store']);
    Route::post('/contacts/update', [ContactController::class, 'update']);
    Route::get('/contacts/destroy/{id}', [ContactController::class, 'destroy']);
    Route::get('/contacts/restore/{id}', [ContactController::class, 'restore']);
    Route::post('/contacts/user-swap', [ContactController::class, 'userSwap']);
    Route::post('/contacts/mark-klient', [ContactController::class, 'markKlient']);
    Route::post('/contacts/create-zapis', [ContactController::class, 'createZapis']);
    Route::post('/contacts/create-fin-plan', [ContactController::class, 'createFinPlan']);
    Route::post('/contacts/mass-change', [ContactController::class, 'massChange']);
    
    //======================================================================================
    // Database - SubContacts
    //======================================================================================
    Route::get('/sub-contacts/index', [SubContactController::class, 'index']);
    Route::post('/sub-contacts/update', [SubContactController::class, 'update']);
    Route::post('/sub-contacts/store', [SubContactController::class, 'store']);
    Route::post('/sub-contacts/destroy', [SubContactController::class, 'destroy']);
    
    //======================================================================================
    // Database - Conracts
    //======================================================================================
    Route::get('/contracts/index/{id}', [ContractController::class, 'index']);
    Route::post('/contracts/store', [ContractController::class, 'store']);
    Route::post('/contracts/update', [ContractController::class, 'update']);
    Route::post('/contracts/destroy', [ContractController::class, 'destroy']);
    
    //======================================================================================
    // Database - Requests
    //======================================================================================
    Route::get('/requests/index', [RequestController::class, 'index']);
    Route::get('/requests/indexVyrocie', [RequestController::class, 'indexVyrocie']);
    Route::post('/requests/store', [RequestController::class, 'store']);
    Route::post('/requests/update', [RequestController::class, 'update']);
    Route::post('/requests/destroy', [RequestController::class, 'destroy']);
    Route::get('/requests/companies', [RequestController::class, 'companies']);
    Route::get('/requests/templates', [RequestController::class, 'templates']);

    //======================================================================================
    // User
    //======================================================================================
    Route::get('/users/show', [UserController::class, 'show']);
    Route::get('/users/get-email-history', [UserController::class, 'getEmailHistory']);    
    
    //======================================================================================
    // Aktivity
    //======================================================================================
    Route::get('/aktivity', [AktivityController::class, 'view']);
    Route::post('/aktivity/index', [AktivityController::class, 'index']);
    Route::post('/aktivity/store', [AktivityController::class, 'store']);

    //======================================================================================
    // Aktivity
    //======================================================================================
    Route::get('/kataster', function() { return view('kataster.index'); });
    
    //======================================================================================
    // Calculators
    //======================================================================================
    Route::get('/calculators/investor', function() { return view('calculators.investor'); });
    Route::get('/calculators/investor-plus', function() { return view('calculators.investor-plus'); });
    Route::get('/calculators/progresia', function() { return view('calculators.progresia'); });
    Route::get('/calculators/iban', function() { return view('calculators.iban'); });
    Route::get('/calculators/date', function() { return view('calculators.date'); });
    Route::get('/calculators/hypoteky', function() { return view('calculators.hypo.index'); });
    Route::get('/calculators/hypo/index', [GlobalAttributeController::class, 'getHypoIndex']);
    Route::post('/calculators/hypo/create', [GlobalAttributeController::class, 'crateNewHypo']);
    Route::post('/calculators/hypo/update', [GlobalAttributeController::class, 'updateHypo']);
    Route::post('/calculators/hypo/updateSettings', [GlobalAttributeController::class, 'updateHypoSettings']);
    Route::post('/calculators/hypo/delete', [GlobalAttributeController::class, 'deleteHypo']);
    
    //======================================================================================
    // Settings
    //======================================================================================
    Route::get('/settings', [SettingsController::class, 'view']);
    Route::get('/settings/getShareUsers', [SettingsController::class, 'getShareUsers']);
    Route::get('/settings/addShareUser/{email}', [SettingsController::class, 'addShareUser']);
    Route::get('/settings/removeShareUser/{email}', [SettingsController::class, 'removeShareUser']);
    Route::get('/settings/removeAccess/{email}', [SettingsController::class, 'removeAccess']);
    Route::get('/settings/getCompanies', [SettingsController::class, 'getCompanies']);
    Route::post('/settings/insertCompany', [SettingsController::class, 'insertCompany']);
    Route::post('/settings/editCompany', [SettingsController::class, 'editCompany']);
    Route::post('/settings/deleteCompany', [SettingsController::class, 'deleteCompany']);
    Route::get('/settings/getRequests', [SettingsController::class, 'getRequests']);
    Route::post('/settings/insertRequest', [SettingsController::class, 'insertRequest']);
    Route::post('/settings/editRequest', [SettingsController::class, 'editRequest']);
    Route::post('/settings/deleteRequest', [SettingsController::class, 'deleteRequest']);
    Route::get('/settings/getLoginHistory', [SettingsController::class, 'getLoginHistory']);

    //======================================================================================
    // Custom Fields
    //======================================================================================
    Route::post('/custom-fields/update-attribute', [CustomFields::class, 'updateAttribute']);
    Route::get('/custom-fields/index', [CustomFields::class, 'index']);

    //======================================================================================
    // Dogs
    //======================================================================================
    Route::get('/dogs/getAll', [DogController::class, 'getAll']);
    Route::post('/dogs/store', [DogController::class, 'store']);
    Route::post('/dogs/update', [DogController::class, 'update']);
    Route::post('/dogs/delete', [DogController::class, 'delete']);

    //======================================================================================
    // SL - Správca klienta
    //======================================================================================
    Route::get('/sl-spravca-klienta', [SlSpravcaKlientaController::class, 'view']);
    Route::post('/sl-generate-links', [SlSpravcaKlientaController::class, 'processHtml']);

    //======================================================================================
    // Magic...
    //======================================================================================
    Route::get('/magic/{x?}', [ApiSupportController::class, 'magic']);

});

//======================================================================================
// Angelic Soul
//======================================================================================
Route::get('/angelic-soul/{id_person}', [AngelicSoulController::class, 'view']);
Route::get('/angelic-soul/data/{id_person}', [AngelicSoulController::class, 'index']);
Route::post('/angelic-soul/update', [AngelicSoulController::class, 'update']);

//======================================================================================
// Show / Download PDF request
//======================================================================================
Route::get('/requests/stream/{id}', [RequestController::class, 'stream']);
Route::get('/requests/download/{id}', [RequestController::class, 'download']);

//======================================================================================
// Laravel artisan comands
//======================================================================================
Route::middleware([AuthUser::class])->group(function () {

    //Clear Cache facade value:
    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function() {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function() {
        $exitCode = Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function() {
        $exitCode = Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function() {
        $exitCode = Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    });

    //Clear Config cache:
    Route::get('/config-cache', function() {
        $exitCode = Artisan::call('config:cache');
        return '<h1>Clear Config cleared</h1>';
    });

});